﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Flurl;
using Flurl.Http;
using Fingerprint_Reader.models;
using System.IO;

namespace Fingerprint_Reader.forms
{
    public partial class AddPhoto : Form
    {
        Enrollments enrollment;
        string pictureOnePath = null;
        string pictureTwoPath = null;
        public AddPhoto(Enrollments enrollment)
        {
            InitializeComponent();
            this.enrollment = enrollment;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if(pictureOnePath == null && pictureTwoPath == null)
            {
                MessageBox.Show("Silahkan input minimal 1 foto!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            saveImage();

        }

        async void saveImage()
        {
            try
            {
                btnsave.Enabled = false;
                btnsave.Text = "Saving Picture 1...";
                var resp = await Properties.Settings.Default.serverUrl
                    .WithHeader("token", Program.token)
                    .AppendPathSegments(new string[] { "enrollment", "picture" })
                    .PostMultipartAsync(mp =>
                    {
                        mp.AddString("id", enrollment.id.ToString());
                        mp.AddString("number", 1.ToString());
                        var photo = File.OpenRead(pictureOnePath);
                        mp.AddFile("file", photo, String.Format("{0}_photo1.{1}", enrollment.id.ToString(), "jpg"));
                    });
                if (resp.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    var r = MessageBox.Show("Simpan gagal. Coba lagi?", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (r == DialogResult.Yes)
                    {
                        saveImage();
                    }
                    btnsave.Enabled = true;
                    btnsave.Text = "Simpan";

                    return;
                }
                if (pictureTwoPath != null)
                {
                    btnsave.Text = "Saving Picture 2...";
                    resp = await Properties.Settings.Default.serverUrl
                        .WithHeader("token", Program.token)
                        .AppendPathSegments(new string[] { "enrollment", "picture" })
                        .PostMultipartAsync(mp =>
                        {
                            mp.AddString("number", 2.ToString());
                            mp.AddString("id", enrollment.id.ToString());
                            var photo = File.OpenRead(pictureTwoPath);
                            mp.AddFile("file", photo, String.Format("{0}_photo2.{1}", enrollment.id.ToString(), "jpg"));
                        });
                    if (resp.StatusCode != System.Net.HttpStatusCode.OK)
                    {
                        var r = MessageBox.Show("Simpan gagal. Coba lagi?", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (r == DialogResult.Yes)
                        {
                            saveImage();
                        }
                        btnsave.Enabled = true;
                        btnsave.Text = "Simpan";

                        return;
                    }
                }

                MessageBox.Show("Simpan berhasil.", "Berhasil", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnsave.Enabled = true;
                btnsave.Text = "Simpan";
                DataStore.LoadEnrollments(() => { });
                Hide();
                Dispose();
            }catch(Exception e)
            {
                Console.WriteLine(e);
                MessageBox.Show("Kesalahan Server. Gagal menyimpan file.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnsave.Enabled = true;
                btnsave.Text = "Simpan";
            }
        }

        private void pickphoto1_Click(object sender, EventArgs e)
        {
            var dialog = new OpenFileDialog();
            dialog.Filter = "Photo (*.jpg)|*.jpg|Photo (*.jpeg)|*.jpeg";

            var r = dialog.ShowDialog();            
            if(r == DialogResult.OK)
            {
                pictureOnePath = dialog.FileName;
                Console.WriteLine(pictureOnePath);
                pictureone.Image = Image.FromFile(pictureOnePath);
            }
        }

        private void pickphoto2_Click(object sender, EventArgs e)
        {
            var dialog = new OpenFileDialog();
            dialog.Filter = "Photo (*.jpg)|*.jpg|Photo (*.jpeg)|*.jpeg";

            var r = dialog.ShowDialog();
            if (r == DialogResult.OK)
            {
                pictureTwoPath = dialog.FileName;
                Console.WriteLine(pictureTwoPath);
                picturetwo.Image = Image.FromFile(pictureTwoPath);
            }
        }

        private void AddPhoto_Load(object sender, EventArgs e)
        {
            l_enrollment_name.Text = enrollment.firstname;
        }
    }
}

﻿using DPUruNet;
using Fingerprint_Reader.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Flurl;
using Flurl.Http;
using Fingerprint_Reader.models;
using System.Net.Http;

namespace Fingerprint_Reader.forms
{
    public partial class EnrollUser : Form
    {
        private Point BaseLocation = new Point(170, 71);
        private Size FormSize = new Size(862, 701);
        Reader fpreader = null;
        int _fpreadNo = 0;
        int fpreadNo
        {
            get
            {
                return _fpreadNo;
            }
            set
            {
                _fpreadNo = value;
                Invoke(new Action(() =>
                {
                    if (_fpreadNo > 2)
                    {
                        fp_scan_prompt.Text = "Finish";
                        btn_finish.Visible = true;
                    }
                    else
                    {
                        fp_scan_prompt.Text = String.Format("Scan fingerprint {0}", _fpreadNo + 1);
                    }
                }));
            }
        }

        models.Enrollments enrollment = new models.Enrollments();
        bool edit = false;
        public EnrollUser()
        {
            InitializeComponent();
        }

        public EnrollUser(Enrollments data)
        {
            InitializeComponent();

            c_divisi.DataSource = DataStore.DivisiKerja.ToList();
            c_kepegawaian.DataSource = DataStore.StatusKetenagakerjaan.ToList();


            enrollment = data;

            edit = true;
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
        void FillFormFromData()
        {
            t_firstname.Text = enrollment.firstname;
            t_lastname.Text = enrollment.lastname;
            t_nik.Text = enrollment.nik;
            t_nip.Text = enrollment.nip;
            t_nohp.Text = enrollment.phone;
            t_email.Text = enrollment.email;
            t_pangkat.Text = enrollment.grade;
            //t_alamat.Text = enrollment.address;
            t_kota.Text = enrollment.kota;
            t_kecamatan.Text = enrollment.kecamatan;
            t_kelurahan.Text = enrollment.kelurahan;
            t_nuptk.Text = enrollment.nuptk;

            t_duk.Value = enrollment.duk;
            c_active.Checked = enrollment.active == 1;

            t_username.Text = enrollment.username;

            t_rfid_code.Text = enrollment.rfid_code;
            t_rfid_active.Checked = enrollment.rfid_allowed == 1;

            try
            {
                c_divisi.SelectedValue = enrollment.division_id;
            }
            catch (Exception e) { }
            try
            {
                c_kepegawaian.SelectedValue = enrollment.employment_status_id;
            }
            catch (Exception e) { }
            c_jk.SelectedValue = enrollment.gender;
        }
        private void EnrollUser_Load(object sender, EventArgs e)
        {
            var jk = new Dictionary<String, String>();

            jk.Add("Pria", "Pria");
            jk.Add("Wanita", "Wanita");
            c_jk.DataSource = jk.ToList();
            c_jk.DisplayMember = "Value";
            c_jk.ValueMember = "Key";
            panel_biodata.Location = BaseLocation;
            panel_fp.Location = BaseLocation;
            panel_fp.Visible = false;
            panel_biodata.Visible = true;

            FormSize.Height = panel_biodata.Height + 20;

            if (edit)
            {
                FillFormFromData();
            }

            if (c_divisi.DataSource == null)
            {
                c_divisi.DataSource = DataStore.DivisiKerja.ToList();
                c_kepegawaian.DataSource = DataStore.StatusKetenagakerjaan.ToList();
            }


            var readers = HomePage.fpreaders;
            if (readers.Count < 1)
            {
                MessageBox.Show("Tidak ada alat fingerprint yang terdeteksi!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //this.Hide();
                //this.Dispose();
                //return;
                btn_accept_biodata.Enabled = false;

            }
            else
            {
                btn_accept_biodata.Enabled = true;
            }

            if (readers.Count > 0) fpreader = readers.First();
            //var fpresult = fpreader.Open(Constants.CapturePriority.DP_PRIORITY_EXCLUSIVE);
            l_data_karyawan.Font = new Font(l_data_karyawan.Font, FontStyle.Bold);
            fpreadNo = 0;
            btn_finish.Visible = false;

        }

        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }


        private void label2_Click(object sender, EventArgs e)
        {

        }
        bool validate()
        {
            if (t_firstname.Text == "") return false;
            //if (t_lastname.Text == "") return false;
            //if (t_nik.Text == "") return false;
            //if (t_nip.Text == "") return false;
            //if (t_nohp.Text == "") return false;
            //if (t_email.Text == "") return false;
            //if (t_pangkat.Text == "") return false;
            //if (t_kota.Text == "") return false;
            //if (t_kecamatan.Text == "") return false;
            //if (t_kelurahan.Text == "") return false;
            //if (t_nuptk.Text == "") return false;
            if (c_kepegawaian.SelectedValue == null) return false;
            if (c_divisi.SelectedValue == null) return false;
            if (c_jk.Text == "") return false;
            //if (t_username.Text == "") return false;
            //if (t_password.Text == "") return false;
            return true;
        }
        void populateData()
        {
            enrollment.firstname = t_firstname.Text == "" ? enrollment.firstname : t_firstname.Text;
            enrollment.lastname = t_lastname.Text == "" ? enrollment.lastname : t_lastname.Text;
            enrollment.nik = t_nik.Text == "" ? enrollment.nik : t_nik.Text;
            enrollment.nip = t_nip.Text == "" ? enrollment.nip : t_nip.Text;
            enrollment.phone = t_nohp.Text == "" ? enrollment.phone : t_nohp.Text;
            enrollment.email = t_email.Text == "" ? enrollment.email : t_email.Text;
            enrollment.grade = t_pangkat.Text == "" ? enrollment.grade : t_pangkat.Text;
            enrollment.kota = t_kota.Text == "" ? enrollment.kota : t_kota.Text;
            enrollment.kelurahan = t_kelurahan.Text == "" ? enrollment.kelurahan : t_kelurahan.Text;
            enrollment.kecamatan = t_kelurahan.Text == "" ? enrollment.kecamatan : t_kelurahan.Text;
            enrollment.nuptk = t_nuptk.Text == "" ? enrollment.nuptk : t_nuptk.Text;
            enrollment.username = t_username.Text == "" ? enrollment.username : t_username.Text;
            enrollment.password = t_password.Text == "" ? enrollment.password : t_password.Text;
            enrollment.gender = c_jk.Text == "" ? enrollment.gender : c_jk.SelectedValue.ToString();

            enrollment.employment_status_id = c_kepegawaian.SelectedValue.ToString();
            enrollment.division_id = c_divisi.SelectedValue.ToString();

            enrollment.duk = (int)t_duk.Value;

            enrollment.active = c_active.Checked ? 1 : 0;

            enrollment.rfid_allowed = t_rfid_active.Checked ? 1 : 0;
            enrollment.rfid_code = t_rfid_code.Text.Replace("\n", "").Replace("\r", "");

        }
        private void btn_accept_biodata_Click(object sender, EventArgs e)
        {
            if (!validate())
            {
                MessageBox.Show("Cek kembali input anda! Pastikan: \n" +
                    "1. Tidak boleh ada isian yang kosong\n" +
                    "2. Apabila ada yang ingin dikosongkan, gunakan strip (-)\n", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            populateData();
            scanFingerprint();
        }

        void scanFingerprint()
        {
            fpreadNo = 0;
            l_data_karyawan.Font = new Font(l_data_karyawan.Font, FontStyle.Regular);
            l_fingerprint.Font = new Font(l_data_karyawan.Font, FontStyle.Bold);
            panel_biodata.Visible = false;
            panel_fp.Visible = true;
            MessageBox.Show("Mulai membaca fingerprint.\nLetakkan jari anda pada reader yang terhubung", "Pendaftaran Fingerprint", MessageBoxButtons.OK, MessageBoxIcon.Information);
            foreach (var r in HomePage.fpreaders)
            {
                r.On_Captured += Fpreader_On_Captured;
            }
            //fpreader.CaptureAsync(Constants.Formats.Fid.ISO, Constants.CaptureProcessing.DP_IMG_PROC_DEFAULT, 500);
        }

        private void Fpreader_On_Captured(CaptureResult result)
        {
            Console.WriteLine("Scan completed");
            string fmd = enrollment.fmd1 = Fmd.SerializeXml(FeatureExtraction.CreateFmdFromFid(result.Data, Constants.Formats.Fmd.ISO).Data); ;
            if (fpreadNo == 0)
            {
                enrollment.fmd1 = fmd;
                fp1.Image = Resources.fingerprint_ok;
            }
            else if (fpreadNo == 1)
            {
                enrollment.fmd2 = fmd;
                fp2.Image = Resources.fingerprint_ok;
            }
            else if (fpreadNo == 2)
            {
                enrollment.fmd3 = fmd;
                fp3.Image = Resources.fingerprint_ok;


            }
            fpreadNo++;
        }

        private void EnrollUser_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                foreach (var r in HomePage.fpreaders)
                {
                    //r.On_Captured -= Fpreader_On_Captured;
                }
                //fpreader.Dispose();
            }
            catch (Exception ex)
            {

            }
        }

        async private void btn_finish_Click(object sender, EventArgs e)
        {
            if (edit)
            {
                update();
            }
            else
            {
                save();
            }
            foreach (var r in HomePage.fpreaders)
            {
                r.On_Captured -= Fpreader_On_Captured;
            }
        }
        async void save()
        {
            btn_finish.Enabled = false;
            try
            {
                var q = await Settings.Default.serverUrl
                    .WithHeader("token", Program.token)
                    .AppendPathSegment("enrollment")
                    .PostJsonAsync(enrollment);

                if (q.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    MessageBox.Show("Pendaftaran berhasil!", "Sukses", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Hide();
                    Dispose();
                }
                else
                {
                    MessageBox.Show("Pendaftaran Gagal!", "Sukses", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (FlurlHttpException ex)
            {
                if (ex.Call.Response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                {
                    MessageBox.Show("Ada masalah dalam input data! Silahkan input data dengan benar", "Sukses", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                MessageBox.Show("Koneksi ke server bermasalah!", "Sukses", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine(await ex.GetResponseStringAsync());
            }
            finally
            {
                btn_finish.Enabled = true;
                DataStore.LoadEnrollments(() => { });
            }
        }
        async void update()
        {
            btn_finish.Enabled = false;
            try
            {
                var req = Settings.Default.serverUrl
                    .WithHeader("token", Program.token)
                    .AppendPathSegment("enrollment");
                HttpResponseMessage q;

                if (edit)
                {
                    q = await req.AppendPathSegment(enrollment.id.ToString())
                    .PutJsonAsync(enrollment);
                }
                else
                {
                    q = await req.PostJsonAsync(enrollment);
                }

                if (q.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    MessageBox.Show("Pendaftaran berhasil!", "Sukses", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Hide();
                    Dispose();
                }
                else
                {
                    MessageBox.Show("Pendaftaran Gagal!", "Sukses", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (FlurlHttpException ex)
            {
                MessageBox.Show("Koneksi ke server bermasalah!", "Sukses", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                btn_finish.Enabled = true;
                DataStore.LoadEnrollments(() => { });
            }
        }

        private void panel_fp_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var confirm = MessageBox.Show("Simpan tanpa ubah fingerprint?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (confirm == DialogResult.Yes)
            {
                populateData();
                update();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
    }
}

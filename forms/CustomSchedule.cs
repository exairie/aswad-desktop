﻿using Fingerprint_Reader.models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Flurl;
using Flurl.Http;

namespace Fingerprint_Reader.forms
{
    public partial class CustomSchedule : Form
    {
        PersonalSchedule schedule;
        Enrollments enrollment;
        public CustomSchedule(Enrollments enrollment)
        {
            InitializeComponent();
            this.enrollment = enrollment;
            if(enrollment.personal_schedule != null)
            {
                this.schedule = enrollment.personal_schedule;
            }
        }
        void populate()
        {
            if(this.schedule == null)
            {
                this.schedule = new PersonalSchedule();
                this.schedule.enrollment_id = enrollment.id;
            }
            schedule.senin_custom = rb_senin_jadwalsendiri.Checked ? 1 : 0;
            schedule.senin_hadir = !rb_senin_tidakhadir.Checked ? 1 : 0;
            schedule.senin_masuk_jam = (int)n_jammasuk_senin.Value;
            schedule.senin_masuk_menit = (int)n_menitmasuk_senin.Value;
            schedule.senin_keluar_jam = (int)n_jamkeluar_senin.Value;
            schedule.senin_keluar_menit = (int)n_menitkeluar_senin.Value;


            schedule.selasa_custom = rb_selasa_jadwalsendiri.Checked ? 1 : 0;
            schedule.selasa_hadir = !rb_selasa_tidakhadir.Checked ? 1 : 0;
            schedule.selasa_masuk_jam = (int)n_jammasuk_selasa.Value;
            schedule.selasa_masuk_menit = (int)n_menitmasuk_selasa.Value;
            schedule.selasa_keluar_jam = (int)n_jamkeluar_selasa.Value;
            schedule.selasa_keluar_menit = (int)n_menitkeluar_selasa.Value;


            schedule.rabu_custom = rb_rabu_jadwalsendiri.Checked ? 1 : 0;
            schedule.rabu_hadir = !rb_rabu_tidakhadir.Checked ? 1 : 0;
            schedule.rabu_masuk_jam = (int)n_jammasuk_rabu.Value;
            schedule.rabu_masuk_menit = (int)n_menitmasuk_rabu.Value;
            schedule.rabu_keluar_jam = (int)n_jamkeluar_rabu.Value;
            schedule.rabu_keluar_menit = (int)n_menitkeluar_rabu.Value;


            schedule.kamis_custom = rb_kamis_jadwalsendiri.Checked ? 1 : 0;
            schedule.kamis_hadir = !rb_kamis_tidakhadir.Checked ? 1 : 0;
            schedule.kamis_masuk_jam = (int)n_jammasuk_kamis.Value;
            schedule.kamis_masuk_menit = (int)n_menitmasuk_kamis.Value;
            schedule.kamis_keluar_jam = (int)n_jamkeluar_kamis.Value;
            schedule.kamis_keluar_menit = (int)n_menitkeluar_kamis.Value;


            schedule.jumat_custom = rb_jumat_jadwalsendiri.Checked ? 1 : 0;
            schedule.jumat_hadir = !rb_jumat_tidakhadir.Checked ? 1 : 0;
            schedule.jumat_masuk_jam = (int)n_jammasuk_jumat.Value;
            schedule.jumat_masuk_menit = (int)n_menitmasuk_jumat.Value;
            schedule.jumat_keluar_jam = (int)n_jamkeluar_jumat.Value;
            schedule.jumat_keluar_menit = (int)n_menitkeluar_jumat.Value;

        }
        void setView()
        {
            if(this.schedule == null)
            {
                l_entry_jadwal.Text = "Sesuai Divisi";
                return;
            }
            l_entry_jadwal.Text = "Custom";
            rb_senin_jadwalsendiri.Checked = schedule.senin_custom == 1;
            rb_senin_tidakhadir.Checked = schedule.senin_hadir == 0;
            rb_senin_sesuaidivisi.Checked = schedule.senin_hadir == 0 && schedule.senin_custom == 0;
            n_jammasuk_senin.Value = (decimal)schedule.senin_masuk_jam;
            n_menitmasuk_senin.Value = (decimal)schedule.senin_masuk_menit;
            n_jamkeluar_senin.Value = (decimal)schedule.senin_keluar_jam;
            n_menitkeluar_senin.Value = (decimal)schedule.senin_keluar_menit;

            rb_selasa_jadwalsendiri.Checked = schedule.selasa_custom == 1;
            rb_selasa_tidakhadir.Checked = schedule.selasa_hadir == 0;
            rb_selasa_sesuaidivisi.Checked = schedule.selasa_hadir == 0 && schedule.selasa_custom == 0;
            n_jammasuk_selasa.Value = (decimal)schedule.selasa_masuk_jam;
            n_menitmasuk_selasa.Value = (decimal)schedule.selasa_masuk_menit;
            n_jamkeluar_selasa.Value = (decimal)schedule.selasa_keluar_jam;
            n_menitkeluar_selasa.Value = (decimal)schedule.selasa_keluar_menit;

            rb_rabu_jadwalsendiri.Checked = schedule.rabu_custom == 1;
            rb_rabu_tidakhadir.Checked = schedule.rabu_hadir == 0;
            rb_rabu_sesuaidivisi.Checked = schedule.rabu_hadir == 0 && schedule.rabu_custom == 0;
            n_jammasuk_rabu.Value = (decimal)schedule.rabu_masuk_jam;
            n_menitmasuk_rabu.Value = (decimal)schedule.rabu_masuk_menit;
            n_jamkeluar_rabu.Value = (decimal)schedule.rabu_keluar_jam;
            n_menitkeluar_rabu.Value = (decimal)schedule.rabu_keluar_menit;

            rb_kamis_jadwalsendiri.Checked = schedule.kamis_custom == 1;
            rb_kamis_tidakhadir.Checked = schedule.kamis_hadir == 0;
            rb_kamis_sesuaidivisi.Checked = schedule.kamis_hadir == 0 && schedule.kamis_custom == 0;
            n_jammasuk_kamis.Value = (decimal)schedule.kamis_masuk_jam;
            n_menitmasuk_kamis.Value = (decimal)schedule.kamis_masuk_menit;
            n_jamkeluar_kamis.Value = (decimal)schedule.kamis_keluar_jam;
            n_menitkeluar_kamis.Value = (decimal)schedule.kamis_keluar_menit;

            rb_jumat_jadwalsendiri.Checked = schedule.jumat_custom == 1;
            rb_jumat_tidakhadir.Checked = schedule.jumat_hadir == 0;
            rb_jumat_sesuaidivisi.Checked = schedule.jumat_hadir == 0 && schedule.jumat_custom == 0;
            n_jammasuk_jumat.Value = (decimal)schedule.jumat_masuk_jam;
            n_menitmasuk_jumat.Value = (decimal)schedule.jumat_masuk_menit;
            n_jamkeluar_jumat.Value = (decimal)schedule.jumat_keluar_jam;
            n_menitkeluar_jumat.Value = (decimal)schedule.jumat_keluar_menit;
        }

        private void flowLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {            
        }

        private void radiobutton_change(object sender, EventArgs e)
        {
            var radio = sender as RadioButton;                       
        }

        private void button1_Click(object sender, EventArgs e)
        {
            save();
        }

        async void save()
        {
            try
            {
                populate();

                var resp = await Properties.Settings.Default.serverUrl
                    .WithHeader("token",Program.token)
                    .AppendPathSegments(new string[] { "enrollment", "personalschedule" })
                    .PutJsonAsync(new
                    {
                        id = enrollment.id,
                        schedule = schedule
                    });

                if (resp.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var dialog = MessageBox.Show("Simpan berhasil!", "Informasi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    DataStore.LoadEnrollments(() => { });
                    Hide();
                    Dispose();
                }
                else
                {
                    var dialog = MessageBox.Show("Kesalahan komunikasi ke server. Coba lagi?", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                    if (dialog == DialogResult.Yes)
                    {
                        save();
                    }
                }
            }
            catch(Exception e)
            {
                var dialog = MessageBox.Show("Gagal Menyimpan. Coba Lagi?", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if(dialog == DialogResult.Yes)
                {
                    save();
                }
            }
            finally
            {

            }
        }

        private void n_jammasuk_kamis_ValueChanged(object sender, EventArgs e)
        {

        }
        async void deleteSchedule()
        {
            try
            {
                populate();

                var resp = await Properties.Settings.Default.serverUrl
                    .WithHeader("token", Program.token)
                    .AppendPathSegments(new string[] { "enrollment", "personalschedule" })
                    .PutJsonAsync(new
                    {
                        id = enrollment.id                        
                    });

                if (resp.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var dialog = MessageBox.Show("Hapus berhasil!", "Informasi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    DataStore.LoadEnrollments(() => { });
                    Hide();
                    Dispose();
                }
                else
                {
                    var dialog = MessageBox.Show("Kesalahan komunikasi ke server. Coba lagi?", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                    if (dialog == DialogResult.Yes)
                    {
                        deleteSchedule();
                    }
                }
            }
            catch (Exception e)
            {
                var dialog = MessageBox.Show("Gagal Menyimpan. Coba Lagi?", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (dialog == DialogResult.Yes)
                {
                    deleteSchedule();
                }
            }
            finally
            {

            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            var dialog = MessageBox.Show("Hapus data pribadi?\nData pribadi pegawai akan dihapus dan jadwal akan mengikuti aturan divisi yang didaftarkan", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if(dialog == DialogResult.Yes)
            {
                deleteSchedule();
            }
        }

        private void CustomSchedule_Load(object sender, EventArgs e)
        {
            l_enrollment_name.Text = enrollment.firstname;
            l_employment.Text = enrollment.employment_status != null ? enrollment.employment_status.employment_status : "-";
            l_division.Text = enrollment.division != null ? enrollment.division.division : "-";

            setView();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}

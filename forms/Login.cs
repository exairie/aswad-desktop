﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Flurl;
using Flurl.Http;
namespace Fingerprint_Reader.forms
{
    public partial class Login : Form
    {
        class LoginResponse
        {
            public models.Admins data { get; set; }
            public string token { get; set; }
        }
        public Login()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            login();
        }

        async private void login()
        {
            try
            {
                var admin = await Properties.Settings.Default.serverUrl
                .AppendPathSegments(new string[] { "administrator", "login" })
                .PostJsonAsync(new
                {
                    username = textBox1.Text,
                    password = textBox2.Text
                }).ReceiveJson<LoginResponse>();
                if (admin.data != null)
                {
                    Program.AdminSession = admin.data;
                    Program.token = admin.token;                    
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Invalid username/password", "Error");
                }

            }catch(FlurlHttpException e)
            {
                MessageBox.Show("Tidak dapat terhubung ke server!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            } 
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}

﻿namespace Fingerprint_Reader.forms
{
    partial class Permits
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Permits));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.enrollmentDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reasonDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.permitbyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.permitfromDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.entryhourDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.exithourDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.permitsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.buatDispensasiBaruToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.permitsBindingSource)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Fingerprint_Reader.Properties.Resources.info;
            this.pictureBox1.Location = new System.Drawing.Point(12, 30);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(52, 50);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(70, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(851, 50);
            this.label1.TabIndex = 1;
            this.label1.Text = "Berikut adalah daftar dispensasi yang ditujukan untuk Guru/Karyawan\r\nAbsensi akan" +
    " berlaku pada tanggal yang ditentukan dan akan berpengaruh terhadap jam masuk/ke" +
    "luar Guru/Karyawan yang bersangkutan";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.enrollmentDataGridViewTextBoxColumn,
            this.reasonDataGridViewTextBoxColumn,
            this.permitbyDataGridViewTextBoxColumn,
            this.permitfromDataGridViewTextBoxColumn,
            this.entryhourDataGridViewTextBoxColumn,
            this.exithourDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.permitsBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(12, 92);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(909, 372);
            this.dataGridView1.TabIndex = 2;
            // 
            // enrollmentDataGridViewTextBoxColumn
            // 
            this.enrollmentDataGridViewTextBoxColumn.DataPropertyName = "enrollment";
            this.enrollmentDataGridViewTextBoxColumn.HeaderText = "Pemohon";
            this.enrollmentDataGridViewTextBoxColumn.Name = "enrollmentDataGridViewTextBoxColumn";
            this.enrollmentDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // reasonDataGridViewTextBoxColumn
            // 
            this.reasonDataGridViewTextBoxColumn.DataPropertyName = "reason";
            this.reasonDataGridViewTextBoxColumn.HeaderText = "Keperluan";
            this.reasonDataGridViewTextBoxColumn.Name = "reasonDataGridViewTextBoxColumn";
            this.reasonDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // permitbyDataGridViewTextBoxColumn
            // 
            this.permitbyDataGridViewTextBoxColumn.DataPropertyName = "permit_by";
            this.permitbyDataGridViewTextBoxColumn.HeaderText = "Diizinkan oleh";
            this.permitbyDataGridViewTextBoxColumn.Name = "permitbyDataGridViewTextBoxColumn";
            this.permitbyDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // permitfromDataGridViewTextBoxColumn
            // 
            this.permitfromDataGridViewTextBoxColumn.HeaderText = "Tanggal berlaku";
            this.permitfromDataGridViewTextBoxColumn.Name = "permitfromDataGridViewTextBoxColumn";
            this.permitfromDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // entryhourDataGridViewTextBoxColumn
            // 
            this.entryhourDataGridViewTextBoxColumn.HeaderText = "Jam Masuk";
            this.entryhourDataGridViewTextBoxColumn.Name = "entryhourDataGridViewTextBoxColumn";
            this.entryhourDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // exithourDataGridViewTextBoxColumn
            // 
            this.exithourDataGridViewTextBoxColumn.HeaderText = "Jam Keluar";
            this.exithourDataGridViewTextBoxColumn.Name = "exithourDataGridViewTextBoxColumn";
            this.exithourDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // permitsBindingSource
            // 
            this.permitsBindingSource.DataSource = typeof(Fingerprint_Reader.models.Permits);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buatDispensasiBaruToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(933, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // buatDispensasiBaruToolStripMenuItem
            // 
            this.buatDispensasiBaruToolStripMenuItem.Name = "buatDispensasiBaruToolStripMenuItem";
            this.buatDispensasiBaruToolStripMenuItem.Size = new System.Drawing.Size(127, 20);
            this.buatDispensasiBaruToolStripMenuItem.Text = "Buat dispensasi baru";
            this.buatDispensasiBaruToolStripMenuItem.Click += new System.EventHandler(this.buatDispensasiBaruToolStripMenuItem_Click);
            // 
            // Permits
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(933, 476);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Segoe UI Symbol", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Permits";
            this.Text = "Permits";
            this.Activated += new System.EventHandler(this.Permits_Activated);
            this.Load += new System.EventHandler(this.Permits_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.permitsBindingSource)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem buatDispensasiBaruToolStripMenuItem;
        private System.Windows.Forms.BindingSource permitsBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn enrollmentDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn reasonDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn permitbyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn permitfromDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn entryhourDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn exithourDataGridViewTextBoxColumn;
    }
}
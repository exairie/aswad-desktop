﻿using DPUruNet;
using Fingerprint_Reader.models;
using Flurl;
using Flurl.Http;
using Quobject.SocketIoClientDotNet.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fingerprint_Reader.forms
{
    public partial class Absensi : Form
    {
        private bool _reading = false;
        bool Reading
        {
            get
            {
                return _reading;
            }
            set
            {
                waitLoading1.Visible = value;
                reading.Visible = value;
                _reading = value;
            }
        }
        private Reader fpreader;
        private List<Reader> fpreaders = new List<Reader>();
        List<models.Permits> permits = new List<models.Permits>();
        public Absensi()
        {
            InitializeComponent();

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }
        void setupTimer()
        {
            var t = new Timer();
            t.Tick += (sender, e) =>
            {
                l_fingerprintcount.Text = DateTime.Now.ToString("dd MMM yyyy");
            };
        }

        async void loadSum()
        {
            try
            {
                var data = await Properties.Settings.Default.serverUrl
                .AppendPathSegments(new string[] { "attendance", "sumtoday" })
                .GetJsonAsync<Summary.FingerprintSum>();

                Invoke(new Action(() =>
                {
                    l_total.Text = DataStore.enrollments.Count.ToString();
                    l_belum_absen.Text = (DataStore.enrollments.Count() - data.attendance.Count()).ToString();
                    l_absen.Text = data.attendance.Count().ToString();
                    l_permits.Text = data.permit.Count().ToString();
                }));
                permits = data.permit.ToList();
            }
            catch(FlurlHttpException e)
            {

            }

        }

        private void Absensi_Load(object sender, EventArgs e)
        {
            setupTimer();
            l_fingerprintcount.Visible = false;
            progressBar1.Value = 80;
            DataStore.LoadEnrollments(() =>
            {
                progressBar1.Value = 100;
                progressBar1.Visible = false;
                l_total.Text = DataStore.enrollments.Count.ToString();
                startScan();
                loadSum();
            });
            Reading = false;
        }

        private void Fpreader_On_Captured(CaptureResult result)
        {
            Console.WriteLine("Finger Scanned");            
            Invoke(new Action(() =>
            {
                Reading = true;
            }));
            var fmd = FeatureExtraction.CreateFmdFromFid(result.Data, Constants.Formats.Fmd.ISO).Data;
            foreach (var fdata in DataStore.enrollments)
            {
                var fmdcheck = Fmd.DeserializeXml(fdata.fmd1);
                var checkResult = Comparison.Compare(fmd, 0, fmdcheck, 0).Score;
                bool match = false;

                Invoke(new Action(() =>
                {
                    l_status.Text = "Checking!";
                    l_status.Text = checkResult.ToString();
                }));

                if (checkResult <= 100)
                {
                    match = true;
                }

                fmdcheck = Fmd.DeserializeXml(fdata.fmd2);
                checkResult = Comparison.Compare(fmd, 0, fmdcheck, 0).Score;

                if (checkResult <= 100)
                {
                    match = true;
                }

                fmdcheck = Fmd.DeserializeXml(fdata.fmd3);
                checkResult = Comparison.Compare(fmd, 0, fmdcheck, 0).Score;

                if (checkResult <= 100)
                {
                    match = true;
                }

                if (match)
                {
                    new ScanComplete(fdata, fmd).ShowDialog();
                    loadSum();
                    break;
                }

            }
            Invoke(new Action(() =>
            {
                Reading = false;
            }));            
        }        
        void startScan()
        {
            var readers = ReaderCollection.GetReaders();
            if (readers.Count < 1)
            {
                MessageBox.Show("Alat fingerprint tidak terdeteksi!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            fpreaders = new List<Reader>(readers);

            foreach(var fpreader in fpreaders)
            {
                //fpreader = readers.First();

                fpreader.On_Captured += Fpreader_On_Captured;
                var fpresult = fpreader.Open(Constants.CapturePriority.DP_PRIORITY_EXCLUSIVE);
                fpreader.CaptureAsync(Constants.Formats.Fid.ISO, Constants.CaptureProcessing.DP_IMG_PROC_DEFAULT, 500);
                l_status.Text = "SCAN STARTED";
            }
        }
        void stopScan()
        {

            foreach(var r in fpreaders)
            {
                r.On_Captured -= Fpreader_On_Captured;
            }        
        }

        private void Absensi_Deactivate(object sender, EventArgs e)
        {
            if(fpreader != null)
            {
                stopScan();
            }
        }

        private void Absensi_Activated(object sender, EventArgs e)
        {

            if(fpreader != null)
            {
                foreach(var r in fpreaders)
                {
                    r.On_Captured += Fpreader_On_Captured;
                }
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            new Login().Show();
        }

        private void label18_Click(object sender, EventArgs e)
        {

        }

        private void Absensi_FormClosed(object sender, FormClosedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void waitLoading1_Click(object sender, EventArgs e)
        {

        }
    }
}

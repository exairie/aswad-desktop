﻿using Fingerprint_Reader.models;
using Flurl;
using Flurl.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fingerprint_Reader.forms
{
    public partial class ExtraTask : Form
    {
        Enrollments enrollment;
        public ExtraTask(Enrollments enrollment)
        {
            InitializeComponent();

            this.enrollment = enrollment;
        }
        async void loadExtraTask()
        {
            try
            {
                waitLoading1.Visible = true;
                var resp = await Properties.Settings.Default.serverUrl
                    .AppendPathSegments(new string[] { "extratask", "enrollment", enrollment.id.ToString() })
                    .WithHeader("token", Program.token)
                    .GetJsonAsync<models.ExtraTasks[]>();
                dataGridView1.DataSource = new List<models.ExtraTasks>(resp);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                waitLoading1.Visible = false;
            }
        }
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void ExtraTask_Load(object sender, EventArgs e)
        {
            dataGridView1.AutoGenerateColumns = false;
            loadExtraTask();
            populateCombobox();
            installView();

        }
        void installView()
        {
            l_nama.Text = String.Format("{0} {1}", enrollment.firstname, enrollment.lastname);
            l_divisi.Text = enrollment.division.division;
            l_staken.Text = enrollment.employment_status.employment_status;
            l_nip.Text = enrollment.nip;
            l_nuptk.Text = enrollment.nuptk;

        }
        void populateCombobox()
        {
            comboBox1.ValueMember = "key";
            comboBox1.DisplayMember = "value";
            comboBox1.DataSource = DataStore.ListPenugasan;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selected = comboBox1.SelectedValue.ToString();
            switch (selected)
            {
                case "wakasek":
                    l_jabatan.Text = "Bidang Wakil Kepala Sekolah";
                    break;
                case "kakom":
                    l_jabatan.Text = "Kompetensi Keahlian";
                    break;
                case "kabeng":
                    l_jabatan.Text = "Kompetensi Keahlian";
                    break;
                case "walas":
                    l_jabatan.Text = "Kelas";
                    break;
                case "stafftu":
                    l_jabatan.Text = "Posisi Staff";
                    break;
            }
        }

        async private void button1_Click(object sender, EventArgs e)
        {
            models.ExtraTasks task = new ExtraTasks()
            {
                enrollment_id = enrollment.id,
                task_description = textBox3.Text,
                task_point_spec = t_jabatan.Text,
                task_type = comboBox1.SelectedValue.ToString()
            };

            try
            {
                var resp = await Properties.Settings.Default.serverUrl
                    .WithHeader("token", Program.token)
                    .AppendPathSegments(new string[] { "extratask" })
                    .PostJsonAsync(task);

                if (resp.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    MessageBox.Show("Data tersimpan!");
                    loadExtraTask();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex < 0 || e.RowIndex < 0) return;
            var data = dataGridView1.DataSource as List<ExtraTasks>;
            var d = data[e.RowIndex];
            if (dataGridView1.Columns[e.ColumnIndex] is DataGridViewButtonColumn)
            {
                deleteTask(d.id.Value);
            }
        }
        async void deleteTask(int id)
        {
            var confirm = MessageBox.Show("Hapus data?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (confirm == DialogResult.No) return;
            try
            {
                var resp = await Properties.Settings.Default.serverUrl
                    .WithHeader("token", Program.token)
                    .AppendPathSegments(new string[] { "extratask", id.ToString() })
                    .DeleteAsync();
                if (resp.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    loadExtraTask();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

        }
    }
}

﻿namespace Fingerprint_Reader.forms
{
    partial class PermitInput
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PermitInput));
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.firstname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.division = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._select = new System.Windows.Forms.DataGridViewButtonColumn();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.group_bio = new System.Windows.Forms.GroupBox();
            this.l_telah_ada_izin = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.l_nopermit = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label22 = new System.Windows.Forms.Label();
            this.l_nama = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.l_nip = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.l_divisi = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.l_status_pn = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.l_izin = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.l_izin_bulan = new System.Windows.Forms.Label();
            this.waitLoading1 = new CSharp.Winform.UI.Loading.WaitLoading();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.group_input = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.r_tipe_izin_lain = new System.Windows.Forms.RadioButton();
            this.r_tipe_cuti_lain = new System.Windows.Forms.RadioButton();
            this.r_tipe_cuti = new System.Windows.Forms.RadioButton();
            this.r_tipe_sakit = new System.Windows.Forms.RadioButton();
            this.r_tipe_izin = new System.Windows.Forms.RadioButton();
            this.label26 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.l_reason = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.l_permitby = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.l_to = new System.Windows.Forms.DateTimePicker();
            this.l_from = new System.Windows.Forms.DateTimePicker();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.save_btn = new System.Windows.Forms.Button();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.enrollmentsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.group_bio.SuspendLayout();
            this.l_telah_ada_izin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.group_input.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.enrollmentsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(59)))), ((int)(((byte)(68)))));
            this.label2.Font = new System.Drawing.Font("Segoe UI Symbol", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(0, -1);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(1241, 69);
            this.label2.TabIndex = 4;
            this.label2.Text = "Buat Izin Dispensasi";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 79);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(140, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = "Cari Guru/Karyawan";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.firstname,
            this.division,
            this._select});
            this.dataGridView1.Location = new System.Drawing.Point(12, 135);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(295, 414);
            this.dataGridView1.TabIndex = 6;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // firstname
            // 
            this.firstname.DataPropertyName = "firstname";
            this.firstname.HeaderText = "Nama";
            this.firstname.Name = "firstname";
            this.firstname.ReadOnly = true;
            // 
            // division
            // 
            this.division.DataPropertyName = "division";
            this.division.HeaderText = "Divisi";
            this.division.Name = "division";
            this.division.ReadOnly = true;
            // 
            // _select
            // 
            this._select.HeaderText = "Action";
            this._select.Name = "_select";
            this._select.ReadOnly = true;
            this._select.Text = "Select";
            this._select.UseColumnTextForButtonValue = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 102);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(295, 27);
            this.textBox1.TabIndex = 7;
            this.textBox1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyUp);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI Symbol", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(17, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 19);
            this.label3.TabIndex = 0;
            this.label3.Text = "Nama";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Segoe UI Symbol", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(128, 33);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(252, 19);
            this.label4.TabIndex = 0;
            this.label4.Text = "Nama";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI Symbol", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(17, 63);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 19);
            this.label5.TabIndex = 0;
            this.label5.Text = "NIP";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Segoe UI Symbol", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(128, 63);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(252, 19);
            this.label6.TabIndex = 0;
            this.label6.Text = "Nama";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI Symbol", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(17, 95);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 19);
            this.label7.TabIndex = 0;
            this.label7.Text = "Divisi";
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Segoe UI Symbol", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(128, 95);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(252, 19);
            this.label8.TabIndex = 0;
            this.label8.Text = "Nama";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI Symbol", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(17, 126);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(69, 19);
            this.label9.TabIndex = 0;
            this.label9.Text = "Status PN";
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Segoe UI Symbol", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(128, 126);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(252, 19);
            this.label10.TabIndex = 0;
            this.label10.Text = "Nama";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(313, 102);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(386, 275);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Biodata";
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Segoe UI Symbol", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(128, 161);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(252, 19);
            this.label12.TabIndex = 0;
            this.label12.Text = "Nama";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI Symbol", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(17, 161);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(69, 19);
            this.label11.TabIndex = 0;
            this.label11.Text = "Status PN";
            // 
            // group_bio
            // 
            this.group_bio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.group_bio.Controls.Add(this.l_telah_ada_izin);
            this.group_bio.Controls.Add(this.flowLayoutPanel1);
            this.group_bio.Location = new System.Drawing.Point(313, 102);
            this.group_bio.Name = "group_bio";
            this.group_bio.Size = new System.Drawing.Size(386, 459);
            this.group_bio.TabIndex = 8;
            this.group_bio.TabStop = false;
            this.group_bio.Text = "Biodata";
            this.group_bio.Enter += new System.EventHandler(this.group_bio_Enter);
            // 
            // l_telah_ada_izin
            // 
            this.l_telah_ada_izin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.l_telah_ada_izin.Controls.Add(this.button2);
            this.l_telah_ada_izin.Controls.Add(this.button1);
            this.l_telah_ada_izin.Controls.Add(this.label24);
            this.l_telah_ada_izin.Controls.Add(this.label23);
            this.l_telah_ada_izin.Controls.Add(this.pictureBox2);
            this.l_telah_ada_izin.Controls.Add(this.l_nopermit);
            this.l_telah_ada_izin.Location = new System.Drawing.Point(6, 315);
            this.l_telah_ada_izin.Name = "l_telah_ada_izin";
            this.l_telah_ada_izin.Size = new System.Drawing.Size(371, 138);
            this.l_telah_ada_izin.TabIndex = 1;
            this.l_telah_ada_izin.TabStop = false;
            this.l_telah_ada_izin.Visible = false;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Segoe UI Symbol", 9F);
            this.button2.Location = new System.Drawing.Point(197, 109);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "Batalkan Izin";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Segoe UI Symbol", 9F);
            this.button1.Location = new System.Drawing.Point(303, 108);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(62, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Edit";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Segoe UI Symbol", 9F);
            this.label24.Location = new System.Drawing.Point(8, 58);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(357, 48);
            this.label24.TabIndex = 2;
            this.label24.Text = "Pegawai yang anda pilih sedang tercatat izin. Anda dapat membatalkan izin tersebu" +
    "t atau mengubah keterangan izin yang telah diinputkan";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(54, 26);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(144, 20);
            this.label23.TabIndex = 1;
            this.label23.Text = "Sedang Tercatat Izin";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Fingerprint_Reader.Properties.Resources.info;
            this.pictureBox2.Location = new System.Drawing.Point(9, 18);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(39, 34);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // l_nopermit
            // 
            this.l_nopermit.Font = new System.Drawing.Font("Segoe UI Symbol", 9F);
            this.l_nopermit.ForeColor = System.Drawing.Color.Red;
            this.l_nopermit.Location = new System.Drawing.Point(8, 110);
            this.l_nopermit.Name = "l_nopermit";
            this.l_nopermit.Size = new System.Drawing.Size(357, 22);
            this.l_nopermit.TabIndex = 4;
            this.l_nopermit.Text = "Anda tidak memiliki izin untuk mengubah informasi ";
            this.l_nopermit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.Controls.Add(this.panel1);
            this.flowLayoutPanel1.Controls.Add(this.panel2);
            this.flowLayoutPanel1.Controls.Add(this.panel6);
            this.flowLayoutPanel1.Controls.Add(this.panel3);
            this.flowLayoutPanel1.Controls.Add(this.panel4);
            this.flowLayoutPanel1.Controls.Add(this.panel5);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(6, 23);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(374, 279);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.l_nama);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(371, 38);
            this.panel1.TabIndex = 2;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Segoe UI Symbol", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(14, 7);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(45, 19);
            this.label22.TabIndex = 0;
            this.label22.Text = "Nama";
            // 
            // l_nama
            // 
            this.l_nama.Font = new System.Drawing.Font("Segoe UI Symbol", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_nama.Location = new System.Drawing.Point(119, 7);
            this.l_nama.Name = "l_nama";
            this.l_nama.Size = new System.Drawing.Size(249, 19);
            this.l_nama.TabIndex = 0;
            this.l_nama.Text = "Nama";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.l_nip);
            this.panel2.Location = new System.Drawing.Point(3, 47);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(371, 38);
            this.panel2.TabIndex = 2;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI Symbol", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(14, 7);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(31, 19);
            this.label13.TabIndex = 0;
            this.label13.Text = "NIP";
            // 
            // l_nip
            // 
            this.l_nip.Font = new System.Drawing.Font("Segoe UI Symbol", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_nip.Location = new System.Drawing.Point(119, 7);
            this.l_nip.Name = "l_nip";
            this.l_nip.Size = new System.Drawing.Size(249, 19);
            this.l_nip.TabIndex = 0;
            this.l_nip.Text = "Nama";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.label14);
            this.panel6.Controls.Add(this.l_divisi);
            this.panel6.Location = new System.Drawing.Point(3, 91);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(371, 38);
            this.panel6.TabIndex = 4;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI Symbol", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(14, 7);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 19);
            this.label14.TabIndex = 0;
            this.label14.Text = "Divisi";
            // 
            // l_divisi
            // 
            this.l_divisi.Font = new System.Drawing.Font("Segoe UI Symbol", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_divisi.Location = new System.Drawing.Point(119, 7);
            this.l_divisi.Name = "l_divisi";
            this.l_divisi.Size = new System.Drawing.Size(249, 19);
            this.l_divisi.TabIndex = 0;
            this.l_divisi.Text = "Nama";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label15);
            this.panel3.Controls.Add(this.l_status_pn);
            this.panel3.Location = new System.Drawing.Point(3, 135);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(371, 38);
            this.panel3.TabIndex = 3;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Segoe UI Symbol", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(14, 7);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(69, 19);
            this.label15.TabIndex = 0;
            this.label15.Text = "Status PN";
            // 
            // l_status_pn
            // 
            this.l_status_pn.Font = new System.Drawing.Font("Segoe UI Symbol", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_status_pn.Location = new System.Drawing.Point(119, 7);
            this.l_status_pn.Name = "l_status_pn";
            this.l_status_pn.Size = new System.Drawing.Size(249, 19);
            this.l_status_pn.TabIndex = 0;
            this.l_status_pn.Text = "Nama";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label17);
            this.panel4.Controls.Add(this.l_izin);
            this.panel4.Location = new System.Drawing.Point(3, 179);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(371, 38);
            this.panel4.TabIndex = 3;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Segoe UI Symbol", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(14, 7);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(77, 19);
            this.label17.TabIndex = 0;
            this.label17.Text = "Jumlah Izin";
            // 
            // l_izin
            // 
            this.l_izin.Font = new System.Drawing.Font("Segoe UI Symbol", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_izin.Location = new System.Drawing.Point(119, 7);
            this.l_izin.Name = "l_izin";
            this.l_izin.Size = new System.Drawing.Size(249, 19);
            this.l_izin.TabIndex = 0;
            this.l_izin.Text = "Nama";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.label19);
            this.panel5.Controls.Add(this.l_izin_bulan);
            this.panel5.Location = new System.Drawing.Point(3, 223);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(371, 38);
            this.panel5.TabIndex = 3;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Segoe UI Symbol", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(14, 7);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(62, 19);
            this.label19.TabIndex = 0;
            this.label19.Text = "Bulan Ini";
            // 
            // l_izin_bulan
            // 
            this.l_izin_bulan.Font = new System.Drawing.Font("Segoe UI Symbol", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_izin_bulan.Location = new System.Drawing.Point(119, 7);
            this.l_izin_bulan.Name = "l_izin_bulan";
            this.l_izin_bulan.Size = new System.Drawing.Size(249, 19);
            this.l_izin_bulan.TabIndex = 0;
            this.l_izin_bulan.Text = "Nama";
            // 
            // waitLoading1
            // 
            this.waitLoading1.Alpha = 125;
            this.waitLoading1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.waitLoading1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.waitLoading1.BindControl = null;
            this.waitLoading1.BkColor = System.Drawing.Color.WhiteSmoke;
            this.waitLoading1.IsTransparent = true;
            this.waitLoading1.Location = new System.Drawing.Point(12, 165);
            this.waitLoading1.Name = "waitLoading1";
            this.waitLoading1.Size = new System.Drawing.Size(295, 384);
            this.waitLoading1.TabIndex = 9;
            this.waitLoading1.Text = "waitLoading1";
            this.waitLoading1.Visible = false;
            this.waitLoading1.Click += new System.EventHandler(this.waitLoading1_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "division";
            this.dataGridViewTextBoxColumn1.HeaderText = "Divisi";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 84;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "division";
            this.dataGridViewTextBoxColumn2.HeaderText = "Divisi";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 84;
            // 
            // group_input
            // 
            this.group_input.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.group_input.Controls.Add(this.flowLayoutPanel2);
            this.group_input.Controls.Add(this.save_btn);
            this.group_input.Location = new System.Drawing.Point(705, 105);
            this.group_input.Name = "group_input";
            this.group_input.Size = new System.Drawing.Size(523, 456);
            this.group_input.TabIndex = 10;
            this.group_input.TabStop = false;
            this.group_input.Text = "Detil Dispensasi";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel2.AutoScroll = true;
            this.flowLayoutPanel2.Controls.Add(this.panel11);
            this.flowLayoutPanel2.Controls.Add(this.panel7);
            this.flowLayoutPanel2.Controls.Add(this.panel8);
            this.flowLayoutPanel2.Controls.Add(this.panel9);
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(6, 26);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(511, 388);
            this.flowLayoutPanel2.TabIndex = 0;
            this.flowLayoutPanel2.WrapContents = false;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.r_tipe_izin_lain);
            this.panel11.Controls.Add(this.r_tipe_cuti_lain);
            this.panel11.Controls.Add(this.r_tipe_cuti);
            this.panel11.Controls.Add(this.r_tipe_sakit);
            this.panel11.Controls.Add(this.r_tipe_izin);
            this.panel11.Controls.Add(this.label26);
            this.panel11.Location = new System.Drawing.Point(3, 3);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(481, 116);
            this.panel11.TabIndex = 7;
            // 
            // r_tipe_izin_lain
            // 
            this.r_tipe_izin_lain.AutoSize = true;
            this.r_tipe_izin_lain.Location = new System.Drawing.Point(292, 42);
            this.r_tipe_izin_lain.Name = "r_tipe_izin_lain";
            this.r_tipe_izin_lain.Size = new System.Drawing.Size(157, 24);
            this.r_tipe_izin_lain.TabIndex = 3;
            this.r_tipe_izin_lain.TabStop = true;
            this.r_tipe_izin_lain.Text = "Izin (keperluan lain)";
            this.r_tipe_izin_lain.UseVisualStyleBackColor = true;
            // 
            // r_tipe_cuti_lain
            // 
            this.r_tipe_cuti_lain.AutoSize = true;
            this.r_tipe_cuti_lain.Location = new System.Drawing.Point(366, 72);
            this.r_tipe_cuti_lain.Name = "r_tipe_cuti_lain";
            this.r_tipe_cuti_lain.Size = new System.Drawing.Size(94, 24);
            this.r_tipe_cuti_lain.TabIndex = 2;
            this.r_tipe_cuti_lain.TabStop = true;
            this.r_tipe_cuti_lain.Text = "Cuti (Lain)";
            this.r_tipe_cuti_lain.UseVisualStyleBackColor = true;
            // 
            // r_tipe_cuti
            // 
            this.r_tipe_cuti.AutoSize = true;
            this.r_tipe_cuti.Location = new System.Drawing.Point(130, 72);
            this.r_tipe_cuti.Name = "r_tipe_cuti";
            this.r_tipe_cuti.Size = new System.Drawing.Size(203, 24);
            this.r_tipe_cuti.TabIndex = 2;
            this.r_tipe_cuti.TabStop = true;
            this.r_tipe_cuti.Text = "Cuti (Tanggungan Negara)";
            this.r_tipe_cuti.UseVisualStyleBackColor = true;
            // 
            // r_tipe_sakit
            // 
            this.r_tipe_sakit.AutoSize = true;
            this.r_tipe_sakit.Location = new System.Drawing.Point(18, 72);
            this.r_tipe_sakit.Name = "r_tipe_sakit";
            this.r_tipe_sakit.Size = new System.Drawing.Size(59, 24);
            this.r_tipe_sakit.TabIndex = 2;
            this.r_tipe_sakit.TabStop = true;
            this.r_tipe_sakit.Text = "Sakit";
            this.r_tipe_sakit.UseVisualStyleBackColor = true;
            // 
            // r_tipe_izin
            // 
            this.r_tipe_izin.AutoSize = true;
            this.r_tipe_izin.Location = new System.Drawing.Point(18, 42);
            this.r_tipe_izin.Name = "r_tipe_izin";
            this.r_tipe_izin.Size = new System.Drawing.Size(190, 24);
            this.r_tipe_izin.TabIndex = 1;
            this.r_tipe_izin.TabStop = true;
            this.r_tipe_izin.Text = "Izin (dengan surat tugas)";
            this.r_tipe_izin.UseVisualStyleBackColor = true;
            this.r_tipe_izin.CheckedChanged += new System.EventHandler(this.r_tipe_izin_CheckedChanged);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Segoe UI Symbol", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(14, 7);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(100, 19);
            this.label26.TabIndex = 0;
            this.label26.Text = "Tipe dispensasi";
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.l_reason);
            this.panel7.Controls.Add(this.label16);
            this.panel7.Location = new System.Drawing.Point(3, 125);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(481, 85);
            this.panel7.TabIndex = 3;
            // 
            // l_reason
            // 
            this.l_reason.Location = new System.Drawing.Point(18, 30);
            this.l_reason.Multiline = true;
            this.l_reason.Name = "l_reason";
            this.l_reason.Size = new System.Drawing.Size(460, 49);
            this.l_reason.TabIndex = 1;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Segoe UI Symbol", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(14, 7);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(138, 19);
            this.label16.TabIndex = 0;
            this.label16.Text = "Keperluan Dispensasi";
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.l_permitby);
            this.panel8.Controls.Add(this.label18);
            this.panel8.Location = new System.Drawing.Point(3, 216);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(481, 64);
            this.panel8.TabIndex = 4;
            // 
            // l_permitby
            // 
            this.l_permitby.Enabled = false;
            this.l_permitby.Location = new System.Drawing.Point(18, 30);
            this.l_permitby.Name = "l_permitby";
            this.l_permitby.Size = new System.Drawing.Size(460, 27);
            this.l_permitby.TabIndex = 1;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Segoe UI Symbol", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(14, 7);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(128, 19);
            this.label18.TabIndex = 0;
            this.label18.Text = "Penanggung Jawab";
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.l_to);
            this.panel9.Controls.Add(this.l_from);
            this.panel9.Controls.Add(this.label21);
            this.panel9.Controls.Add(this.label20);
            this.panel9.Location = new System.Drawing.Point(3, 286);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(481, 62);
            this.panel9.TabIndex = 5;
            // 
            // l_to
            // 
            this.l_to.CustomFormat = "dd/MM/yyyy";
            this.l_to.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_to.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.l_to.Location = new System.Drawing.Point(248, 29);
            this.l_to.Name = "l_to";
            this.l_to.Size = new System.Drawing.Size(230, 23);
            this.l_to.TabIndex = 1;
            // 
            // l_from
            // 
            this.l_from.CustomFormat = "dd/MM/yyyy";
            this.l_from.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_from.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.l_from.Location = new System.Drawing.Point(18, 29);
            this.l_from.Name = "l_from";
            this.l_from.Size = new System.Drawing.Size(224, 23);
            this.l_from.TabIndex = 1;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Segoe UI Symbol", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(244, 7);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(53, 19);
            this.label21.TabIndex = 0;
            this.label21.Text = "Sampai";
            this.label21.Click += new System.EventHandler(this.label20_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Segoe UI Symbol", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(14, 7);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(106, 19);
            this.label20.TabIndex = 0;
            this.label20.Text = "Tanggal Berlaku";
            this.label20.Click += new System.EventHandler(this.label20_Click);
            // 
            // save_btn
            // 
            this.save_btn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.save_btn.BackColor = System.Drawing.Color.LimeGreen;
            this.save_btn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.save_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.save_btn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.save_btn.Image = global::Fingerprint_Reader.Properties.Resources.Save_65301;
            this.save_btn.Location = new System.Drawing.Point(375, 420);
            this.save_btn.Name = "save_btn";
            this.save_btn.Size = new System.Drawing.Size(148, 36);
            this.save_btn.TabIndex = 6;
            this.save_btn.Text = "Simpan";
            this.save_btn.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.save_btn.UseVisualStyleBackColor = false;
            this.save_btn.Click += new System.EventHandler(this.save_btn_Click);
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "division";
            this.dataGridViewTextBoxColumn3.HeaderText = "Divisi";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 84;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(59)))), ((int)(((byte)(68)))));
            this.pictureBox1.Image = global::Fingerprint_Reader.Properties.Resources.logo_app;
            this.pictureBox1.Location = new System.Drawing.Point(-1, -1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(86, 69);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // PermitInput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1240, 565);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.group_input);
            this.Controls.Add(this.waitLoading1);
            this.Controls.Add(this.group_bio);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Font = new System.Drawing.Font("Segoe UI Symbol", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "PermitInput";
            this.Text = "Input Izin";
            this.Activated += new System.EventHandler(this.PermitInput_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PermitInput_FormClosing);
            this.Load += new System.EventHandler(this.PermitInput_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.group_bio.ResumeLayout(false);
            this.l_telah_ada_izin.ResumeLayout(false);
            this.l_telah_ada_izin.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.group_input.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.enrollmentsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource enrollmentsBindingSource;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox group_bio;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label l_nama;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label l_nip;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label l_status_pn;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label l_izin;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label l_izin_bulan;
        private CSharp.Winform.UI.Loading.WaitLoading waitLoading1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label l_divisi;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn firstname;
        private System.Windows.Forms.DataGridViewTextBoxColumn division;
        private System.Windows.Forms.DataGridViewButtonColumn _select;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.GroupBox group_input;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox l_reason;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.TextBox l_permitby;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.DateTimePicker l_to;
        private System.Windows.Forms.DateTimePicker l_from;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button save_btn;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.RadioButton r_tipe_izin_lain;
        private System.Windows.Forms.RadioButton r_tipe_sakit;
        private System.Windows.Forms.RadioButton r_tipe_izin;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.GroupBox l_telah_ada_izin;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.RadioButton r_tipe_cuti_lain;
        private System.Windows.Forms.RadioButton r_tipe_cuti;
        private System.Windows.Forms.Label l_nopermit;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Flurl;
using Flurl.Http;
using Fingerprint_Reader.models;

namespace Fingerprint_Reader.forms
{
    public partial class CreateDivision : Form
    {
        models.Divisions division = new models.Divisions();
        bool edit = false;
        DivisionList parent;
        public CreateDivision()
        {
            InitializeComponent();
        }
        public CreateDivision withParent(DivisionList parent)
        {
            this.parent = parent;
            return this;
        }
        public CreateDivision(Divisions data)
        {
            InitializeComponent();
            this.division = data;
            edit = true;
            t_division.Text = division.division;
            n_hour_start.Value = (decimal)division.shift_start_hour;
            n_minute_start.Value = (decimal)division.shift_start_minute;
            n_hour_end.Value = (decimal)division.shift_end_hour;
            n_minute_end.Value = (decimal)division.shift_end_minute;

            n_closing_minute.Value = (decimal)division.closing_minute;
            n_closing_hour.Value = (decimal)division.closing_hour;
            n_break_hour.Value = (decimal)division.break_hour;
            n_break_minute.Value = (decimal)division.break_minute;
            n_break_hour_end.Value = (decimal)division.break_hour_end;
            n_break_minute_end.Value = (decimal)division.break_minute_end;

            inter_start_hour.Value = (decimal)division.inter_shift_start_hour;
            inter_start_minute.Value = (decimal)division.inter_shift_start_minute;
            inter_end_hour.Value = (decimal)division.inter_shift_end_hour;
            inter_end_minute.Value = (decimal)division.inter_shift_end_minute;



        }
        bool validate()
        {
            if (t_division.Text.Length < 1)
            {
                return false;
            }
            return true;
        }
        async private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            if (!validate())
            {
                MessageBox.Show("Cek kembali input anda!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }

            if (edit)
            {
                update();
            }
            else
            {
                create();
            }
        }

        async void update()
        {
            division.division = t_division.Text;
            division.shift_start_hour = (int)n_hour_start.Value;
            division.shift_end_hour = (int)n_hour_end.Value;

            division.shift_start_minute = (int)n_minute_start.Value;
            division.shift_end_minute = (int)n_minute_end.Value;

            division.closing_hour = (int)n_closing_hour.Value;
            division.closing_minute = (int)n_closing_minute.Value;

            division.break_hour = (int)n_break_hour.Value;
            division.break_minute = (int)n_break_minute.Value;

            division.break_hour_end = (int)n_break_hour_end.Value;
            division.break_minute_end = (int)n_break_minute_end.Value;

            division.inter_shift_start_hour = (int)inter_start_hour.Value;
            division.inter_shift_start_minute = (int)inter_start_minute.Value;

            division.inter_shift_end_hour = (int)inter_end_hour.Value;
            division.inter_shift_end_minute = (int)inter_end_minute.Value;

            try
            {
                var q = await Properties.Settings.Default.serverUrl
                    .WithHeader("token", Program.token)
                    .AppendPathSegment("division")
                    .AppendPathSegment(division.id.ToString())
                    .PutJsonAsync(division);
                if (q.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    MessageBox.Show("Data berhasil disimpan!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Hide();
                    Dispose();
                    DataStore.LoadDivisions(() =>
                    {
                        if (parent != null)
                        {
                            Invoke(new Action(() =>
                            {
                                parent.formatDate();
                            }));
                        }
                    });

                }
            }
            catch (FlurlHttpException ex)
            {
                MessageBox.Show("Tidak dapat terhubung ke server!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            finally
            {
                button1.Enabled = true;
            }
        }

        async void create()
        {
            division.division = t_division.Text;
            division.shift_start_hour = (int)n_hour_start.Value;
            division.shift_end_hour = (int)n_hour_end.Value;

            division.shift_start_minute = (int)n_minute_start.Value;
            division.shift_end_minute = (int)n_minute_end.Value;

            division.closing_hour = (int)n_closing_hour.Value;
            division.closing_minute = (int)n_closing_minute.Value;

            division.break_hour = (int)n_break_hour.Value;
            division.break_minute = (int)n_break_minute.Value;

            division.break_hour_end = (int)n_break_hour_end.Value;
            division.break_minute_end = (int)n_break_minute_end.Value;

            division.inter_shift_start_hour = (int)inter_start_hour.Value;
            division.inter_shift_start_minute = (int)inter_start_minute.Value;

            division.inter_shift_end_hour = (int)inter_end_hour.Value;
            division.inter_shift_end_minute = (int)inter_end_minute.Value;

            try
            {
                var q = await Properties.Settings.Default.serverUrl
                    .WithHeader("token", Program.token)
                    .AppendPathSegment("division")
                    .PostJsonAsync(division);
                if (q.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    MessageBox.Show("Data berhasil disimpan!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Hide();
                    Dispose();
                    DataStore.LoadDivisions(() =>
                    {
                        if (parent != null)
                        {
                            Invoke(new Action(() =>
                            {
                                parent.formatDate();
                            }));
                        }
                    });
                }
            }
            catch (FlurlHttpException ex)
            {
                MessageBox.Show("Tidak dapat terhubung ke server!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            finally
            {
                button1.Enabled = true;
            }
        }

        private void CreateDivision_Load(object sender, EventArgs e)
        {

        }
    }
}

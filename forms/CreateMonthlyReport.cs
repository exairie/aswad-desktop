﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fingerprint_Reader.forms
{
    public partial class CreateMonthlyReport : Form
    {
        Dictionary<int, string> mapTahun = new Dictionary<int, string>();
        Dictionary<int, string> mapBulan = new Dictionary<int, string>();
        Dictionary<string, string> mapDivisi = new Dictionary<string, string>();

        public CreateMonthlyReport()
        {
            InitializeComponent();
            var nyear = DateTime.Now.Year;
            for(int i = 0;i < 5;i++)
            {
                mapTahun.Add(nyear + i, (nyear + i).ToString());
            }
            mapBulan.Add(1, "Januari");
            mapBulan.Add(2, "Februari");
            mapBulan.Add(3, "Maret");
            mapBulan.Add(4, "April");
            mapBulan.Add(5, "Mei");
            mapBulan.Add(6, "Juni");
            mapBulan.Add(7, "Juli");
            mapBulan.Add(8, "Agustus");
            mapBulan.Add(9, "September");
            mapBulan.Add(10, "Oktober");
            mapBulan.Add(11, "November");
            mapBulan.Add(12, "Desember");

            mapDivisi.Add("guru", "Guru");
            mapDivisi.Add("tata_usaha", "Tata Usaha");
        }

        private void CreateMonthlyReport_Load(object sender, EventArgs e)
        {
            c_tahun.DisplayMember = "Value";
            c_tahun.ValueMember = "Key";
            c_bulan.DisplayMember = "Value";
            c_bulan.ValueMember = "Key";
            c_divisi.DisplayMember = "Value";
            c_divisi.ValueMember = "Key";

            c_tahun.DataSource = mapTahun.ToList();
            c_bulan.DataSource = mapBulan.ToList();
            c_divisi.DataSource = mapDivisi.ToList();


        }
        async void download(string filename)
        {
            var waiter = new DownloadFileStatus();
            waiter.TopMost = true;
            string tahun = c_tahun.SelectedValue.ToString(), bulan = c_bulan.SelectedValue.ToString();
            string divisi = c_divisi.SelectedValue.ToString();
            try
            {
                waiter.Show();
                var wClient = new WebClient();
                await Task.Run(() =>
                {
                    try
                    {
                        wClient.DownloadFile(Properties.Settings.Default.serverUrl +
                            String.Format("monthlyexport/{0}/{1}/{2}",
                            tahun,
                            bulan,divisi), filename);
                        Invoke(new Action(() =>
                        {
                            waiter.finish();
                        }));
                        var dialog = MessageBox.Show("File telah disimpan! Buka file?", "Berhasil", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                        if (dialog == DialogResult.Yes)
                        {
                            Process.Start(filename);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        Invoke(new Action(() =>
                        {
                            MessageBox.Show("File gagal disimpan!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            waiter.finish();
                        }));
                    }
                    finally
                    {

                    }
                });

            }
            catch (Exception e)
            {
                MessageBox.Show("File gagal disimpan!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void b_ok_Click(object sender, EventArgs e)
        {
            var dialog = new SaveFileDialog();
            dialog.Filter = "Excel File (*.xlsx)|*.xlsx";
            dialog.FileName = String.Format("Report-{0} {1} {2}", c_divisi.SelectedValue.ToString(), c_tahun.SelectedValue.ToString(), c_bulan.SelectedValue.ToString());

            var result = dialog.ShowDialog(); //shows save file dialog
            if (result == DialogResult.OK)
            {
                Console.WriteLine("writing to: " + dialog.FileName); //prints the file to save

                try
                {
                    download(dialog.FileName);
                }
                catch (Exception e2)
                {
                    MessageBox.Show("File gagal disimpan!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void c_divisi_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Flurl;
using Flurl.Http;

namespace Fingerprint_Reader.forms
{
    public partial class Offdays : Form
    {
                
        List<models.Offday> data = new List<models.Offday>();
        public Offdays()
        {
            InitializeComponent();
        }

        private void Offdays_Load(object sender, EventArgs e)
        {
            grid_offdays.AutoGenerateColumns = false;            
        }

        async void load()
        {
            try
            {
                waitLoading1.Visible = true;
                var resp = await Properties.Settings.Default.serverUrl
                    .AppendPathSegments("offdays")
                    .WithHeader("token", Program.token)
                    .GetJsonAsync<models.Offday[]>();

                this.data.Clear();
                this.data.AddRange(resp);

                Invoke(new Action(() =>
                {
                    grid_offdays.DataSource = null;
                    grid_offdays.DataSource = this.data;
                }));


            }
            catch (Exception e)
            {
                MessageBox.Show("Unable to fetch data! " + e.Message);
            }
            finally
            {
                var t = new Timer();
                t.Tick += (e,s)=> waitLoading1.Visible = false;
                t.Interval = 1000;
                t.Start();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            new AddOffday().ShowDialog();            
        }

        private void Offdays_Activated(object sender, EventArgs e)
        {
            load();
        }

        private void Offdays_Enter(object sender, EventArgs e)
        {

        }
    }
}

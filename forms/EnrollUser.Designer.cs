﻿namespace Fingerprint_Reader.forms
{
    partial class EnrollUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.panel_biodata = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.t_kelurahan = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.t_kecamatan = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.t_kota = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.c_active = new System.Windows.Forms.CheckBox();
            this.label14 = new System.Windows.Forms.Label();
            this.t_password = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.t_username = new System.Windows.Forms.TextBox();
            this.btn_accept_biodata = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.t_email = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.t_nohp = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.c_kepegawaian = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.c_divisi = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.t_pangkat = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.t_nuptk = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.t_nik = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.t_duk = new System.Windows.Forms.NumericUpDown();
            this.t_nip = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.c_jk = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.t_firstname = new System.Windows.Forms.TextBox();
            this.t_lastname = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.l_data_karyawan = new System.Windows.Forms.Label();
            this.l_fingerprint = new System.Windows.Forms.Label();
            this.panel_fp = new System.Windows.Forms.Panel();
            this.fp_scan_prompt = new System.Windows.Forms.Label();
            this.btn_finish = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.fp3 = new System.Windows.Forms.PictureBox();
            this.label16 = new System.Windows.Forms.Label();
            this.fp2 = new System.Windows.Forms.PictureBox();
            this.label15 = new System.Windows.Forms.Label();
            this.fp1 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.t_rfid_code = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.t_rfid_active = new System.Windows.Forms.CheckBox();
            this.panel_biodata.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t_duk)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel_fp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fp3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fp2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fp1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(59)))), ((int)(((byte)(68)))));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(0, -2);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(100, 0, 0, 0);
            this.label2.Size = new System.Drawing.Size(840, 69);
            this.label2.TabIndex = 3;
            this.label2.Text = "Daftarkan User  Baru";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // panel_biodata
            // 
            this.panel_biodata.Controls.Add(this.t_rfid_active);
            this.panel_biodata.Controls.Add(this.button1);
            this.panel_biodata.Controls.Add(this.groupBox2);
            this.panel_biodata.Controls.Add(this.groupBox1);
            this.panel_biodata.Controls.Add(this.btn_accept_biodata);
            this.panel_biodata.Controls.Add(this.label11);
            this.panel_biodata.Controls.Add(this.t_email);
            this.panel_biodata.Controls.Add(this.label21);
            this.panel_biodata.Controls.Add(this.t_rfid_code);
            this.panel_biodata.Controls.Add(this.label10);
            this.panel_biodata.Controls.Add(this.t_nohp);
            this.panel_biodata.Controls.Add(this.label9);
            this.panel_biodata.Controls.Add(this.c_kepegawaian);
            this.panel_biodata.Controls.Add(this.label3);
            this.panel_biodata.Controls.Add(this.label8);
            this.panel_biodata.Controls.Add(this.c_divisi);
            this.panel_biodata.Controls.Add(this.label7);
            this.panel_biodata.Controls.Add(this.t_pangkat);
            this.panel_biodata.Controls.Add(this.label18);
            this.panel_biodata.Controls.Add(this.t_nuptk);
            this.panel_biodata.Controls.Add(this.label6);
            this.panel_biodata.Controls.Add(this.t_nik);
            this.panel_biodata.Controls.Add(this.label5);
            this.panel_biodata.Controls.Add(this.t_duk);
            this.panel_biodata.Controls.Add(this.t_nip);
            this.panel_biodata.Controls.Add(this.label4);
            this.panel_biodata.Controls.Add(this.c_jk);
            this.panel_biodata.Controls.Add(this.label1);
            this.panel_biodata.Controls.Add(this.t_firstname);
            this.panel_biodata.Location = new System.Drawing.Point(170, 71);
            this.panel_biodata.Name = "panel_biodata";
            this.panel_biodata.Size = new System.Drawing.Size(659, 647);
            this.panel_biodata.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(296, 596);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(151, 37);
            this.button1.TabIndex = 19;
            this.button1.Text = "Simpan";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.t_kelurahan);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.t_kecamatan);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.t_kota);
            this.groupBox2.Location = new System.Drawing.Point(296, 352);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(325, 235);
            this.groupBox2.TabIndex = 48;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Alamat";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 154);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(114, 20);
            this.label20.TabIndex = 43;
            this.label20.Text = "Kelurahan/Desa";
            // 
            // t_kelurahan
            // 
            this.t_kelurahan.Location = new System.Drawing.Point(10, 186);
            this.t_kelurahan.Name = "t_kelurahan";
            this.t_kelurahan.Size = new System.Drawing.Size(309, 27);
            this.t_kelurahan.TabIndex = 15;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(8, 92);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(83, 20);
            this.label12.TabIndex = 43;
            this.label12.Text = "Kecamatan";
            // 
            // t_kecamatan
            // 
            this.t_kecamatan.Location = new System.Drawing.Point(12, 124);
            this.t_kecamatan.Name = "t_kecamatan";
            this.t_kecamatan.Size = new System.Drawing.Size(309, 27);
            this.t_kecamatan.TabIndex = 14;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 30);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(118, 20);
            this.label19.TabIndex = 43;
            this.label19.Text = "Kota/Kabupaten";
            // 
            // t_kota
            // 
            this.t_kota.Location = new System.Drawing.Point(10, 62);
            this.t_kota.Name = "t_kota";
            this.t_kota.Size = new System.Drawing.Size(309, 27);
            this.t_kota.TabIndex = 13;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.c_active);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.t_password);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.t_username);
            this.groupBox1.Location = new System.Drawing.Point(8, 393);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(271, 172);
            this.groupBox1.TabIndex = 47;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Akses Frontend";
            // 
            // c_active
            // 
            this.c_active.AutoSize = true;
            this.c_active.Location = new System.Drawing.Point(167, 124);
            this.c_active.Name = "c_active";
            this.c_active.Size = new System.Drawing.Size(59, 24);
            this.c_active.TabIndex = 18;
            this.c_active.Text = "Aktif";
            this.c_active.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 92);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(71, 20);
            this.label14.TabIndex = 43;
            this.label14.Text = "Password";
            // 
            // t_password
            // 
            this.t_password.Location = new System.Drawing.Point(10, 124);
            this.t_password.Name = "t_password";
            this.t_password.Size = new System.Drawing.Size(134, 27);
            this.t_password.TabIndex = 17;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 30);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(75, 20);
            this.label13.TabIndex = 43;
            this.label13.Text = "Username";
            // 
            // t_username
            // 
            this.t_username.Location = new System.Drawing.Point(10, 62);
            this.t_username.Name = "t_username";
            this.t_username.Size = new System.Drawing.Size(255, 27);
            this.t_username.TabIndex = 16;
            // 
            // btn_accept_biodata
            // 
            this.btn_accept_biodata.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btn_accept_biodata.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btn_accept_biodata.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_accept_biodata.Location = new System.Drawing.Point(453, 596);
            this.btn_accept_biodata.Name = "btn_accept_biodata";
            this.btn_accept_biodata.Size = new System.Drawing.Size(192, 37);
            this.btn_accept_biodata.TabIndex = 20;
            this.btn_accept_biodata.Text = "Lanjut";
            this.btn_accept_biodata.UseVisualStyleBackColor = false;
            this.btn_accept_biodata.Click += new System.EventHandler(this.btn_accept_biodata_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(4, 320);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(46, 20);
            this.label11.TabIndex = 43;
            this.label11.Text = "Email";
            // 
            // t_email
            // 
            this.t_email.Location = new System.Drawing.Point(8, 352);
            this.t_email.Name = "t_email";
            this.t_email.Size = new System.Drawing.Size(230, 27);
            this.t_email.TabIndex = 12;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 258);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 20);
            this.label10.TabIndex = 41;
            this.label10.Text = "No HP";
            // 
            // t_nohp
            // 
            this.t_nohp.Location = new System.Drawing.Point(7, 290);
            this.t_nohp.Name = "t_nohp";
            this.t_nohp.Size = new System.Drawing.Size(231, 27);
            this.t_nohp.TabIndex = 9;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(292, 199);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(143, 20);
            this.label9.TabIndex = 39;
            this.label9.Text = "Status Kepegawaian";
            // 
            // c_kepegawaian
            // 
            this.c_kepegawaian.DisplayMember = "value";
            this.c_kepegawaian.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.c_kepegawaian.Location = new System.Drawing.Point(296, 231);
            this.c_kepegawaian.Name = "c_kepegawaian";
            this.c_kepegawaian.Size = new System.Drawing.Size(349, 28);
            this.c_kepegawaian.TabIndex = 8;
            this.c_kepegawaian.ValueMember = "key";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(532, 134);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 20);
            this.label3.TabIndex = 37;
            this.label3.Text = "DUK";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(292, 134);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 20);
            this.label8.TabIndex = 37;
            this.label8.Text = "Divisi Kerja";
            // 
            // c_divisi
            // 
            this.c_divisi.DisplayMember = "value";
            this.c_divisi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.c_divisi.Location = new System.Drawing.Point(296, 166);
            this.c_divisi.Name = "c_divisi";
            this.c_divisi.Size = new System.Drawing.Size(234, 28);
            this.c_divisi.TabIndex = 5;
            this.c_divisi.ValueMember = "key";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 196);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 20);
            this.label7.TabIndex = 36;
            this.label7.Text = "Pangkat";
            // 
            // t_pangkat
            // 
            this.t_pangkat.Location = new System.Drawing.Point(7, 228);
            this.t_pangkat.Name = "t_pangkat";
            this.t_pangkat.Size = new System.Drawing.Size(231, 27);
            this.t_pangkat.TabIndex = 7;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(292, 72);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(55, 20);
            this.label18.TabIndex = 33;
            this.label18.Text = "NUPTK";
            // 
            // t_nuptk
            // 
            this.t_nuptk.Location = new System.Drawing.Point(296, 104);
            this.t_nuptk.Name = "t_nuptk";
            this.t_nuptk.Size = new System.Drawing.Size(349, 27);
            this.t_nuptk.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 134);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 20);
            this.label6.TabIndex = 33;
            this.label6.Text = "NIK";
            // 
            // t_nik
            // 
            this.t_nik.Location = new System.Drawing.Point(7, 166);
            this.t_nik.Name = "t_nik";
            this.t_nik.Size = new System.Drawing.Size(231, 27);
            this.t_nik.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 72);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 20);
            this.label5.TabIndex = 31;
            this.label5.Text = "NIP";
            // 
            // t_duk
            // 
            this.t_duk.Location = new System.Drawing.Point(536, 166);
            this.t_duk.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.t_duk.Name = "t_duk";
            this.t_duk.Size = new System.Drawing.Size(109, 27);
            this.t_duk.TabIndex = 6;
            this.t_duk.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // t_nip
            // 
            this.t_nip.Location = new System.Drawing.Point(8, 104);
            this.t_nip.Name = "t_nip";
            this.t_nip.Size = new System.Drawing.Size(230, 27);
            this.t_nip.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(449, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 20);
            this.label4.TabIndex = 29;
            this.label4.Text = "Jenis Kelamin";
            // 
            // c_jk
            // 
            this.c_jk.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.c_jk.Location = new System.Drawing.Point(453, 42);
            this.c_jk.Name = "c_jk";
            this.c_jk.Size = new System.Drawing.Size(192, 28);
            this.c_jk.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 20);
            this.label1.TabIndex = 25;
            this.label1.Text = "Nama";
            // 
            // t_firstname
            // 
            this.t_firstname.Location = new System.Drawing.Point(7, 42);
            this.t_firstname.Name = "t_firstname";
            this.t_firstname.Size = new System.Drawing.Size(415, 27);
            this.t_firstname.TabIndex = 0;
            // 
            // t_lastname
            // 
            this.t_lastname.Location = new System.Drawing.Point(398, 38);
            this.t_lastname.Name = "t_lastname";
            this.t_lastname.Size = new System.Drawing.Size(192, 27);
            this.t_lastname.TabIndex = 1;
            this.t_lastname.Visible = false;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.l_data_karyawan);
            this.flowLayoutPanel1.Controls.Add(this.l_fingerprint);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(4, 71);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(160, 450);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // l_data_karyawan
            // 
            this.l_data_karyawan.Location = new System.Drawing.Point(3, 10);
            this.l_data_karyawan.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.l_data_karyawan.Name = "l_data_karyawan";
            this.l_data_karyawan.Size = new System.Drawing.Size(157, 30);
            this.l_data_karyawan.TabIndex = 0;
            this.l_data_karyawan.Text = "1. Data Karyawan";
            this.l_data_karyawan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // l_fingerprint
            // 
            this.l_fingerprint.Location = new System.Drawing.Point(3, 60);
            this.l_fingerprint.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.l_fingerprint.Name = "l_fingerprint";
            this.l_fingerprint.Size = new System.Drawing.Size(157, 30);
            this.l_fingerprint.TabIndex = 1;
            this.l_fingerprint.Text = "2. Fingerprint";
            this.l_fingerprint.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel_fp
            // 
            this.panel_fp.Controls.Add(this.fp_scan_prompt);
            this.panel_fp.Controls.Add(this.btn_finish);
            this.panel_fp.Controls.Add(this.label17);
            this.panel_fp.Controls.Add(this.fp3);
            this.panel_fp.Controls.Add(this.label16);
            this.panel_fp.Controls.Add(this.fp2);
            this.panel_fp.Controls.Add(this.label15);
            this.panel_fp.Controls.Add(this.fp1);
            this.panel_fp.Location = new System.Drawing.Point(843, 71);
            this.panel_fp.Name = "panel_fp";
            this.panel_fp.Size = new System.Drawing.Size(565, 478);
            this.panel_fp.TabIndex = 4;
            this.panel_fp.Paint += new System.Windows.Forms.PaintEventHandler(this.panel_fp_Paint);
            // 
            // fp_scan_prompt
            // 
            this.fp_scan_prompt.Location = new System.Drawing.Point(14, 60);
            this.fp_scan_prompt.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.fp_scan_prompt.Name = "fp_scan_prompt";
            this.fp_scan_prompt.Size = new System.Drawing.Size(526, 30);
            this.fp_scan_prompt.TabIndex = 48;
            this.fp_scan_prompt.Text = "Fingerprint";
            this.fp_scan_prompt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_finish
            // 
            this.btn_finish.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_finish.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_finish.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_finish.Font = new System.Drawing.Font("Segoe UI Symbol", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_finish.ForeColor = System.Drawing.Color.White;
            this.btn_finish.Location = new System.Drawing.Point(27, 385);
            this.btn_finish.Name = "btn_finish";
            this.btn_finish.Size = new System.Drawing.Size(513, 37);
            this.btn_finish.TabIndex = 47;
            this.btn_finish.Text = "Finish Enrollment";
            this.btn_finish.UseVisualStyleBackColor = false;
            this.btn_finish.Click += new System.EventHandler(this.btn_finish_Click);
            // 
            // label17
            // 
            this.label17.Location = new System.Drawing.Point(429, 278);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(111, 20);
            this.label17.TabIndex = 30;
            this.label17.Text = "Fingerprint ";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // fp3
            // 
            this.fp3.Image = global::Fingerprint_Reader.Properties.Resources.fingerprint;
            this.fp3.Location = new System.Drawing.Point(429, 173);
            this.fp3.Name = "fp3";
            this.fp3.Size = new System.Drawing.Size(111, 102);
            this.fp3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.fp3.TabIndex = 29;
            this.fp3.TabStop = false;
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(222, 278);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(111, 20);
            this.label16.TabIndex = 28;
            this.label16.Text = "Fingerprint ";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // fp2
            // 
            this.fp2.Image = global::Fingerprint_Reader.Properties.Resources.fingerprint;
            this.fp2.InitialImage = global::Fingerprint_Reader.Properties.Resources.waiting;
            this.fp2.Location = new System.Drawing.Point(222, 173);
            this.fp2.Name = "fp2";
            this.fp2.Size = new System.Drawing.Size(111, 102);
            this.fp2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.fp2.TabIndex = 27;
            this.fp2.TabStop = false;
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(23, 278);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(111, 20);
            this.label15.TabIndex = 26;
            this.label15.Text = "Fingerprint ";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // fp1
            // 
            this.fp1.Image = global::Fingerprint_Reader.Properties.Resources.fingerprint;
            this.fp1.InitialImage = global::Fingerprint_Reader.Properties.Resources.waiting;
            this.fp1.Location = new System.Drawing.Point(23, 173);
            this.fp1.Name = "fp1";
            this.fp1.Size = new System.Drawing.Size(111, 102);
            this.fp1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.fp1.TabIndex = 0;
            this.fp1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(59)))), ((int)(((byte)(68)))));
            this.pictureBox1.Image = global::Fingerprint_Reader.Properties.Resources.logo_app;
            this.pictureBox1.Location = new System.Drawing.Point(4, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(86, 60);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // t_rfid_code
            // 
            this.t_rfid_code.Location = new System.Drawing.Point(296, 310);
            this.t_rfid_code.Name = "t_rfid_code";
            this.t_rfid_code.Size = new System.Drawing.Size(231, 27);
            this.t_rfid_code.TabIndex = 10;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(292, 278);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(124, 20);
            this.label21.TabIndex = 41;
            this.label21.Text = "RFID Code (Scan)";
            // 
            // t_rfid_active
            // 
            this.t_rfid_active.AutoSize = true;
            this.t_rfid_active.Location = new System.Drawing.Point(546, 312);
            this.t_rfid_active.Name = "t_rfid_active";
            this.t_rfid_active.Size = new System.Drawing.Size(59, 24);
            this.t_rfid_active.TabIndex = 11;
            this.t_rfid_active.Text = "Aktif";
            this.t_rfid_active.UseVisualStyleBackColor = true;
            // 
            // EnrollUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(841, 730);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.panel_fp);
            this.Controls.Add(this.panel_biodata);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.t_lastname);
            this.Font = new System.Drawing.Font("Segoe UI Symbol", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "EnrollUser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pendaftaran Pegawai";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.EnrollUser_FormClosed);
            this.Load += new System.EventHandler(this.EnrollUser_Load);
            this.panel_biodata.ResumeLayout(false);
            this.panel_biodata.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t_duk)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.panel_fp.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fp3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fp2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fp1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel_biodata;
        private System.Windows.Forms.Button btn_accept_biodata;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox t_email;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox t_nohp;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox c_kepegawaian;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox c_divisi;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox t_pangkat;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox t_nik;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox t_nip;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox c_jk;
        private System.Windows.Forms.TextBox t_lastname;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox t_firstname;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label l_data_karyawan;
        private System.Windows.Forms.Label l_fingerprint;
        private System.Windows.Forms.Panel panel_fp;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.PictureBox fp3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.PictureBox fp2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.PictureBox fp1;
        private System.Windows.Forms.Button btn_finish;
        private System.Windows.Forms.Label fp_scan_prompt;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox t_password;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox t_username;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox t_kelurahan;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox t_kecamatan;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox t_kota;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox t_nuptk;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown t_duk;
        private System.Windows.Forms.CheckBox c_active;
        private System.Windows.Forms.CheckBox t_rfid_active;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox t_rfid_code;
    }
}
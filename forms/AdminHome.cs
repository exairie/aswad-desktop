﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Flurl;
using Flurl.Http;
using Fingerprint_Reader.models;
using DPUruNet;
using Fingerprint_Reader.libs;

namespace Fingerprint_Reader.forms
{
    public partial class AdminHome : Form
    {
        /**
         * From absensi.cs
         * 
         **/
        private bool _reading = false;
        bool Reading
        {
            get
            {
                return _reading;
            }
            set
            {
                waitLoading1.Visible = value;
                //reading.Visible = value;
                _reading = value;
            }
        }

        List<models.Permits> permits = new List<models.Permits>();
        void setupTimer()
        {
            var t = new Timer();
            t.Tick += (sender, e) =>
            {
                //l_fingerprintcount.Text = DateTime.Now.ToString("dd MMM yyyy");
            };
        }

        void LoadDataAbsensi()
        {
            setupTimer();
            //l_fingerprintcount.Visible = false;
            progressBar1.Value = 80;
            DataStore.LoadEnrollments(() =>
            {
                progressBar1.Value = 100;
                progressBar1.Visible = false;
                //l_total.Text = DataStore.enrollments.Count.ToString();
                startScan();
                loadDashboardData();
                var e = DataStore.enrollments;
            });
        }
        void startScan()
        {

        }

        private void Fpreader_On_Captured(CaptureResult result)
        {
            var fmd = FeatureExtraction.CreateFmdFromFid(result.Data, Constants.Formats.Fmd.ANSI).Data;
            Invoke(new Action(() =>
            {
                new ScanComplete(null, fmd).Show();
            }));
        }

        void stopScan()
        {

        }

        private void Absensi_Deactivate(object sender, EventArgs e)
        {

        }

        private void Absensi_Activated(object sender, EventArgs e)
        {
            checkLoginStatus();
        }

        private void checkLoginStatus()
        {
            var isLogin = Program.token != null;
            //button1.Enabled = isLogin;
            button2.Enabled = isLogin;
            //button3.Enabled = isLogin;
            button1.Enabled = isLogin;
            button4.Enabled = isLogin;
            pendaftaranFingerprintToolStripMenuItem.Enabled = isLogin;
            //dataDivisiToolStripMenuItem.Enabled = isLogin;
            kepegawaianToolStripMenuItem.Enabled = isLogin;
            daftarAdminToolStripMenuItem.Enabled = isLogin;
            logOutToolStripMenuItem.Visible = isLogin;
            logInToolStripMenuItem.Visible = !isLogin;
            jadwalWFHToolStripMenuItem.Visible = isLogin;
        }

        /** */
        public AdminHome()
        {
            InitializeComponent();
            if (Program.token == null)
            {
                new Login().ShowDialog();
            }
        }

        private void dataDivisiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new DivisionList().ShowDialog();
        }

        private void AdminHome_FormClosed(object sender, FormClosedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void pendaftaranFingerprintToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!PermissionCheck.checkPermission(Program.AdminSession, PermissionCheck.Permission.ChangeDivisionSettings))
            {
                MessageBox.Show("Anda tidak memiliki hak akses untuk membuka halaman ini!", "Forbidden", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            stopScan();
            new EnrollmentList().ShowDialog();
            startScan();
        }

        private void dataStatusKepegawaianToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new EmploymentStatusList().ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Hide();
            new Absensi().Show();
        }

        private void AdminHome_Load(object sender, EventArgs e)
        {
            loadDashboardData();
            LoadDataAbsensi();
        }

        async void loadDashboardData()
        {
            loadEntranceExit();
            loadAverage();
            loadPermits();
        }

        async void loadEntranceExit()
        {

            try
            {
                var data = await Properties.Settings.Default.serverUrl
                .AppendPathSegments(new string[] { "administrator", "sumtoday" })
                .GetJsonAsync<Summary.FingerprintSum>();

                int totalEntry = data.attendance.Where(x => x.exit_date == null).Count();
                int totalExit = data.attendance.Where(x => x.exit_date != null).Count();
                l_totalentry.Text = totalEntry.ToString();
                l_totalexit.Text = totalExit.ToString();

                var entry = data.attendance.Sum(x => x.entry_delta < 0 ? x.entry_delta : 0);

                l_permits.Text = data.permit.Count().ToString();

                var totalAttendance = data.attendance.Count();

                l_late_avg.Text = ScanComplete.dateToString(entry / totalAttendance);


            }
            catch (Exception e)
            {
                l_late_avg.Text = "0";
            }
        }
        async void loadAverage()
        {
            try
            {
                var warningentry = await Properties.Settings.Default.serverUrl
                   .WithHeader("token", Program.token)
                   .AppendPathSegments(new string[] { "attendance", "warningreport", DateTime.Now.ToString("yyyy-MM-dd") })
                   .GetJsonAsync<Attendances[]>();

                l_autoclose_count.Text = warningentry.Count().ToString();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        async void loadPermits()
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (!PermissionCheck.checkPermission(Program.AdminSession, PermissionCheck.Permission.AddPermit))
            {
                MessageBox.Show("Anda tidak memiliki hak akses untuk membuka halaman ini!", "Forbidden", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            new Permits().ShowDialog();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            new ReportLate().ShowDialog();
        }

        private void logInToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new Login().ShowDialog();
        }

        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Program.AdminSession = null;
            Program.token = null;
            this.checkLoginStatus();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void pengaturanDivisiKerjaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!PermissionCheck.checkPermission(Program.AdminSession, PermissionCheck.Permission.ChangeDivisionSettings))
            {
                MessageBox.Show("Anda tidak memiliki hak akses untuk membuka halaman ini!", "Forbidden", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            new DivisionList().ShowDialog();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            new ClosingWarningReport().ShowDialog();
        }

        private void logAbsensiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new ReportEntry().ShowDialog();
        }

        private void l_late_avg_Click(object sender, EventArgs e)
        {
            new DownloadFileStatus().Show();
        }

        private void daftarAdminToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!PermissionCheck.checkPermission(Program.AdminSession, PermissionCheck.Permission.ChangeDivisionSettings))
            {
                MessageBox.Show("Anda tidak memiliki hak akses untuk membuka halaman ini!", "Forbidden", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            new Admins().ShowDialog();
        }

        private void laporanAbsensiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new ReportLate().ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            new CreateMonthlyReport().ShowDialog();
        }

        private void AdminHome_FormClosing(object sender, FormClosingEventArgs e)
        {
            MessageBox.Show("Anda akan logout!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Program.token = null;
            Program.AdminSession = null;
        }

        private void tanggalLiburToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new Offdays().ShowDialog();
        }

        private void administrasiJamPelajaranToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new HourDefinitions().ShowDialog();
        }

        private void guruPiketToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new Picket().ShowDialog();
        }

        private void jadwalWFHToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new WFHSchedule().ShowDialog();
        }
    }
}

﻿using DPUruNet;
using Fingerprint_Reader.models;
using Flurl;
using Flurl.Http;
using Newtonsoft.Json;
using Quobject.SocketIoClientDotNet.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fingerprint_Reader.forms
{
    public partial class ScanComplete : Form
    {
        public event VoidCallback scanComplete;
        private bool _reading = false;
        private bool useRfid = false;
        bool Reading
        {
            get
            {
                return _reading;
            }
            set
            {
                waitLoading1.Visible = value;
                progressBar1.Visible = value;
                //reading.Visible = value;
                _reading = value;
            }
        }
        public ScanComplete(Enrollments id, bool notifyOnly)
        {
            InitializeComponent();
            this.enrollmentData = id;
            this.notifyOnly = notifyOnly;
            socketClient = Program.socketClient;
        }
        public static void Process(Fmd fmd)
        {
            new ScanComplete(null, fmd).Show();
        }
        public static void Process(Enrollments enrollment, string rfid_code)
        {
            new ScanComplete(enrollment, null, true).Show();
        }
        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            var ips = new List<String>();
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    ips.Add(ip.ToString());
                }
            }

            return string.Join(", ", ips.ToArray());
        }
        public static string dateToString(long secs)
        {
            var h = secs / 3600;
            secs -= h * 3600;
            var m = secs / 60;
            secs -= m * 60;

            string _r = "";
            if (Math.Abs(h) > 0) _r += String.Format("{0} jam ", Math.Abs(h).ToString());
            if (Math.Abs(m) > 0) _r += String.Format("{0} menit ", Math.Abs(m).ToString());
            _r += String.Format("{0} detik ", Math.Abs(secs).ToString());

            return _r;
        }
        public async void processScan(Fmd fmd, BoolCallback completed = null)
        {
            List<Enrollments> tempList = DataStore.enrollments
                .Where(e => e.active != null && e.active.Value == 1).ToList();
            progressBar1.Maximum = tempList.Count;
            new Thread((ThreadStart)delegate
            {
                //Console.WriteLine("Finger Scanned");
                Invoke(new Action(() =>
                {
                    Reading = true;
                }));
                bool match = false;
                foreach (var fdata in tempList)
                {
                    //Console.WriteLine("Testing " + fdata.firstname);
                    Fmd fmdcheck;
                    try
                    {
                        fmdcheck = Fmd.DeserializeXml(fdata.fmd1);
                    }
                    catch (Exception e)
                    {
                        continue;
                    }
                    var checkResult = Comparison.Compare(fmd, 0, fmdcheck, 0).Score;

                    Invoke(new Action(() =>
                    {
                        l_status.Text = "Checking!";
                        //l_status.Text = checkResult.ToString();
                    }));

                    if (checkResult <= 100)
                    {
                        match = true;
                    }

                    fmdcheck = Fmd.DeserializeXml(fdata.fmd2);
                    checkResult = Comparison.Compare(fmd, 0, fmdcheck, 0).Score;

                    if (checkResult <= 100)
                    {
                        match = true;
                    }

                    fmdcheck = Fmd.DeserializeXml(fdata.fmd3);
                    checkResult = Comparison.Compare(fmd, 0, fmdcheck, 0).Score;

                    if (checkResult <= 100)
                    {
                        match = true;
                    }
                    progressBar1.Invoke(new Action(() => { progressBar1.Value++; }));
                    if (match)
                    {
                        this.enrollmentData = fdata;
                        completed?.Invoke(true);
                        break;
                    }
                }
                if (!match)
                {
                    completed?.Invoke(false);
                }
                Invoke(new Action(() =>
                {
                    Reading = false;
                }));
            }).Start();
        }
        Quobject.SocketIoClientDotNet.Client.Socket socketClient = null;
        bool notifyOnly = false;
        private Enrollments enrollmentData = null;
        private Fmd fmd;
        public void setNo(int no)
        {
            this.Text = this.Text + " - " + no;
        }
        public ScanComplete(Enrollments e, Fmd fmd, bool rfid = false)
        {
            InitializeComponent();

            this.enrollmentData = e;
            this.fmd = fmd;
            this.useRfid = rfid;

            socketClient = Program.socketClient;

        }
        void showPicture()
        {
            try
            {
                if (enrollmentData.photo1 != "")
                {
                    pictureBox2.Load(Properties.Settings.Default.serverUrl + "profile/" + enrollmentData.photo1);
                }
                else
                {
                    pictureBox2.Load(Properties.Settings.Default.serverUrl + "profile/nophoto.png");
                }
            }
            catch (Exception)
            {
                //pictureBox2.Load(Properties.Settings.Default.serverUrl + "profile/nophoto.png");
            }
        }
        private void ScanComplete_Load(object sender, EventArgs e)
        {
            //TopMost = true;
            if (this.enrollmentData != null && notifyOnly == true)
            {
                l_status.Text = "Entri Masuk";
                l_nama.Text = String.Format("{0} {1}", enrollmentData.firstname, enrollmentData.lastname);
                l_nip.Text = enrollmentData.nip;
                l_divisi.Text = String.Format("{0}/{1}",
                    enrollmentData.division.division,
                    enrollmentData.employment_status.employment_status);
                getEnrollment();
            }
            if (this.enrollmentData == null && notifyOnly == false)
            {
                this.processScan(this.fmd, (success) =>
                {
                    Invoke(new Action(() =>
                    {
                        processAttendance(true);
                    }));
                });
            }
            if (this.enrollmentData != null && useRfid)
            {
                processAttendance(true);
            }
        }
        void processAttendance(bool success)
        {
            if (success && enrollmentData != null)
            {

                showPicture();
                if (enrollmentData?.wfh_schedules.Count > 0)
                {
                    /** Today, user is WFH */
                    l_status.Text = "Scan Berhasil";
                    l_nama.Text = String.Format("{0} {1}", enrollmentData.firstname, enrollmentData.lastname);
                    l_nip.Text = enrollmentData.nip;
                    l_divisi.Text = String.Format("{0}/{1}",
                        enrollmentData.division.division,
                        enrollmentData.employment_status.employment_status);

                    l_message.Text = String.Format("Anda terjadwal WFH untuk hari ini! ");
                    l_ontime_status.BackColor = Color.Gold;
                    l_ontime_status.ForeColor = Color.Black;
                    l_ontime_status.Text = "Work From Home";

                    waitLoading1.Visible = false;

                    var t = new System.Windows.Forms.Timer();
                    t.Tick += (s, ev) =>
                    {
                        Hide();
                        Dispose();
                        //try
                        //{
                        //    pictureBox2.Image.Dispose();

                        //}
                        //catch (Exception ex)
                        //{
                        //    Console.WriteLine(ex);
                        //}
                    };
                    t.Interval = 3000;
                    t.Start();
                }
                else
                {
                    l_status.Text = "Scan Berhasil";
                    l_nama.Text = String.Format("{0} {1}", enrollmentData.firstname, enrollmentData.lastname);
                    l_nip.Text = enrollmentData.nip;
                    l_divisi.Text = String.Format("{0}/{1}",
                        enrollmentData.division.division,
                        enrollmentData.employment_status.employment_status);

                    waitLoading1.Visible = false;

                    getEnrollment();
                }

            }
            else
            {
                if (Properties.Settings.Default.playAudio)
                {
                    try
                    {
                        axWindowsMediaPlayer1.URL = Directory.GetCurrentDirectory() + "/audio/ohno.mp3";
                        axWindowsMediaPlayer1.Ctlcontrols.play();
                    }
                    catch (Exception ex)
                    {

                    }
                }
                l_message.Text = "Data tidak ditemukan!";
                var t = new System.Windows.Forms.Timer();
                t.Tick += (s, ev) =>
                {
                    Hide();
                    Dispose();
                    //try
                    //{
                    //    pictureBox2.Image.Dispose();

                    //}
                    //catch (Exception ex)
                    //{
                    //    Console.WriteLine(ex);
                    //}
                };
                t.Interval = 3000;
                t.Start();
            }
        }
        bool checkSchedule(PersonalSchedule schedule)
        {
            var today = DateTime.Now.DayOfWeek;
            if (today == DayOfWeek.Monday && schedule.senin_hadir == 0) return false;
            if (today == DayOfWeek.Tuesday && schedule.selasa_hadir == 0) return false;
            if (today == DayOfWeek.Wednesday && schedule.rabu_hadir == 0) return false;
            if (today == DayOfWeek.Thursday && schedule.kamis_hadir == 0) return false;
            if (today == DayOfWeek.Friday && schedule.jumat_hadir == 0) return false;
            return true;
        }
        async void getEnrollment()
        {
            if (enrollmentData.personal_schedule != null)
            {
                if (!checkSchedule(enrollmentData.personal_schedule))
                {
                    noSchedule();
                    return;
                }
            }

            var checkPermit = await Properties.Settings.Default.serverUrl
                    .AppendPathSegments(new string[] { "attendance_log", "inpermit", enrollmentData.id.ToString() })
                    .GetJsonAsync<Permits>();

            if (checkPermit != null)
            {
                //MessageBox.Show("Has permit in " + DateTime.Now.ToString("yyyy-MM-dd"));
                hasPermit();
                return;
            }

            try
            {
                var query = await Properties.Settings.Default.serverUrl
                    .AppendPathSegments(new string[] { "attendance_log" })
                    .AppendPathSegments(new string[] { "today" })
                    .AppendPathSegments(new string[] { enrollmentData.id.ToString() })
                    .GetJsonAsync<models.Attendances>();

                if (query == null)
                {
                    // Entrance
                    validateTime();
                }
                else
                {
                    // Exit
                    query.entry_date = HomePage.setZone(query.entry_date.Value);
                    validateTime(query);

                }
            }
            catch (FlurlHttpException e)
            {
                Console.Write(e);
            }
        }

        async void validateTime(Attendances todayAttendance = null)
        {
            /**
             * Default time is based on division's entry and exit time 
             * */
            var division = this.enrollmentData.division;
            var today = DateTime.Now;
            var entranceToday = new DateTime(today.Year, today.Month, today.Day, (int)division.shift_start_hour, (int)division.shift_start_minute, 59);
            var exitToday = new DateTime(today.Year, today.Month, today.Day, (int)division.shift_end_hour, (int)division.shift_end_minute, 0);

            /**
             * User has personal Schedule. Use that schedule instead 
             */
            if (enrollmentData.personal_schedule != null)
            {
                var cschedule = enrollmentData.personal_schedule;
                if (today.DayOfWeek == DayOfWeek.Monday && cschedule.senin_custom == 1)
                {
                    entranceToday = new DateTime(entranceToday.Year, entranceToday.Month, entranceToday.Day, cschedule.senin_masuk_jam.Value, cschedule.senin_masuk_menit.Value, 0);
                    exitToday = new DateTime(exitToday.Year, exitToday.Month, exitToday.Day, cschedule.senin_keluar_jam.Value, cschedule.senin_keluar_menit.Value, 0);
                }
                else if (today.DayOfWeek == DayOfWeek.Tuesday && cschedule.selasa_custom == 1)
                {
                    entranceToday = new DateTime(entranceToday.Year, entranceToday.Month, entranceToday.Day, cschedule.selasa_masuk_jam.Value, cschedule.selasa_masuk_menit.Value, 0);
                    exitToday = new DateTime(exitToday.Year, exitToday.Month, exitToday.Day, cschedule.selasa_keluar_jam.Value, cschedule.selasa_keluar_menit.Value, 0);
                }
                else if (today.DayOfWeek == DayOfWeek.Wednesday && cschedule.rabu_custom == 1)
                {
                    entranceToday = new DateTime(entranceToday.Year, entranceToday.Month, entranceToday.Day, cschedule.rabu_masuk_jam.Value, cschedule.rabu_masuk_menit.Value, 0);
                    exitToday = new DateTime(exitToday.Year, exitToday.Month, exitToday.Day, cschedule.rabu_keluar_jam.Value, cschedule.rabu_keluar_menit.Value, 0);
                }
                else if (today.DayOfWeek == DayOfWeek.Thursday && cschedule.kamis_custom == 1)
                {
                    entranceToday = new DateTime(entranceToday.Year, entranceToday.Month, entranceToday.Day, cschedule.kamis_masuk_jam.Value, cschedule.kamis_masuk_menit.Value, 0);
                    exitToday = new DateTime(exitToday.Year, exitToday.Month, exitToday.Day, cschedule.kamis_keluar_jam.Value, cschedule.kamis_keluar_menit.Value, 0);
                }
                else if (today.DayOfWeek == DayOfWeek.Friday && cschedule.jumat_custom == 1)
                {
                    entranceToday = new DateTime(entranceToday.Year, entranceToday.Month, entranceToday.Day, cschedule.jumat_masuk_jam.Value, cschedule.jumat_masuk_menit.Value, 0);
                    exitToday = new DateTime(exitToday.Year, exitToday.Month, exitToday.Day, cschedule.jumat_keluar_jam.Value, cschedule.jumat_keluar_menit.Value, 0);
                }
            }

            if (todayAttendance == null)
            {
                /**
                 * New record for attendance
                 * === Entry
                 */
                l_jammasukkeluar.Text = "Jam Masuk";
                l_jammasuk.Text = String.Format("{0}", DateTime.Now.ToString("HH:mm"));
                l_jadwalmasuk.Text = String.Format("{0}", entranceToday.ToString("HH:mm"));
                saveAttendance(entranceToday, exitToday);
            }
            else
            {
                var diffMinutes = Math.Abs((today - todayAttendance.entry_date.Value).TotalSeconds);
                if (diffMinutes < Properties.Settings.Default.min_interval_attendance * 60)
                {
                    l_jammasukkeluar.Text = "Jam Keluar";
                    l_jammasuk.Text = String.Format("{0}", DateTime.Now.ToString("HH:mm"));
                    l_jadwalmasuk.Text = String.Format("{0}", exitToday.ToString("HH:mm"));
                    promptAlreadyEntry((long)diffMinutes);
                    return;
                }
                l_jammasukkeluar.Text = "Jam Keluar";
                l_jammasuk.Text = String.Format("{0}", DateTime.Now.ToString("HH:mm"));
                l_jadwalmasuk.Text = String.Format("{0}", exitToday.ToString("HH:mm"));
                updateAttendance(exitToday, todayAttendance);
            }
        }
        void hasPermit()
        {
            BackColor = Color.Blue;
            l_message.Text = String.Format("Anda tercatat izin! ");
            l_ontime_status.BackColor = Color.Cyan;
            l_ontime_status.ForeColor = Color.Black;
            l_ontime_status.Text = "Izin";

            var timer = new System.Windows.Forms.Timer();
            timer.Interval = 4000;
            timer.Tick += (sender, e) =>
            {
                Hide();
                Dispose();
                try
                {
                    pictureBox2.Image.Dispose();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            };
            timer.Start();
        }
        void noSchedule()
        {
            BackColor = Color.Blue;
            l_message.Text = String.Format("Anda tidak memiliki jadwal masuk! ");
            l_ontime_status.BackColor = Color.Blue;
            l_ontime_status.ForeColor = Color.Black;
            l_ontime_status.Text = "Tidak ada jadwal";

            var timer = new System.Windows.Forms.Timer();
            timer.Interval = 4000;
            timer.Tick += (sender, e) =>
            {
                Hide();
                Dispose();
                try
                {
                    pictureBox2.Image.Dispose();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            };
            timer.Start();
        }
        void promptAlreadyEntry(long diffMinute)
        {
            BackColor = Color.Blue;
            l_message.Text = String.Format("Anda baru masuk " + dateToString(diffMinute) + " yang lalu!");
            l_ontime_status.BackColor = Color.Green;
            l_ontime_status.ForeColor = Color.Black;
            l_ontime_status.Text = "Baru Masuk";

            var timer = new System.Windows.Forms.Timer();
            timer.Interval = 4000;
            timer.Tick += (sender, e) =>
            {
                Hide();
                Dispose();
                try
                {
                    pictureBox2.Image.Dispose();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            };
            timer.Start();
        }
        DateTime createDate(int h, int m, int s = 0)
        {
            DateTime d = DateTime.Now;
            return new DateTime(d.Year, d.Month, d.Day, h, m, s);
        }
        int getWorkHour(Attendances attendance)
        {
            /** Check divisi */
            var division = enrollmentData.division;
            var divEntry = createDate(division.shift_start_hour.Value, division.shift_start_minute.Value, 59);
            var divExit = createDate(division.shift_end_hour.Value, division.shift_end_minute.Value, 59);

            var breakStart = createDate(division.break_hour.Value, division.break_minute.Value, 59);
            var breakEnd = createDate(division.break_hour_end.Value, division.break_minute_end.Value, 59);

            var enter = attendance.entry_date;
            var exit = attendance.exit_date;

            if (enter < divEntry)
            {
                enter = divEntry;
            }
            if (exit != null && exit > divExit)
            {
                exit = divExit;
            }
            var total = (exit.Value - enter.Value).TotalSeconds;


            if (enter < breakStart && exit > breakEnd)
            {
                total -= (breakEnd - breakStart).TotalSeconds;
            }
            else if (enter < breakStart && (exit < breakEnd && exit > breakStart))
            {
                total = (breakStart - enter.Value).TotalSeconds;
            }
            else if ((enter > breakStart && enter < breakEnd) && exit > breakEnd)
            {
                total = (exit.Value - breakEnd).TotalSeconds;
            }

            return (int)total;
        }
        List<DateTime> getInterHourForDivision()
        {
            if (enrollmentData == null) return null;
            var today = DateTime.Now;

            var interStart = new DateTime(today.Year, today.Month, today.Day, (int)enrollmentData.division.inter_shift_start_hour, (int)enrollmentData.division.inter_shift_start_minute, 0);
            var interEnd = new DateTime(today.Year, today.Month, today.Day, (int)enrollmentData.division.inter_shift_end_hour, (int)enrollmentData.division.inter_shift_end_minute, 59);

            return new List<DateTime>() { interStart, interEnd };

        }
        async void updateAttendance(DateTime exitToday, Attendances attendance)
        {
            long entryDelta = 0;
            var now = DateTime.Now;
            var entrance_late = (long)(exitToday - now).TotalSeconds;
            entryDelta = entrance_late * -1;

            /**
             * Check Inter-hour attendance
             * If exitToday is BETWEEN inter-hour attendance then inter-hour will be
             * the same as exit date
             * Else, register inter-hour instead of exit hour
             */

            // Check if exit date is between inter-hour AND exit date is not in inter hour
            var interHours = getInterHourForDivision();
            if (now > interHours[0] && now < interHours[1] && exitToday > interHours[1] && false) // Disable Inter-attendance
            {
                #region Inter-hour flow
                /** Flow for inter hour */
                l_message.Text = String.Format("Terimakasih sudah melakukan absensi ke 2!");
                l_ontime_status.BackColor = Color.Green;
                l_ontime_status.ForeColor = Color.White;
                l_ontime_status.Text = "Uang Saku Aman Terkendali";
                if (attendance.inter_attendance_date != null)
                {
                    /** Already doing inter-attendance */

                    l_message.Text = String.Format("Anda sudah melakukan absensi ke 2!");

                    l_ontime_status.Text = "Aman bro tenang aja";
                    var timer = new System.Windows.Forms.Timer();
                    timer.Interval = 4000;
                    timer.Tick += (sender, e) =>
                    {
                        Hide();
                        Dispose();
                        try
                        {
                            pictureBox2.Image.Dispose();

                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                        }
                    };
                    timer.Start();
                    return;

                }

                attendance.inter_attendance_date = now;
                attendance.inter_attendance_fmd = Fmd.SerializeXml(fmd);

                attendance.inter_attendance_start_hour = enrollmentData.division.inter_shift_start_hour;
                attendance.inter_attendance_start_minute = enrollmentData.division.inter_shift_start_minute;
                attendance.inter_attendance_end_hour = enrollmentData.division.inter_shift_end_hour;
                attendance.inter_attendance_end_minute = enrollmentData.division.inter_shift_end_minute;
                #endregion
            }
            /** 
             * if somehow attendance is taken between inter-hour and
             * exit date is ALSO before inter-hour
             */
            else if (now > interHours[0] && now < interHours[1] && exitToday < interHours[1] && false) // Disable Inter-attendance
            {
                if (attendance.exit_date != null)
                {
                    showAttendanceExitAlready();
                    return;
                }
                attendance.inter_attendance_date = now;
                attendance.inter_attendance_fmd = Fmd.SerializeXml(fmd);

                attendance.inter_attendance_start_hour = enrollmentData.division.inter_shift_start_hour;
                attendance.inter_attendance_start_minute = enrollmentData.division.inter_shift_start_minute;
                attendance.inter_attendance_end_hour = enrollmentData.division.inter_shift_end_hour;
                attendance.inter_attendance_end_minute = enrollmentData.division.inter_shift_end_minute;

                attendance.exit_date = now;
                attendance.exit_fmd = Fmd.SerializeXml(fmd);
                attendance.exit_delta = entryDelta;
                attendance.entry_status = 1;

                checkExitDate(attendance);
            }
            /**
             * if somehow attendance is recorded AFTER inter-hour  
             */
            else
            {
                #region Normal Flow (non-inter)
                if (attendance.exit_date != null)
                {
                    showAttendanceExitAlready();
                    return;
                }

                if (entrance_late < 0)
                {
                    playAudio("applause10.mp3");
                    // Early
                    attendance.entry_status = 0;
                    l_message.Text = String.Format("Anda pulang lebih lambat {0}", dateToString(Math.Abs(entrance_late)));
                    l_ontime_status.BackColor = Color.Green;
                    l_ontime_status.ForeColor = Color.White;
                    l_ontime_status.Text = "Ontime";
                    //changeAllText(Color.Black);
                    BackColor = Color.Green;
                }
                else if (entrance_late > 0)
                {
                    playAudio("ohno.mp3");
                    // Late
                    attendance.entry_status = -1;
                    l_message.Text = String.Format("Anda pulang lebih awal {0}", dateToString(Math.Abs(entrance_late)));
                    l_ontime_status.BackColor = Color.Red;
                    l_ontime_status.ForeColor = Color.White;
                    l_ontime_status.Text = "Late";
                    //changeAllText(Color.White);
                    BackColor = Color.FromArgb(255, 32, 27); ;
                }
                else
                {
                    playAudio("applause10.mp3");
                    // Unlikely, right on time
                    attendance.entry_status = 0;
                    l_message.Text = String.Format("Anda tepat waktu");
                    l_ontime_status.BackColor = Color.Green;
                    l_ontime_status.ForeColor = Color.White;
                    l_ontime_status.Text = "Bingo!";
                    //changeAllText(Color.Black);
                    BackColor = Color.Green;
                }
                if (notifyOnly)
                {
                    waitLoading1.Visible = false;
                    var timer = new System.Windows.Forms.Timer();
                    timer.Interval = 4000;
                    timer.Tick += (sender, e) =>
                    {
                        Hide();
                        Dispose();
                        try
                        {
                            pictureBox2.Image.Dispose();

                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                        }
                    };
                    timer.Start();


                    Program.notifyAttendanceDataUpdated();
                    return;
                }

                /** Attendance exit calculations */
                attendance.exit_date = now;
                if (fmd != null)
                {
                    attendance.exit_fmd = Fmd.SerializeXml(fmd);
                }
                attendance.exit_delta = entryDelta;
                attendance.entry_status = 1;

                checkExitDate(attendance);

                attendance.total_work_sec = getWorkHour(attendance);
                #endregion
            }




            try
            {
                var req = Properties.Settings.Default.serverUrl
                .AppendPathSegments(new string[] { "attendance_log" })
                .AppendPathSegments(new string[] { "entry" })
                .AppendPathSegments(new string[] { attendance.id.ToString() });

                var query = await req.PutJsonAsync(attendance);

                if (query.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    socketClient.Emit("notifier", JsonConvert.SerializeObject(new
                    {
                        action = "new_attendance",
                        attendant = enrollmentData.id,
                        origin = Program.socket_id
                    }));
                    waitLoading1.Visible = false;
                    var timer = new System.Windows.Forms.Timer();
                    timer.Interval = 4000;
                    timer.Tick += (sender, e) =>
                    {
                        Hide();
                        Dispose();
                        try
                        {

                            pictureBox2.Image.Dispose();

                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                        }
                    };
                    timer.Start();


                    Program.notifyAttendanceDataUpdated();
                }
                else
                {
                    var dialog = MessageBox.Show("Unable to save data! Retry?", "Error", MessageBoxButtons.RetryCancel, MessageBoxIcon.Exclamation);
                    if (dialog == DialogResult.Retry)
                    {
                        updateAttendance(exitToday, attendance);
                    }
                }
            }
            catch (FlurlHttpException e)
            {
                Console.Write(e);
            }

        }

        private void showAttendanceExitAlready()
        {
            l_message.Text = String.Format("Anda sudah tercatat pulang!");
            l_ontime_status.BackColor = Color.Green;
            l_ontime_status.ForeColor = Color.White;
            l_ontime_status.Text = "Sudah pulang";

            var timer = new System.Windows.Forms.Timer();
            timer.Interval = 4000;
            timer.Tick += (sender, e) =>
            {
                Hide();
                Dispose();
                try
                {
                    pictureBox2.Image.Dispose();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            };
            timer.Start();
            if (!notifyOnly)
            {
                socketClient.Emit("notifier", JsonConvert.SerializeObject(new
                {
                    action = "new_attendance",
                    attendant = enrollmentData.id,
                    origin = Program.socket_id
                }));
            }
        }

        void changeAllText(Color color, Control.ControlCollection control = null)
        {
            control = control == null ? this.Controls : control;
            foreach (Control c in control)
            {
                if (c is Label)
                {
                    c.ForeColor = color;
                }
                else if (c.Controls.Count > 0)
                {
                    changeAllText(color, c.Controls);
                }
            }
        }
        void checkEntryDate(Attendances attendance)
        {
            var division = enrollmentData.division;
            var divEntry = createDate(division.shift_start_hour.Value, division.shift_start_minute.Value);
            var divExit = createDate(division.shift_end_hour.Value, division.shift_end_minute.Value);
            var breakStart = createDate(division.break_hour.Value, division.break_minute.Value);
            var breakEnd = createDate(division.break_hour_end.Value, division.break_minute_end.Value);

            if (attendance.entry_date > breakStart && attendance.entry_date < breakEnd)
            {
                attendance.entry_date = breakEnd;
            }
        }
        void checkExitDate(Attendances attendance)
        {
            var division = enrollmentData.division;
            var divEntry = createDate(division.shift_start_hour.Value, division.shift_start_minute.Value);
            var divExit = createDate(division.shift_end_hour.Value, division.shift_end_minute.Value);
            var breakStart = createDate(division.break_hour.Value, division.break_minute.Value);
            var breakEnd = createDate(division.break_hour_end.Value, division.break_minute_end.Value);

            if (attendance.exit_date < breakEnd && attendance.exit_date > breakStart)
            {
                attendance.exit_date = breakStart;
            }
        }
        void playAudio(String audio)
        {
            if (Properties.Settings.Default.playAudio)
            {
                try
                {
                    axWindowsMediaPlayer1.URL = Directory.GetCurrentDirectory() + "/audio/" + audio;
                    axWindowsMediaPlayer1.Ctlcontrols.play();
                }
                catch (Exception ex)
                {

                }
            }
        }
        async void saveAttendance(DateTime entranceToday, DateTime exitToday)
        {
            long entryDelta = 0;
            var now = DateTime.Now;
            now.AddSeconds(-now.Second);
            var entrance_late = (long)(now - entranceToday).TotalSeconds;
            entryDelta = entrance_late * -1;

            var attendance = new Attendances();
            attendance.shift_start_hour = entranceToday.Hour;
            attendance.shift_end_hour = exitToday.Hour;
            attendance.shift_start_minute = entranceToday.Minute;
            attendance.shift_end_minute = exitToday.Minute;

            attendance.position = Environment.MachineName.ToString() + "/" + GetLocalIPAddress();

            attendance.entry_date = now;
            attendance.enrollment_id = enrollmentData.id;

            checkEntryDate(attendance);

            try
            {
                attendance.fmd = Fmd.SerializeXml(fmd);
            }
            catch (Exception e) { }

            attendance.entry_delta = entryDelta;

            if (entrance_late < 0)
            {
                playAudio("applause10.mp3");
                // Early
                attendance.entry_status = 0;
                l_message.Text = String.Format("Anda lebih awal {0}", dateToString(Math.Abs(entrance_late)));
                l_ontime_status.BackColor = Color.Green;
                l_ontime_status.ForeColor = Color.White;
                this.BackColor = Color.Green;
                l_ontime_status.Text = "Ontime";
                //changeAllText(Color.Black);
            }
            else if (entrance_late > 0)
            {
                // Late
                playAudio("ohno.mp3");
                attendance.entry_status = -1;
                l_message.Text = String.Format("Anda terlambat {0}", dateToString(Math.Abs(entrance_late)));
                l_ontime_status.BackColor = Color.Red;
                l_ontime_status.ForeColor = Color.White;
                l_ontime_status.Text = "Late";
                this.BackColor = Color.FromArgb(255, 32, 27);
                //changeAllText(Color.White);
            }
            else
            {
                playAudio("applause10.mp3");
                // Unlikely, right on time
                attendance.entry_status = 0;
                l_message.Text = String.Format("Anda tepat waktu");
                l_ontime_status.BackColor = Color.Green;
                l_ontime_status.ForeColor = Color.White;
                l_ontime_status.Text = "Bingo!";
                this.BackColor = Color.Green;
                //changeAllText(Color.Black);
            }
            if (notifyOnly)
            {
                waitLoading1.Visible = false;
                var timer = new System.Windows.Forms.Timer();
                timer.Interval = 4000;
                timer.Tick += (sender, e) =>
                {
                    Hide();
                    Dispose();
                    try
                    {
                        pictureBox2.Image.Dispose();

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                };
                timer.Start();
                Program.notifyAttendanceDataUpdated();
                return;
            }
            try
            {
                var req = Properties.Settings.Default.serverUrl
                    .AppendPathSegments(new string[] { "attendance_log" })
                    .AppendPathSegments(new string[] { "entry" });

                var query = await req.PostJsonAsync(attendance);


                if (query.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    socketClient.Emit("notifier", JsonConvert.SerializeObject(new
                    {
                        action = "new_attendance",
                        attendant = enrollmentData.id,
                        origin = Program.socket_id
                    }));
                    waitLoading1.Visible = false;
                    var timer = new System.Windows.Forms.Timer();
                    timer.Interval = 4000;
                    timer.Tick += (sender, e) =>
                    {
                        Hide();
                        Dispose();
                        try
                        {
                            pictureBox2.Image.Dispose();

                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                        }
                    };
                    timer.Start();
                    Program.notifyAttendanceDataUpdated();
                }
            }
            catch (FlurlHttpException e)
            {
                dynamic c = await e.GetResponseJsonAsync();
                MessageBox.Show(e.Message);
            }
            finally
            {

            }
        }

        private void waitLoading1_Click(object sender, EventArgs e)
        {

        }

        private void groupBox7_Enter(object sender, EventArgs e)
        {

        }

        private void l_jammasuk_Click(object sender, EventArgs e)
        {

        }

        private void ScanComplete_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void ScanComplete_FormClosed(object sender, FormClosedEventArgs e)
        {

        }
    }
}

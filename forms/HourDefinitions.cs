﻿using Flurl;
using Flurl.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fingerprint_Reader.forms
{
    public partial class HourDefinitions : Form
    {
        public HourDefinitions()
        {
            InitializeComponent();
        }

        private void HourDefinitions_Load(object sender, EventArgs e)
        {
            dataGridView1.AutoGenerateColumns = false;
            setupCombobox();
        }
        void setupCombobox()
        {
            List<int> hour = new List<int>();
            for(int i = 0;i < 13; i++)
            {
                hour.Add(i + 1);
            }
            cb_jam.DataSource = hour;

            Dictionary<string, string> hari = new Dictionary<string, string>();
            hari.Add("senin", "Senin");
            hari.Add("selasa", "Selasa");
            hari.Add("rabu", "Rabu");
            hari.Add("kamis", "Kamis");
            hari.Add("jumat", "Jumat");

            cb_hari.ValueMember = "Key";
            cb_hari.DisplayMember = "Value";
            cb_hari.DataSource = hari.ToArray();
        }
        async void loadData()
        {
            try
            {
                var data = await Properties.Settings.Default.serverUrl
                    .AppendPathSegment("hour_definition")
                    .WithHeader("token", Program.token)
                    .GetJsonAsync<models.HourDefinition[]>();
                
                dataGridView1.DataSource = new List<models.HourDefinition>(data);
            }
            catch (Exception)
            {

                if (MessageBox.Show("Gagal mengambil data! Coba lagi?", "Info", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes) {
                    loadData();
                };
            }
        }
        bool validate()
        {
            var hourPattern = @"\d{2}\:\d{2}";
            if (Regex.Match(t_start.Text, hourPattern).Length < 1)
            {
                return false;
            }
            if (Regex.Match(t_end.Text, hourPattern).Length < 1)
            {
                return false;
            }
            return true;
        }
        async void update()
        {
            if (!validate())
            {
                MessageBox.Show("Periksa kembali input.\nWaktu mulai dan selesai harus sesuai dengan format [00:00]\nContoh 08:00, 08:30, 09:00", "Error",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
                return;
            }
            try
            {
                var resp = await Properties.Settings.Default.serverUrl
                    .WithHeader("token",Program.token)
                    .AppendPathSegments(new string[] { "hour_definition", "update" })
                    .PostJsonAsync(new
                    {
                        hour = (int)cb_jam.SelectedValue,
                        day = cb_hari.SelectedValue.ToString(),
                        definition = String.Format("{0} - {1}", t_start.Text, t_end.Text)
                    });
                if(resp.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    //MessageBox.Show("Data telah diupdate!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    loadData();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Data gagal diupdate!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            update();
        }

        private void HourDefinitions_Activated(object sender, EventArgs e)
        {
            loadData();
        }
    }
}

﻿namespace Fingerprint_Reader.forms
{
    partial class CreateMonthlyReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreateMonthlyReport));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.c_tahun = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.c_bulan = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.b_ok = new System.Windows.Forms.Button();
            this.c_divisi = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(59)))), ((int)(((byte)(68)))));
            this.pictureBox1.Image = global::Fingerprint_Reader.Properties.Resources.logo_app;
            this.pictureBox1.Location = new System.Drawing.Point(4, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(79, 57);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(59)))), ((int)(((byte)(68)))));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(0, -1);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(709, 60);
            this.label2.TabIndex = 16;
            this.label2.Text = "Buat Laporan Bulanan";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // c_tahun
            // 
            this.c_tahun.FormattingEnabled = true;
            this.c_tahun.Location = new System.Drawing.Point(12, 102);
            this.c_tahun.Name = "c_tahun";
            this.c_tahun.Size = new System.Drawing.Size(185, 26);
            this.c_tahun.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 18);
            this.label1.TabIndex = 19;
            this.label1.Text = "Pilih Tahun";
            // 
            // c_bulan
            // 
            this.c_bulan.FormattingEnabled = true;
            this.c_bulan.Location = new System.Drawing.Point(201, 102);
            this.c_bulan.Name = "c_bulan";
            this.c_bulan.Size = new System.Drawing.Size(185, 26);
            this.c_bulan.TabIndex = 18;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(201, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 18);
            this.label3.TabIndex = 19;
            this.label3.Text = "Pilih Bulan";
            // 
            // b_ok
            // 
            this.b_ok.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.b_ok.Location = new System.Drawing.Point(581, 102);
            this.b_ok.Name = "b_ok";
            this.b_ok.Size = new System.Drawing.Size(115, 26);
            this.b_ok.TabIndex = 20;
            this.b_ok.Text = "Buat Laporan";
            this.b_ok.UseVisualStyleBackColor = true;
            this.b_ok.Click += new System.EventHandler(this.b_ok_Click);
            // 
            // c_divisi
            // 
            this.c_divisi.FormattingEnabled = true;
            this.c_divisi.Location = new System.Drawing.Point(392, 102);
            this.c_divisi.Name = "c_divisi";
            this.c_divisi.Size = new System.Drawing.Size(185, 26);
            this.c_divisi.TabIndex = 18;
            this.c_divisi.SelectedIndexChanged += new System.EventHandler(this.c_divisi_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(392, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 18);
            this.label4.TabIndex = 19;
            this.label4.Text = "Pilih Divisi";
            // 
            // CreateMonthlyReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(708, 140);
            this.Controls.Add(this.b_ok);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.c_divisi);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.c_bulan);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.c_tahun);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label2);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CreateMonthlyReport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Laporan Bulanan";
            this.Load += new System.EventHandler(this.CreateMonthlyReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox c_tahun;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox c_bulan;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button b_ok;
        private System.Windows.Forms.ComboBox c_divisi;
        private System.Windows.Forms.Label label4;
    }
}
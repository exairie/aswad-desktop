﻿using Fingerprint_Reader.models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fingerprint_Reader.forms
{
    public delegate void OnEnrollmentSelected(Enrollments e);
    public partial class SelectEnrollment : Form
    {
        public event OnEnrollmentSelected enrollmentSelected;
        List<Enrollments> enrollments;
        public SelectEnrollment(string title, OnEnrollmentSelected selected)
        {
            InitializeComponent();
            this.enrollmentSelected += selected;
        }

        private void SelectEnrollment_Load(object sender, EventArgs e)
        {
            dataGridView1.AutoGenerateColumns = false;
            DataStore.LoadEnrollments(() =>
            {
                this.enrollments = DataStore.enrollments.ToList();
                Invoke(new Action(() =>
                {
                    dataGridView1.DataSource = this.enrollments;
                }));
            });
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if(textBox1.Text == "")
            {
                this.enrollments = DataStore.enrollments.ToList();
            }else
            {
                this.enrollments = (from l in DataStore.enrollments
                                    where l.firstname.ToLower().Contains(textBox1.Text.ToLower())
                                    select l).ToList();
            }

            Invoke(new Action(() =>
            {
                dataGridView1.DataSource = this.enrollments;
            }));
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0) return;
            if (e.ColumnIndex< 0) return;
            if (dataGridView1[e.ColumnIndex, e.RowIndex] is DataGridViewButtonCell)
            {
                var datasource = (List<Enrollments>)dataGridView1.DataSource;
                this.enrollmentSelected(datasource[e.RowIndex]);
                Hide();
                Dispose();
            }
        }
    }
}

﻿using DPUruNet;
using Fingerprint_Reader.models;
using Flurl;
using Flurl.Http;
using Newtonsoft.Json;
using Quobject.SocketIoClientDotNet.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fingerprint_Reader.forms
{
    public partial class ScanView : Control
    {
        public event VoidCallback scanComplete;
        private bool _reading = false;
        bool Reading
        {
            get
            {
                return _reading;
            }
            set
            {
                waitLoading1.Visible = value;
                progressBar1.Visible = value;
                //reading.Visible = value;
                _reading = value;
            }
        }
        public static void Process(Fmd fmd)
        {
            new ScanComplete(null, fmd).Show();
        }
        public static string dateToString(long secs)
        {
            var h = secs / 3600;
            secs -= h * 3600;
            var m = secs / 60;
            secs -= m * 60;

            string _r = "";
            if (Math.Abs(h) > 0) _r += String.Format("{0} jam ", Math.Abs(h).ToString());
            if (Math.Abs(m) > 0) _r += String.Format("{0} menit ", Math.Abs(m).ToString());
            _r += String.Format("{0} detik ", Math.Abs(secs).ToString());

            return _r;
        }
        public async void processScan(Fmd fmd, BoolCallback completed = null)
        {            
            progressBar1.Maximum = DataStore.enrollments.Count;
            new Thread((ThreadStart)delegate
            {
                Console.WriteLine("Finger Scanned");
                Invoke(new Action(() =>
                {
                    Reading = true;
                }));
                bool match = false;
                foreach (var fdata in DataStore.enrollments)
                {
                    Console.WriteLine("Testing " + fdata.firstname);
                    Fmd fmdcheck;
                    try
                    {
                        fmdcheck = Fmd.DeserializeXml(fdata.fmd1);
                    }
                    catch(Exception e)
                    {
                        continue;
                    }
                    var checkResult = Comparison.Compare(fmd, 0, fmdcheck, 0).Score;

                    Invoke(new Action(() =>
                    {
                        l_status.Text = "Checking!";
                        //l_status.Text = checkResult.ToString();
                    }));

                    if (checkResult <= 100)
                    {
                        match = true;
                    }

                    fmdcheck = Fmd.DeserializeXml(fdata.fmd2);
                    checkResult = Comparison.Compare(fmd, 0, fmdcheck, 0).Score;

                    if (checkResult <= 100)
                    {
                        match = true;
                    }

                    fmdcheck = Fmd.DeserializeXml(fdata.fmd3);
                    checkResult = Comparison.Compare(fmd, 0, fmdcheck, 0).Score;

                    if (checkResult <= 100)
                    {
                        match = true;
                    }
                    progressBar1.Invoke(new Action(() => { progressBar1.Value++; }));
                    if (match)
                    {
                        this.enrollmentData = fdata;
                        completed?.Invoke(true);
                        break;
                    }
                }
                if (!match)
                {
                    completed?.Invoke(false);
                }
                Invoke(new Action(() =>
                {                    
                    Reading = false;
                }));
            }).Start();            
        }
        Socket socketClient = null;
        private Enrollments enrollmentData = null;
        private Fmd fmd;
        public ScanView(Enrollments e, Fmd fmd)
        {
            InitializeComponent();

            this.enrollmentData = e;
            this.fmd = fmd;

            socketClient = IO.Socket(Properties.Settings.Default.serverUrl);

            ScanComplete_Load(null, null);

        }

        private void ScanComplete_Load(object sender, EventArgs e)
        {            
            if (this.enrollmentData == null)
            {
                this.processScan(this.fmd, (success) => {
                    Invoke(new Action(() =>
                    {
                        if (success)
                        {
                            l_status.Text = "Scan Berhasil";
                            l_nama.Text = String.Format("{0} {1}", enrollmentData.firstname, enrollmentData.lastname);
                            l_nip.Text = enrollmentData.nip;
                            l_divisi.Text = String.Format("{0}/{1}",
                                enrollmentData.division.division,
                                enrollmentData.employment_status.employment_status);

                            waitLoading1.Visible = false;
                            getEnrollment();
                        }
                        else
                        {
                            l_message.Text = "Data tidak ditemukan!";
                            var t = new System.Windows.Forms.Timer();
                            t.Tick += (s, ev) =>
                            {
                                Hide();
                                Dispose();
                            };
                            t.Interval = 3000;
                            t.Start();
                        }
                    }));
                });
            }
        }

        async void getEnrollment() {
            try
            {
                var query = await Properties.Settings.Default.serverUrl
                    .AppendPathSegments(new string[] { "attendance_log" })
                    .AppendPathSegments(new string[] { "today" })
                    .AppendPathSegments(new string[] { enrollmentData.id.ToString() })
                    .GetJsonAsync<models.Attendances>();  
                
                if(query == null)
                {
                    // Entrance
                    validateTime();
                }else
                {
                    // Exit
                    validateTime(query);

                }
            }
            catch (FlurlHttpException e)
            {
                Console.Write(e);
            }
        }

        async void validateTime(Attendances todayAttendance = null)
        {
            var division = this.enrollmentData.division;
            var today = DateTime.Now;
            var entranceToday = new DateTime(today.Year, today.Month, today.Day, (int)division.shift_start_hour, 0, 0);
            var exitToday = new DateTime(today.Year, today.Month, today.Day, (int)division.shift_end_hour, 0, 0);

            if (todayAttendance == null)
            {
                /**
                 * New record for attendance
                 * === Entry
                 */
                l_jammasukkeluar.Text = "Jam Masuk";
                l_jammasuk.Text = String.Format("{0}", entranceToday.ToString("HH:mm"));
                saveAttendance(entranceToday);
            }else
            {
                l_jammasukkeluar.Text = "Jam Keluar";
                l_jammasuk.Text = String.Format("{0}", exitToday.ToString("HH:mm"));
                updateAttendance(exitToday, todayAttendance);
            }
        }
        async void updateAttendance(DateTime exitToday, Attendances attendance)
        {
            long entryDelta = 0;
            var now = DateTime.Now;
            var entrance_late = (long)(exitToday - now).TotalSeconds;
            entryDelta = entrance_late * -1;

            attendance.exit_date = now;
            attendance.exit_fmd = Fmd.SerializeXml(fmd);
            attendance.exit_delta = entryDelta;
            attendance.entry_status = 1;

            if (entrance_late < 0)
            {
                // Early
                attendance.entry_status = 0;
                l_message.Text = String.Format("Anda pulang lebih lambat {0}", dateToString(Math.Abs(entrance_late)));
                l_ontime_status.BackColor = Color.Green;
                l_ontime_status.ForeColor = Color.Black;
                l_ontime_status.Text = "Ontime";
            }
            else if (entrance_late > 0)
            {
                // Late
                attendance.entry_status = -1;
                l_message.Text = String.Format("Anda pulang lebih awal {0}", dateToString(Math.Abs(entrance_late)));
                l_ontime_status.BackColor = Color.Red;
                l_ontime_status.ForeColor = Color.White;
                l_ontime_status.Text = "Late";
            }
            else
            {
                // Unlikely, right on time
                attendance.entry_status = 0;
                l_message.Text = String.Format("Anda tepat waktu");
                l_ontime_status.BackColor = Color.Green;
                l_ontime_status.ForeColor = Color.Black;
                l_ontime_status.Text = "Bingo!";
            }

            try
            {
                var req = Properties.Settings.Default.serverUrl
                .AppendPathSegments(new string[] { "attendance_log" })
                .AppendPathSegments(new string[] { "entry" })
                .AppendPathSegments(new string[] { attendance.id.ToString() });

                var query = await req.PutJsonAsync(attendance);

                if (query.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    socketClient.Emit("notifier", JsonConvert.SerializeObject(new
                    {
                        action = "new_attendance",
                        attendant = enrollmentData.id
                    }));
                    waitLoading1.Visible = false;
                    var timer = new System.Windows.Forms.Timer();
                    timer.Interval = 4000;
                    timer.Tick += (sender, e) =>
                    {
                        Hide();
                        Dispose();
                    };
                    timer.Start();


                    Program.notifyAttendanceDataUpdated();
                }
                else
                {
                    var dialog = MessageBox.Show("Unable to save data! Retry?", "Error", MessageBoxButtons.RetryCancel, MessageBoxIcon.Exclamation);
                    if(dialog == DialogResult.Retry)
                    {
                        updateAttendance(exitToday, attendance);
                    }
                }
            }catch(FlurlHttpException e)
            {
                Console.Write(e);
            }

        }
        async void saveAttendance(DateTime entranceToday)
        {
            long entryDelta = 0;
            var now = DateTime.Now;
            var entrance_late = (long)(now - entranceToday).TotalSeconds;
            entryDelta = entrance_late * -1;

            var attendance = new Attendances();
            attendance.shift_start_hour = enrollmentData.division.shift_start_hour;
            attendance.shift_end_hour = enrollmentData.division.shift_end_hour;
            attendance.shift_start_minute = enrollmentData.division.shift_start_minute;
            attendance.shift_end_minute = enrollmentData.division.shift_end_minute;
            attendance.shift_end_hour = enrollmentData.division.shift_end_hour;
            attendance.entry_date = now;
            attendance.enrollment_id = enrollmentData.id;
            attendance.fmd = Fmd.SerializeXml(fmd);

            attendance.entry_delta = entryDelta;

            if (entrance_late < 0)
            {
                // Early
                attendance.entry_status = 0;
                l_message.Text = String.Format("Anda lebih awal {0}", dateToString(Math.Abs(entrance_late)));
                l_ontime_status.BackColor = Color.Green;
                l_ontime_status.ForeColor = Color.Black;
                l_ontime_status.Text = "Ontime";
            }
            else if (entrance_late > 0)
            {
                // Late
                attendance.entry_status = -1;
                l_message.Text = String.Format("Anda terlambat {0}", dateToString(Math.Abs(entrance_late)));
                l_ontime_status.BackColor = Color.Red;
                l_ontime_status.ForeColor = Color.White;
                l_ontime_status.Text = "Late";
            }
            else
            {
                // Unlikely, right on time
                attendance.entry_status = 0;
                l_message.Text = String.Format("Anda tepat waktu");
                l_ontime_status.BackColor = Color.Green;
                l_ontime_status.ForeColor = Color.Black;
                l_ontime_status.Text = "Bingo!";
            }

            try
            {
                var req = Properties.Settings.Default.serverUrl
                .AppendPathSegments(new string[] { "attendance_log" })
                .AppendPathSegments(new string[] { "entry" });

                var query = await req.PostJsonAsync(attendance);
                

                if (query.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    socketClient.Emit("notifier", JsonConvert.SerializeObject(new
                    {
                        action = "new_attendance",
                        attendant = enrollmentData.id
                    }));
                    waitLoading1.Visible = false;
                    var timer = new System.Windows.Forms.Timer();
                    timer.Interval = 4000;
                    timer.Tick += (sender, e) =>
                    {
                        Hide();
                        Dispose();
                    };
                    timer.Start();
                    Program.notifyAttendanceDataUpdated();
                }
            }catch(FlurlHttpException e)
            {
                dynamic c = await e.GetResponseJsonAsync();
                Console.Write(e);
            }
            finally
            {

            }
        }

        private void waitLoading1_Click(object sender, EventArgs e)
        {

        }

        private void groupBox7_Enter(object sender, EventArgs e)
        {

        }

        private void l_jammasuk_Click(object sender, EventArgs e)
        {

        }
    }
}

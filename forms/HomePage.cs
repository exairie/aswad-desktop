﻿using DPUruNet;
using Fingerprint_Reader.models;
using Flurl;
using Flurl.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CefSharp;
using CefSharp.WinForms;
using Fingerprint_Reader.interfaces;
using System.Diagnostics;
using Fingerprint_Reader.libs;
using Newtonsoft.Json;

namespace Fingerprint_Reader.forms
{
    #region enums
    enum FilterAbsen
    {
        Masuk = 1,
        Izin = 2,
        Semua = 3,
        BelumMasuk = 4
    }
    enum FilterMasuk
    {
        TepatWaktu = 1,
        Terlambat = 2,
        Semua = 3
    }
    enum FilterIzin
    {
        IzinLain = 1,
        IzinDinas = 2,
        Sakit = 3,
        Semua = 4
    }
    #endregion
    public partial class HomePage : Form
    {
        public delegate void VoidCallback();
        /**
         * From absensi.cs
         * 
         **/
        private bool _reading = false;
        bool Reading
        {
            get
            {
                return _reading;
            }
            set
            {
                //waitLoading1.Visible = value;
                //reading.Visible = value;
                _reading = value;
            }
        }
        DateTime lastKeyPressed = DateTime.Now;
        string keyBuffer = "";
        private Reader fpreader;
        public static List<Reader> fpreaders = new List<Reader>();
        List<models.Permits> permits = new List<models.Permits>();
        bool capturing_fingerprint = false;
        bool usingBrowser = false;
        Rectangle screenBounds;
        int screenOffset = 0;
        int scannerNo = 0;
        ChromiumWebBrowser browser;

        public static event VoidCallback SystemReloadNotifier;

        DateTime lastActivityScan = DateTime.Now;

        Timer timerOffdayCheck = new Timer();

        Offday offdayData = null;

        int tWidth = -1;
        int exptwidth = -1;
        List<Attendances> data = new List<Attendances>();
        List<models.Permits> permitData = new List<models.Permits>();
        List<GridData> gridData = new List<GridData>();
        List<GridData> filtered = new List<GridData>();

        int browserLoadTimeoutCount = 0;
        bool browserLoadSuccess = false;


        public static string normalizeDate(DateTime d)
        {
            return (d + TimeZoneInfo.Local.GetUtcOffset(d)).ToString("HH:mm");
        }
        public static DateTime setZone(DateTime d)
        {
            return d + TimeZoneInfo.Local.GetUtcOffset(d);
        }
        ReaderCollection readers = null;

        void setupTimer()
        {
            var t = new Timer();
            t.Tick += (sender, e) =>
            {
                if (!usingBrowser)
                {
                    label14.Text = String.Format("Data Hari Ini - {0}", DateTime.Now.ToString("dd MMM yyyy"));
                }
                setupFingerprint();
            };
            t.Interval = 1000;
            t.Start();

            timerOffdayCheck.Tick += TimerOffdayCheck_Tick;
            timerOffdayCheck.Interval = 60 * 60 * 1000;
            timerOffdayCheck.Start();

            var timerRfidReset = new Timer();
            timerRfidReset.Interval = 1000;
            timerRfidReset.Tick += (a, b) =>
            {
                if ((DateTime.Now - lastActivityScan).Seconds > 1)
                {
                    keyBuffer = "";
                    lastActivityScan = DateTime.Now;

                    try
                    {
                        browser.ExecuteScriptAsync("window.setRFIDStatus", new object[] { 0 });
                    }
                    catch (Exception e)
                    {
                        Console.Write(e);
                    }
                    Console.WriteLine("Resetting RFID Buffer wait time");
                }
            };

            timerRfidReset.Start();
        }

        async private void TimerOffdayCheck_Tick(object sender, EventArgs e)
        {
            try
            {
                var resp = await Properties.Settings.Default.serverUrl
                    .AppendPathSegments(new string[] { "offdays", "today" })
                    .GetAsync();
                if (resp.StatusCode == System.Net.HttpStatusCode.PartialContent)
                {
                    offdayData = null;
                }
                else
                {
                    offdayData = JsonConvert.DeserializeObject<Offday>(await resp.Content.ReadAsStringAsync());
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("Unable to check offday data " + ex.Message);
            }
        }

        void LoadDataAbsensi()
        {
            //progressBar1.Visible = true;
            DataStore.LoadWFHSchedules(DateTime.Now, () => { });
            DataStore.LoadEnrollments(() =>
            {
                Invoke(new Action(() => { Text = "Aswad v" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version + " [" + DataStore.enrollments?.Count + "] FP"; }));
                //startScan();
                if (!usingBrowser)
                {
                    //progressBar1.Visible = false;
                    //l_total.Text = DataStore.enrollments.Count.ToString();
                    //loadStaticData();
                    //var e = DataStore.enrollments;
                    //Invoke(new Action(() =>
                    //{
                    //    loadAttendance();
                    //}));
                }
            }, () =>
            {
                try
                {
                    Invoke(new Action(() => { Text = "Aswad v" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version + " | Reloading..."; }));
                    Task.Delay(3000).ContinueWith((task) => { LoadDataAbsensi(); });
                }
                catch (Exception e)
                {

                }
            });
        }
        #region draw fpdevice
        void drawDevice()
        {
            flow_readercount.Controls.Clear();
            foreach (var r in readers)
            {
                Console.WriteLine("Drawing ");
                var imgreader = new PictureBox();
                imgreader.Image = global::Fingerprint_Reader.Properties.Resources.fingerprint;
                imgreader.Location = new System.Drawing.Point(3, 3);
                imgreader.Name = "finger_stat";
                imgreader.Size = new System.Drawing.Size(50, 50);
                imgreader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
                imgreader.TabIndex = 0;
                imgreader.TabStop = false;

                flow_readercount.Controls.Add(imgreader);
            }

        }
        #endregion
        void startScan(ReaderCollection cReaders = null)
        {
            if (cReaders == null)
                readers = ReaderCollection.GetReaders();
            else readers = cReaders;

            fpreaders = new List<Reader>(readers);

            drawDevice();
            if (readers.Count < 1)
            {
                Console.WriteLine("No FP device detected!");
                return;
            }

            foreach (var fpreader in fpreaders)
            {
                //fpreader = readers.First()

                fpreader.On_Captured += Fpreader_On_Captured;
                var fpresult = fpreader.Open(Constants.CapturePriority.DP_PRIORITY_EXCLUSIVE);
                fpreader.CaptureAsync(Constants.Formats.Fid.ISO, Constants.CaptureProcessing.DP_IMG_PROC_DEFAULT, 500);
                Console.WriteLine("Adding FP");
                Console.WriteLine(fpreader.GetStatus());
                //l_status.Text = "SCAN STARTED";
            }
            capturing_fingerprint = true;
        }
        private bool isOffdayWeekend()
        {
            Console.WriteLine("Checking Weekend Settings");
            var isOff = (DateTime.Now.DayOfWeek == DayOfWeek.Saturday || DateTime.Now.DayOfWeek == DayOfWeek.Sunday) && Properties.Settings.Default.weekendoffday;
            try
            {
                browser.ExecuteScriptAsync("window.setFingerprintOffWeekend", isOff.ToString().ToLower());
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return isOff;
        }
        private void Fpreader_On_Captured(CaptureResult result)
        {
            var isWeekend = isOffdayWeekend();
            if (isWeekend)
            {
                return;
            }
            if (offdayData != null)
            {
                //MessageBox.Show("Libur bang");
                return;
            }
            Console.WriteLine("Captured");
            if (!capturing_fingerprint) return;

            var fmd = FeatureExtraction.CreateFmdFromFid(result.Data, Constants.Formats.Fmd.ANSI).Data;
            Invoke(new Action(() =>
            {
                try
                {
                    TopMost = true;
                    var scanner = new ScanComplete(null, fmd);
                    TopMost = false;
                    scanner.TopMost = true;
                    scanner.StartPosition = FormStartPosition.Manual;
                    scanner.Disposed += (e, i) =>
                    {
                        Console.WriteLine("SCanner no = " + scannerNo);
                    };
                    scanner.Shown += (s, e) =>
                    {
                    };
                    Point scannerLocation = new Point();
                    //scanner.setNo(scannerNo++);
                    lastActivityScan = DateTime.Now;

                    var scannerPos = scannerNo++ % fpreaders.Count;
                    scanner.setNo(scannerPos);
                    scannerLocation.X = (screenBounds.Width / fpreaders.Count * scannerPos) + screenOffset - (scanner.Width / 2);
                    scannerLocation.Y = screenBounds.Height / 2 - (scanner.Height / 2);
                    scanner.Location = scannerLocation;

                    scanner.scanComplete += () =>
                    {
                        loadAttendance();
                    };
                    scanner.Show();
                }
                catch (Exception e)
                {

                }
                finally
                {
                }
            }));

        }

        void stopScan()
        {

            foreach (var r in fpreaders)
            {
                //r.On_Captured -= Fpreader_On_Captured;
            }
        }

        private void Absensi_Deactivate(object sender, EventArgs e)
        {
            capturing_fingerprint = false;
        }

        private void Absensi_Activated(object sender, EventArgs e)
        {
            capturing_fingerprint = true;

        }

        /** */

        public HomePage()
        {
            usingBrowser = Properties.Settings.Default.usingBrowser;
            InitializeComponent();
            if (usingBrowser)
            {
                Process[] processes = Process.GetProcessesByName("CefSharp.BrowserSubprocess");
                foreach (var process in processes)
                {
                    Console.WriteLine("Killing active subprocess");
                    process.Kill();
                }
                var browserUrl = "";
                if (Properties.Settings.Default.browserDebug && !Properties.Settings.Default.localIndexFile)
                {
                    browserUrl = Properties.Settings.Default.debugUrl;
                }
                else if (Properties.Settings.Default.localIndexFile)
                {
                    browserUrl = "customscheme://indexfile.com/index.html";
                }
                else
                {
                    browserUrl = Properties.Settings.Default.serverUrl + "#/dashboard";

                }

                var settings = new CefSettings();
                settings.RegisterScheme(new CefCustomScheme
                {
                    SchemeName = LocalProtocolSchemeHandlerFactory.SchemeName,
                    SchemeHandlerFactory = new LocalProtocolSchemeHandlerFactory()
                });

                Cef.Initialize(settings);

                Console.WriteLine(browserUrl);
                browser = new ChromiumWebBrowser(browserUrl);
                browser.IsBrowserInitializedChanged += (e, init) =>
                {
                    if (init.IsBrowserInitialized)
                    {
                        if (Properties.Settings.Default.browserDebug)
                        {
                            browser.ShowDevTools();
                        }
                        drawFpInBrowser();

                        browser.ExecuteScriptAsyncWhenPageLoaded("setServerUrl('" + Properties.Settings.Default.serverUrl + "')");
                    }
                };
                Controls.Clear();
                Controls.Add(browser);

                Timer timerReloader = new Timer();
                timerReloader.Tick += (e, a) =>
                {
                    if (!browserLoadSuccess)
                    {
                        browserLoadTimeoutCount++;
                    }
                    if (browserLoadTimeoutCount >= 30 && !browserLoadSuccess)
                    {
                        SystemReloadNotifier?.Invoke();
                    }
                    else
                    {
                        /** Do nothing. Reloaded Successfully */
                        return;
                    }
                };

                SystemReloadNotifier += () =>
                {
                    Console.WriteLine("Reloading Frontend...");
                    browser?.Reload(true);
                    browserLoadTimeoutCount = 0;
                    browserLoadSuccess = false;
                };
                Program.systemReloadNotified += (data) =>
                {
                    browser?.Reload(true);
                    browserLoadTimeoutCount = 0;
                    browserLoadSuccess = false;
                };

                BrowserTriggers triggers = new BrowserTriggers();
                triggers.PageLoadCallback += () =>
                {
                    browserLoadSuccess = true;
                    Console.WriteLine("Browser Load Success!");
                };
                triggers.StartAdmin += () =>
                {
                    Invoke(new Action(() =>
                    {
                        button2_Click(null, null);
                    }));
                };
                triggers.StartDispensasi += () =>
                {
                    Invoke(new Action(() =>
                    {
                        button1_Click(null, null);
                    }));
                };
                browser.JavascriptObjectRepository.Register("Interface", triggers);

            }



            if (!usingBrowser)
            {
                dataGridView1.AutoGenerateColumns = false;

                Program.DataUpdate += loadAttendance;

            }
            LoadDataAbsensi();
            TimerOffdayCheck_Tick(null, null);
            setupTimer();
        }
        void fillChartGuru()
        {
            // Guru
            var tGuru = (from d in DataStore.enrollments
                         where d.division_id == "guru"
                         join p in permitData on d.id equals p.enrollment_id into pid
                         select d).Count();
            var guruDispen = (from d in permitData
                              where d.enrollment.division_id == "guru"
                              && d.permit_type == "izin"
                              select d).Count();
            var guruIzin = (from d in permitData
                            where d.enrollment.division_id == "guru"
                            && d.permit_type == "izin_lain"
                            select d).Count();
            var guruSakit = (from d in permitData
                             where d.enrollment.division_id == "guru"
                             && d.permit_type == "sakit"
                             select d).Count();
            var guruSudahAbsen = (from d in data
                                  where d.enrollment.division_id == "guru"
                                  select d).Count();

            tGuru -= (guruDispen + guruIzin + guruSakit + guruSudahAbsen);
            tGuru = Math.Max(0, tGuru);

            chart1.Series["Guru"].Points.Clear();
            chart1.Series["Guru"].Points.AddXY(tGuru + " Belum Absen", tGuru);
            chart1.Series["Guru"].Points[chart1.Series["Guru"].Points.Count - 1].Color = Color.Red;
            if (guruSudahAbsen > 0)
            {
                chart1.Series["Guru"].Points.AddXY(guruSudahAbsen + " Sudah Absen", guruSudahAbsen);
                chart1.Series["Guru"].Points[chart1.Series["Guru"].Points.Count - 1].Color = Color.Green;
            }
            if (guruDispen > 0)
            {
                chart1.Series["Guru"].Points.AddXY(guruDispen + " Dispensasi", guruDispen);
                chart1.Series["Guru"].Points[chart1.Series["Guru"].Points.Count - 1].Color = Color.Blue;
            }
            if (guruIzin > 0)
            {
                chart1.Series["Guru"].Points.AddXY(guruIzin + " Izin", guruIzin);
                chart1.Series["Guru"].Points[chart1.Series["Guru"].Points.Count - 1].Color = Color.Orange;
            }
            if (guruSakit > 0)
            {
                chart1.Series["Guru"].Points.AddXY(guruSakit + " Sakit", guruSakit);
                chart1.Series["Guru"].Points[chart1.Series["Guru"].Points.Count - 1].Color = Color.Yellow;
            }

            chart1.Series["Guru"]["PieLabelStyle"] = "disabled";

            //// Belum Absen
            //chart1.Series["Guru"].Points[chart1.Series["Guru"].Points.Count - 1].Color = Color.Red;
            //// Sudah Absen
            //chart1.Series["Guru"].Points[1].Color = Color.Green;
            //// Dispen
            //chart1.Series["Guru"].Points[2].Color = Color.Blue;
            //// Izin
            //chart1.Series["Guru"].Points[3].Color = Color.Orange;
            //// Sakit
            //chart1.Series["Guru"].Points[4].Color = Color.Yellow;
        }
        void fillChartTU()
        {
            var total = (from d in DataStore.enrollments
                         where d.division_id == "tata_usaha"
                         select d).Count();
            var dispen = (from d in permitData
                          where d.enrollment.division_id == "tata_usaha"
                          && d.permit_type == "izin"
                          select d).Count();
            var izin = (from d in permitData
                        where d.enrollment.division_id == "tata_usaha"
                        && d.permit_type == "izin_lain"
                        select d).Count();
            var sakit = (from d in permitData
                         where d.enrollment.division_id == "tata_usaha"
                         && d.permit_type == "sakit"
                         select d).Count();
            var sudahAbsen = (from d in data
                              where d.enrollment.division_id == "tata_usaha"
                              select d).Count();

            total -= (dispen + izin + sakit + sudahAbsen);
            total = Math.Max(0, total);

            chart2.Series["TU"].Points.Clear();
            chart2.Series["TU"].Points.AddXY(total + " Belum Absen", total);
            chart2.Series["TU"].Points[chart2.Series["TU"].Points.Count - 1].Color = Color.Red;
            if (sudahAbsen > 0)
            {
                chart2.Series["TU"].Points.AddXY(sudahAbsen + " Sudah Absen", sudahAbsen);
                chart2.Series["TU"].Points[chart2.Series["TU"].Points.Count - 1].Color = Color.Green;
                chart2.Series["TU"]["PieLabelStyle"] = "disabled";

            }
            if (dispen > 0)
            {
                chart2.Series["TU"].Points.AddXY(dispen + " Dispensasi", dispen);
                chart2.Series["TU"].Points[chart2.Series["TU"].Points.Count - 1].Color = Color.Blue;
            }
            if (izin > 0)
            {
                chart2.Series["TU"].Points.AddXY(izin + " Izin", izin);
                chart2.Series["TU"].Points[chart2.Series["TU"].Points.Count - 1].Color = Color.Orange;
            }

            if (sakit > 0)
            {
                chart2.Series["TU"].Points.AddXY(sakit + " Sakit", sakit);
                chart2.Series["TU"].Points[chart2.Series["TU"].Points.Count - 1].Color = Color.Yellow;
            }

            chart2.Series["TU"]["PieLabelStyle"] = "disabled";


        }
        void loadStaticData()
        {
            l_tpegawai.Text = DataStore.enrollments.Count().ToString();
            l_tpendidik.Text = DataStore.enrollments.Count(e => e.division_id == "guru").ToString();
            l_tkependidikan.Text = DataStore.enrollments.Count(e => e.division_id == "tata_usaha").ToString();

            l_absen.Text = data.Count().ToString();
            l_absen_kependidikan.Text = data.Count(e => e.enrollment.division_id == "tata_usaha").ToString();
            l_absen_pendidik.Text = data.Count(e => e.enrollment.division_id == "guru").ToString();

        }
        async void loadAttendance()
        {
            try
            {
                Console.WriteLine("Updating dasbhoard");
                if (waitLoading1.Visible)
                {
                    Console.WriteLine("Already in progress. Not going to do anything");
                    return;
                }
                waitLoading1.Visible = true;
                Console.Write("Fetching Attendance Log ... ");
                var data = await Properties.Settings.Default.serverUrl
                    .AppendPathSegments(new string[] { "attendance_log", "logtoday" })
                    .GetJsonAsync<Attendances[]>();
                Console.WriteLine("Attendance log Completed");

                this.data.Clear();
                this.data.AddRange(data);

                Console.Write("Fetching Permit Log ... ");

                var permitData = await Properties.Settings.Default.serverUrl
                    .AppendPathSegments(new string[] { "attendance_log", "permits" })
                    .GetJsonAsync<models.Permits[]>();

                Console.WriteLine("Permit Fetch Completed");

                this.permitData.Clear();
                this.permitData.AddRange(permitData);

                Console.Write("Init Charts ... ");
                fillChartGuru();
                fillChartTU();

                Console.WriteLine("Completed");

                loadStaticData();

                preprocessData();
            }
            catch (FlurlHttpException e)
            {
                Console.WriteLine(e);
                loadAttendance();
            }
        }
        string entryStatus(Attendances a)
        {
            if (a.entry_auto == -2)
            {
                return "Tidak masuk";
            }
            else if (a.entry_auto == 1)
            {
                return "Tugas Keluar";
            }
            else if (a.closing_auto == -2)
            {
                return "Tidak masuk";
            }
            else if (a.closing_auto == -1)
            {
                return "Tidak absen keluar";
            }
            else if (a.closing_auto == null && a.exit_date == null)
            {
                return "Sudah masuk";
            }
            else if (a.closing_auto == null && a.exit_date != null)
            {
                return "Sudah keluar";
            }
            else
            {
                return "N/A";
            }
        }
        #region preprocessData
        async void preprocessData()
        {
            Console.WriteLine("Preprocessing...");
            FilterAbsen filterAbsen = FilterAbsen.Semua;
            FilterMasuk filterMasuk = FilterMasuk.Semua;
            FilterIzin filterIzin = FilterIzin.Semua;

            Invoke(new Action(() =>
            {
                var cfilterAbsen = c_absen.SelectedValue.ToString();
                var cfilterMasuk = c_masuk.SelectedValue.ToString();
                var cfilterIzin = c_izin.SelectedValue.ToString();
                Enum.TryParse<FilterAbsen>(cfilterAbsen, out filterAbsen);
                Enum.TryParse<FilterMasuk>(cfilterMasuk, out filterMasuk);
                Enum.TryParse<FilterIzin>(cfilterIzin, out filterIzin);
            }));
            if (filterAbsen == FilterAbsen.Masuk)
            {
                if (filterMasuk == FilterMasuk.TepatWaktu)
                {
                    gridData = (from d in data
                                orderby d.updated_at descending
                                where d.entry_delta >= 0
                                select new GridData()
                                {
                                    name = d.enrollment.firstname + " " + d.enrollment.lastname,
                                    detail = entryStatus(d),
                                    enter = normalizeDate(d.entry_date.Value) + (d.exit_date != null ? " - " + normalizeDate(d.exit_date.Value) : ""),
                                    exit = "",
                                    status = "Tepat Waktu",
                                    date = d.updated_at.Value
                                }).ToList();
                }
                else if (filterMasuk == FilterMasuk.Terlambat)
                {
                    gridData = (from d in data
                                orderby d.updated_at descending
                                where d.entry_delta < 0
                                select new GridData()
                                {
                                    name = d.enrollment.firstname + " " + d.enrollment.lastname,
                                    detail = entryStatus(d),
                                    enter = normalizeDate(d.entry_date.Value) + (d.exit_date != null ? " - " + normalizeDate(d.exit_date.Value) : ""),
                                    exit = "",
                                    status = "Terlambat",
                                    date = d.updated_at.Value
                                }).ToList();
                }
                else
                {
                    gridData = (from d in data
                                orderby d.updated_at descending
                                where d.entry_auto == null || d.entry_auto.Value >= 0
                                select new GridData()
                                {
                                    name = d.enrollment.firstname + " " + d.enrollment.lastname,
                                    detail = entryStatus(d),
                                    enter = normalizeDate(d.entry_date.Value) + (d.exit_date != null ? " - " + normalizeDate(d.exit_date.Value) : ""),
                                    exit = "",
                                    status = d.entry_delta >= 0 ? "Tepat Waktu" : "Terlambat",
                                    date = d.updated_at.Value
                                }).ToList();
                }
            }
            else if (filterAbsen == FilterAbsen.BelumMasuk)
            {
                gridData = (from enroll in DataStore.enrollments
                            join att in data on enroll.id equals att.enrollment_id into ps
                            from p in ps.DefaultIfEmpty()
                            where ps.Count() < 1
                            select new GridData()
                            {
                                name = enroll.firstname,
                                detail = "Belum Datang",
                                date = DateTime.Now,
                                enter = "-"
                            }).ToList();
            }
            else if (filterAbsen == FilterAbsen.Izin)
            {

                if (filterIzin == FilterIzin.IzinDinas)
                {
                    gridData = (from d in permitData
                                orderby d.permit_from descending
                                where d.permit_type == "izin"
                                select new GridData()
                                {
                                    name = d.enrollment.firstname + " " + d.enrollment.lastname,
                                    detail = getPermitDetailDesc(d.permit_type),
                                    enter = d.permit_from.Value.ToString("dd MMM yyyy"),
                                    date = d.permit_from.Value

                                }).ToList();
                }
                else if (filterIzin == FilterIzin.IzinLain)
                {
                    gridData = (from d in permitData
                                orderby d.permit_from descending
                                where d.permit_type == "izin_lain"
                                select new GridData()
                                {
                                    name = d.enrollment.firstname + " " + d.enrollment.lastname,
                                    detail = getPermitDetailDesc(d.permit_type),
                                    enter = d.permit_from.Value.ToString("dd MMM yyyy"),
                                    date = d.permit_from.Value

                                }).ToList();
                }
                else if (filterIzin == FilterIzin.Sakit)
                {
                    gridData = (from d in permitData
                                orderby d.permit_from descending
                                where d.permit_type == "sakit"
                                select new GridData()
                                {
                                    name = d.enrollment.firstname + " " + d.enrollment.lastname,
                                    detail = getPermitDetailDesc(d.permit_type),
                                    enter = d.permit_from.Value.ToString("dd MMM yyyy"),
                                    date = d.permit_from.Value

                                }).ToList();
                }
                else
                {
                    gridData = (from d in permitData
                                orderby d.permit_from descending
                                select new GridData()
                                {
                                    name = d.enrollment.firstname + " " + d.enrollment.lastname,
                                    detail = getPermitDetailDesc(d.permit_type),
                                    enter = d.permit_from.Value.ToString("dd MMM yyyy"),
                                    date = d.permit_from.Value

                                }).ToList();
                }
            }
            else if (filterAbsen == FilterAbsen.Semua)
            {

                gridData.Clear();
                var attendanceData = (from d in data
                                      select new GridData()
                                      {
                                          name = d.enrollment.firstname + " " + d.enrollment.lastname,
                                          detail = entryStatus(d),
                                          enter = normalizeDate(d.entry_date.Value) + (d.exit_date != null ? " - " + normalizeDate(d.exit_date.Value) : ""),
                                          exit = "",
                                          status = "OK",
                                          date = d.updated_at.Value
                                      }).ToList();
                foreach (var a in attendanceData)
                {
                    gridData.Add(a);
                }
                var permits = (from d in permitData
                               select new GridData()
                               {
                                   name = d.enrollment.firstname + " " + d.enrollment.lastname,
                                   detail = getPermitDetailDesc(d.permit_type),
                                   enter = d.permit_from.Value.ToString("dd MMM yyyy"),
                                   date = d.permit_from.Value

                               }).ToList();
                foreach (var a in permits)
                {
                    gridData.Add(a);
                }
                gridData.AddRange((from enroll in DataStore.enrollments
                                   join att in data on enroll.id equals att.enrollment_id into ps
                                   from p in ps.DefaultIfEmpty()
                                   where ps.Count() < 1
                                   select new GridData()
                                   {
                                       name = enroll.firstname,
                                       detail = "Belum Datang",
                                       date = DateTime.Now,
                                       enter = "-"
                                   }).ToList());
                gridData = (from g in gridData orderby g.date descending select g).ToList();
            }
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = gridData;
            dataGridView1.Refresh();
            waitLoading1.Visible = false;
        }
        #endregion
        string getPermitDetailDesc(string permit_type)
        {
            switch (permit_type)
            {
                case "izin":
                    return "Izin (Dinas)";
                    break;
                case "izin_lain":
                    return "Izin (Keperluan Lain)";
                    break;
                case "sakit":
                    return "sakit";
                    break;
                default:
                    return "Tanpa keterangan";
            }
        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void HomePage_Resize(object sender, EventArgs e)
        {
            if (usingBrowser) return;
            resizePanel();
        }

        private void HomePage_Load(object sender, EventArgs e)
        {
            GlobalKeylogger.OnKeyPressedGlobally += KeyLogger_OnKeyPressedGlobally;

            GlobalKeylogger.Setup();

            Text = "Aswad v" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            screenBounds = Screen.FromControl(this).Bounds;

            Timer timerScannerNoReset = new Timer();
            timerScannerNoReset.Tick += TimerScannerNoReset_Tick;
            timerScannerNoReset.Interval = 3000;
            timerScannerNoReset.Start();

            Timer dataReloader = new Timer();
            dataReloader.Tick += (a, b) => { LoadDataAbsensi(); };
            dataReloader.Interval = 60 * 1000; // 1 Minute
            dataReloader.Start();


            Program.scanNotified += (enrollments) =>
            {
                //Invoke(new Action(() =>
                //{
                //    new ScanComplete(enrollments, true).Show();
                //}));
            };
            Program.updateNotified += (socketid) =>
            {
                try
                {
                    Invoke(new Action(() =>
                    {
                        LoadDataAbsensi();
                        if (usingBrowser)
                        {
                            try
                            {
                                browser.ExecuteScriptAsync("window.reloadData()");
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex);
                            }
                        }
                    }));
                }
                catch (Exception)
                {
                };
            };
            if (usingBrowser)
            {
                return;
            }
            resizePanel();

            setupFilter();

            loadAttendance();


            chart1.Legends[0].Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Right;
            chart1.Legends[0].Alignment = StringAlignment.Center;
            chart2.Legends[0].Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Right;
            chart2.Legends[0].Alignment = StringAlignment.Center;
            //chart1.Legends[0].Position = new System.Windows.Forms.DataVisualization.Charting.ElementPosition(20, 20, 50, 50);



        }
        void checkEnrollmentRFID(string rfcode)
        {
            var filter = DataStore.enrollments.Where(x => x.rfid_code == rfcode.Replace("\n", "").Replace("\r", ""));
            try
            {
                browser.ExecuteScriptAsync("window.setRFIDStatus", new object[] { -2 });
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
            if (filter.Count() > 0)
            {
                try
                {
                    browser.ExecuteScriptAsync("window.setRFIDStatus", new object[] { 1 });
                }
                catch (Exception e)
                {
                    Console.Write(e);
                }
                var enrollment = filter.First();
                if (enrollment.rfid_allowed == 0) return;

                ScanComplete.Process(enrollment, rfcode);
            }
            else
            {
                try
                {
                    browser.ExecuteScriptAsync("window.setRFIDStatus", new object[] { -1 });
                }
                catch (Exception e)
                {
                    Console.Write(e);
                }
            }
        }

        private void KeyLogger_OnKeyPressedGlobally(string key)
        {
            Console.WriteLine("Received Input : " + ((int)key[0] == 13));
            if ((int)key[0] == 13)
            {
                checkEnrollmentRFID(keyBuffer);
                keyBuffer = "";
            }
            else
            {
                keyBuffer += key;
            }
            lastActivityScan = DateTime.Now;
            try
            {
                browser.ExecuteScriptAsync("window.setVideoModeFalse", new object[] { });
                Console.WriteLine("Stopping Video...");
            }
            catch (Exception e)
            {
                Console.Write(e);
                Console.Write(e.StackTrace);
            }
        }

        private void TimerScannerNoReset_Tick(object sender, EventArgs e)
        {
            isOffdayWeekend();
            if (lastActivityScan == null) return;
            if (Math.Abs((DateTime.Now - lastActivityScan).TotalSeconds) > 30)
            {
                scannerNo = 0;
            }
        }
        #region resizePanel
        void resizePanel()
        {
            tWidth = dataGridView1.Width;
            exptwidth = tWidth / 2;

            var c1x = chart1.Location.X;
            var c2x = c1x + exptwidth;

            chart1.Width = exptwidth - 5;
            chart2.Width = exptwidth - 5;

            chart1.Location = new Point(c1x, chart1.Location.Y);
            chart2.Location = new Point(c2x + 5, chart1.Location.Y);


            foreach (var c in flowLayoutPanel2.Controls)
            {
                var p = c as Control;
                if (p != null)
                {
                    p.Width = flowLayoutPanel2.Width;
                }
            }
        }
        #endregion

        private void button2_Click(object sender, EventArgs e)
        {
            Absensi_Deactivate(sender, e);
            new AdminHome().ShowDialog();
            Absensi_Activated(sender, e);
        }

        private void label14_Click(object sender, EventArgs e)
        {

        }
        #region filter setup
        void setupFilter()
        {
            var absenFilter = new Dictionary<FilterAbsen, string>();
            absenFilter.Add(FilterAbsen.Semua, "Semua data");
            absenFilter.Add(FilterAbsen.Masuk, "Sudah Masuk");
            absenFilter.Add(FilterAbsen.Izin, "Izin");
            absenFilter.Add(FilterAbsen.BelumMasuk, "Belum Masuk");

            var masukFilter = new Dictionary<FilterMasuk, string>();
            masukFilter.Add(FilterMasuk.Semua, "Semua data");
            masukFilter.Add(FilterMasuk.TepatWaktu, "Tepat Waktu");
            masukFilter.Add(FilterMasuk.Terlambat, "Terlambat");

            var izinFilter = new Dictionary<FilterIzin, string>();
            izinFilter.Add(FilterIzin.Semua, "Semua data");
            izinFilter.Add(FilterIzin.IzinDinas, "Izin (Keperluan dinas)");
            izinFilter.Add(FilterIzin.IzinLain, "Izin (Keperluan pribadi)");
            izinFilter.Add(FilterIzin.Sakit, "Sakit");


            c_absen.DataSource = absenFilter.ToList();
            c_izin.DataSource = izinFilter.ToList();
            c_masuk.DataSource = masukFilter.ToList();
        }
        #endregion        

        private void c_absen_SelectedValueChanged(object sender, EventArgs e)
        {
            checkBoxControl();

            loadAttendance();
        }
        #region checkboxcontrol
        void checkBoxControl()
        {
            FilterAbsen filterAbsen = FilterAbsen.Semua;
            FilterMasuk filterMasuk = FilterMasuk.Semua;
            FilterIzin filterIzin = FilterIzin.Semua;
            var cfilterAbsen = c_absen.SelectedValue.ToString();
            var cfilterMasuk = (c_masuk.SelectedValue == null ? "Semua" : c_masuk.SelectedValue).ToString();
            var cfilterIzin = (c_izin.SelectedValue == null ? "Semua" : c_izin.SelectedValue).ToString();
            Enum.TryParse<FilterAbsen>(cfilterAbsen, out filterAbsen);
            Enum.TryParse<FilterMasuk>(cfilterAbsen, out filterMasuk);
            Enum.TryParse<FilterIzin>(cfilterAbsen, out filterIzin);
            c_masuk.Visible = filterAbsen == FilterAbsen.Masuk;
            c_izin.Visible = filterAbsen == FilterAbsen.Izin;
        }
        #endregion

        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void HomePage_Activated(object sender, EventArgs e)
        {
            //Absensi_Activated(sender, e);
            DataStore.LoadPermits(() => { });
            if (!usingBrowser)
            {
                loadAttendance();
            }
            else
            {
                try
                {
                    browser.ExecuteScriptAsync("window.reloadData()");
                }
                catch (Exception ex)
                {

                }
            }
        }

        private void HomePage_Deactivate(object sender, EventArgs e)
        {
            //Absensi_Deactivate(sender, e);
        }
        void setupFingerprint()
        {
            Console.WriteLine("Checking Fingerprint");
            try
            {
                readers = ReaderCollection.GetReaders();
                foreach (var r in readers)
                {
                    Console.WriteLine(r.GetStatus());
                }
            }
            catch (Exception e)
            {

            }
            drawFpInBrowser(readers.Count);
            //Console.WriteLine("Found " + readers.Count);
            //Console.WriteLine("Last {0} Now {1}", fpreaders.Count, readers.Count);

            if (fpreaders.Count != readers.Count)
            {
                Console.WriteLine("Restarting scanner....");
                foreach (var r in fpreaders)
                {
                    r.Reset();
                    r.On_Captured -= Fpreader_On_Captured;
                }
                startScan(readers);
            }
            try
            {
                screenOffset = screenBounds.Width / fpreaders.Count / 2;
            }
            catch (DivideByZeroException e)
            {
                screenOffset = 0;
            }
        }
        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }
        void drawFpInBrowser(int count = -1)
        {
            if (usingBrowser)
            {
                try
                {
                    browser.ExecuteScriptAsync("window.setFingerPrintCount", count == -1 ? fpreaders.Count : count);
                }
                catch (Exception e)
                {

                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataStore.LoadPermits(() => { });
            var p = new PermitInput(true);
            p.ReopenForm += () => { button1_Click(null, null); };
            p.ShowDialog();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (textBox1.Text == "")
            {
                dataGridView1.DataSource = gridData;
            }
            else
            {
                filtered = (from d in gridData
                            where d.name.ToLower().Contains(textBox1.Text.ToLower())
                            select d).ToList();
                dataGridView1.DataSource = filtered;
            }
        }

        private void HomePage_FormClosing(object sender, FormClosingEventArgs e)
        {
            foreach (var r in fpreaders)
            {
                r.Dispose();
            }
            GlobalKeylogger.Stop();
            Cef.Shutdown();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }


    }
}

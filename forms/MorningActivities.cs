﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fingerprint_Reader.forms
{
    public partial class MorningActivities : Form
    {
        public MorningActivities()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void MorningActivities_Load(object sender, EventArgs e)
        {
            dataGridView1.AutoGenerateColumns = false;
            setupCombobox();
        }
        void setupCombobox()
        {
            List<int> hour = new List<int>();
            for (int i = 0; i < 13; i++)
            {
                hour.Add(i + 1);
            }
            cb_hour.DataSource = hour;

            Dictionary<string, string> hari = new Dictionary<string, string>();
            hari.Add("senin", "Senin");
            hari.Add("selasa", "Selasa");
            hari.Add("rabu", "Rabu");
            hari.Add("kamis", "Kamis");
            hari.Add("jumat", "Jumat");

            cb_hari.ValueMember = "Key";
            cb_hari.DisplayMember = "Value";
            cb_hari.DataSource = hari.ToArray();

            List<int> week = new List<int>();
            for (int i = 0; i < 4; i++)
            {
                week.Add(i + 1);
            }
            cb_week.DataSource = hour;
        }

        private void cb_hari_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}

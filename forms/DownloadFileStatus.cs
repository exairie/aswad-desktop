﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fingerprint_Reader.forms
{
    public partial class DownloadFileStatus : Form
    {
        public DownloadFileStatus()
        {
            InitializeComponent();
        }

        private void DownloadFileStatus_Load(object sender, EventArgs e)
        {
            waitLoading1.Enabled = true;
            waitLoading1.Visible = true;            
        }
        public void finish()
        {
            Invoke(new Action(() =>
            {
                Hide();
                Dispose();
            }));
        }
    }
}

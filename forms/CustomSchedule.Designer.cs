﻿namespace Fingerprint_Reader.forms
{
    partial class CustomSchedule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CustomSchedule));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.l_enrollment_name = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.l_division = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.l_employment = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.n_menitkeluar_senin = new System.Windows.Forms.NumericUpDown();
            this.n_jamkeluar_senin = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.n_menitmasuk_senin = new System.Windows.Forms.NumericUpDown();
            this.n_jammasuk_senin = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.rb_senin_jadwalsendiri = new System.Windows.Forms.RadioButton();
            this.rb_senin_tidakhadir = new System.Windows.Forms.RadioButton();
            this.rb_senin_sesuaidivisi = new System.Windows.Forms.RadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rb_selasa_tidakhadir = new System.Windows.Forms.RadioButton();
            this.n_menitkeluar_selasa = new System.Windows.Forms.NumericUpDown();
            this.n_jamkeluar_selasa = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.n_menitmasuk_selasa = new System.Windows.Forms.NumericUpDown();
            this.n_jammasuk_selasa = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.rb_selasa_jadwalsendiri = new System.Windows.Forms.RadioButton();
            this.rb_selasa_sesuaidivisi = new System.Windows.Forms.RadioButton();
            this.label14 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.rb_rabu_tidakhadir = new System.Windows.Forms.RadioButton();
            this.n_menitkeluar_rabu = new System.Windows.Forms.NumericUpDown();
            this.n_jamkeluar_rabu = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.n_menitmasuk_rabu = new System.Windows.Forms.NumericUpDown();
            this.n_jammasuk_rabu = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.rb_rabu_jadwalsendiri = new System.Windows.Forms.RadioButton();
            this.rb_rabu_sesuaidivisi = new System.Windows.Forms.RadioButton();
            this.label17 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.rb_kamis_tidakhadir = new System.Windows.Forms.RadioButton();
            this.n_menitkeluar_kamis = new System.Windows.Forms.NumericUpDown();
            this.n_jamkeluar_kamis = new System.Windows.Forms.NumericUpDown();
            this.label18 = new System.Windows.Forms.Label();
            this.n_menitmasuk_kamis = new System.Windows.Forms.NumericUpDown();
            this.n_jammasuk_kamis = new System.Windows.Forms.NumericUpDown();
            this.label19 = new System.Windows.Forms.Label();
            this.rb_kamis_jadwalsendiri = new System.Windows.Forms.RadioButton();
            this.rb_kamis_sesuaidivisi = new System.Windows.Forms.RadioButton();
            this.label20 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.rb_jumat_tidakhadir = new System.Windows.Forms.RadioButton();
            this.n_menitkeluar_jumat = new System.Windows.Forms.NumericUpDown();
            this.n_jamkeluar_jumat = new System.Windows.Forms.NumericUpDown();
            this.label21 = new System.Windows.Forms.Label();
            this.n_menitmasuk_jumat = new System.Windows.Forms.NumericUpDown();
            this.n_jammasuk_jumat = new System.Windows.Forms.NumericUpDown();
            this.label22 = new System.Windows.Forms.Label();
            this.rb_jumat_jadwalsendiri = new System.Windows.Forms.RadioButton();
            this.rb_jumat_sesuaidivisi = new System.Windows.Forms.RadioButton();
            this.label23 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.l_entry_jadwal = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.n_menitkeluar_senin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_jamkeluar_senin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_menitmasuk_senin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_jammasuk_senin)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.n_menitkeluar_selasa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_jamkeluar_selasa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_menitmasuk_selasa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_jammasuk_selasa)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.n_menitkeluar_rabu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_jamkeluar_rabu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_menitmasuk_rabu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_jammasuk_rabu)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.n_menitkeluar_kamis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_jamkeluar_kamis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_menitmasuk_kamis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_jammasuk_kamis)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.n_menitkeluar_jumat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_jamkeluar_jumat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_menitmasuk_jumat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_jammasuk_jumat)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(59)))), ((int)(((byte)(68)))));
            this.pictureBox1.Image = global::Fingerprint_Reader.Properties.Resources.logo_app;
            this.pictureBox1.Location = new System.Drawing.Point(4, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(79, 57);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(59)))), ((int)(((byte)(68)))));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(995, 60);
            this.label2.TabIndex = 16;
            this.label2.Text = "Tambah Jadwal Pribadi";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Segoe UI Symbol", 9.5F);
            this.label1.Location = new System.Drawing.Point(12, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(971, 41);
            this.label1.TabIndex = 18;
            this.label1.Text = resources.GetString("label1.Text");
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI Symbol", 10F);
            this.label3.Location = new System.Drawing.Point(12, 116);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 19);
            this.label3.TabIndex = 19;
            this.label3.Text = "Nama Pegawai";
            // 
            // l_enrollment_name
            // 
            this.l_enrollment_name.AutoSize = true;
            this.l_enrollment_name.Font = new System.Drawing.Font("Segoe UI Symbol", 11F, System.Drawing.FontStyle.Bold);
            this.l_enrollment_name.Location = new System.Drawing.Point(12, 138);
            this.l_enrollment_name.Name = "l_enrollment_name";
            this.l_enrollment_name.Size = new System.Drawing.Size(190, 20);
            this.l_enrollment_name.TabIndex = 20;
            this.l_enrollment_name.Text = "Bambang Sulistyo, M.Pd";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI Symbol", 10F);
            this.label5.Location = new System.Drawing.Point(12, 168);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 19);
            this.label5.TabIndex = 19;
            this.label5.Text = "Divisi";
            // 
            // l_division
            // 
            this.l_division.AutoSize = true;
            this.l_division.Font = new System.Drawing.Font("Segoe UI Symbol", 11F, System.Drawing.FontStyle.Bold);
            this.l_division.Location = new System.Drawing.Point(12, 190);
            this.l_division.Name = "l_division";
            this.l_division.Size = new System.Drawing.Size(92, 20);
            this.l_division.TabIndex = 20;
            this.l_division.Text = "Tata Usaha";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI Symbol", 10F);
            this.label7.Location = new System.Drawing.Point(12, 221);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(141, 19);
            this.label7.TabIndex = 19;
            this.label7.Text = "Stat. Ketenagakerjaan";
            // 
            // l_employment
            // 
            this.l_employment.AutoSize = true;
            this.l_employment.Font = new System.Drawing.Font("Segoe UI Symbol", 11F, System.Drawing.FontStyle.Bold);
            this.l_employment.Location = new System.Drawing.Point(12, 243);
            this.l_employment.Name = "l_employment";
            this.l_employment.Size = new System.Drawing.Size(128, 20);
            this.l_employment.TabIndex = 20;
            this.l_employment.Text = "Pegawai Negeri";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.flowLayoutPanel1);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Location = new System.Drawing.Point(240, 119);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(743, 348);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pengaturan Jadwal";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.Controls.Add(this.panel1);
            this.flowLayoutPanel1.Controls.Add(this.panel2);
            this.flowLayoutPanel1.Controls.Add(this.panel3);
            this.flowLayoutPanel1.Controls.Add(this.panel4);
            this.flowLayoutPanel1.Controls.Add(this.panel5);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(6, 21);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(731, 321);
            this.flowLayoutPanel1.TabIndex = 0;
            this.flowLayoutPanel1.WrapContents = false;
            this.flowLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.flowLayoutPanel1_Paint);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.n_menitkeluar_senin);
            this.panel1.Controls.Add(this.n_jamkeluar_senin);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.n_menitmasuk_senin);
            this.panel1.Controls.Add(this.n_jammasuk_senin);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.rb_senin_jadwalsendiri);
            this.panel1.Controls.Add(this.rb_senin_tidakhadir);
            this.panel1.Controls.Add(this.rb_senin_sesuaidivisi);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(706, 67);
            this.panel1.TabIndex = 0;
            // 
            // n_menitkeluar_senin
            // 
            this.n_menitkeluar_senin.Font = new System.Drawing.Font("Segoe UI Symbol", 10F);
            this.n_menitkeluar_senin.Location = new System.Drawing.Point(423, 32);
            this.n_menitkeluar_senin.Name = "n_menitkeluar_senin";
            this.n_menitkeluar_senin.Size = new System.Drawing.Size(75, 25);
            this.n_menitkeluar_senin.TabIndex = 3;
            this.n_menitkeluar_senin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // n_jamkeluar_senin
            // 
            this.n_jamkeluar_senin.Font = new System.Drawing.Font("Segoe UI Symbol", 10F);
            this.n_jamkeluar_senin.Location = new System.Drawing.Point(342, 32);
            this.n_jamkeluar_senin.Name = "n_jamkeluar_senin";
            this.n_jamkeluar_senin.Size = new System.Drawing.Size(75, 25);
            this.n_jamkeluar_senin.TabIndex = 3;
            this.n_jamkeluar_senin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI Symbol", 10F);
            this.label11.Location = new System.Drawing.Point(256, 34);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(75, 19);
            this.label11.TabIndex = 2;
            this.label11.Text = "Jam Keluar";
            // 
            // n_menitmasuk_senin
            // 
            this.n_menitmasuk_senin.Font = new System.Drawing.Font("Segoe UI Symbol", 10F);
            this.n_menitmasuk_senin.Location = new System.Drawing.Point(172, 32);
            this.n_menitmasuk_senin.Name = "n_menitmasuk_senin";
            this.n_menitmasuk_senin.Size = new System.Drawing.Size(78, 25);
            this.n_menitmasuk_senin.TabIndex = 3;
            this.n_menitmasuk_senin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // n_jammasuk_senin
            // 
            this.n_jammasuk_senin.Font = new System.Drawing.Font("Segoe UI Symbol", 10F);
            this.n_jammasuk_senin.Location = new System.Drawing.Point(88, 32);
            this.n_jammasuk_senin.Name = "n_jammasuk_senin";
            this.n_jammasuk_senin.Size = new System.Drawing.Size(78, 25);
            this.n_jammasuk_senin.TabIndex = 3;
            this.n_jammasuk_senin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI Symbol", 10F);
            this.label10.Location = new System.Drawing.Point(3, 34);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(78, 19);
            this.label10.TabIndex = 2;
            this.label10.Text = "Jam Masuk";
            // 
            // rb_senin_jadwalsendiri
            // 
            this.rb_senin_jadwalsendiri.AutoSize = true;
            this.rb_senin_jadwalsendiri.Location = new System.Drawing.Point(568, 4);
            this.rb_senin_jadwalsendiri.Name = "rb_senin_jadwalsendiri";
            this.rb_senin_jadwalsendiri.Size = new System.Drawing.Size(122, 24);
            this.rb_senin_jadwalsendiri.TabIndex = 1;
            this.rb_senin_jadwalsendiri.TabStop = true;
            this.rb_senin_jadwalsendiri.Text = "Jadwal Sendiri";
            this.rb_senin_jadwalsendiri.UseVisualStyleBackColor = true;
            // 
            // rb_senin_tidakhadir
            // 
            this.rb_senin_tidakhadir.AutoSize = true;
            this.rb_senin_tidakhadir.Location = new System.Drawing.Point(313, 4);
            this.rb_senin_tidakhadir.Name = "rb_senin_tidakhadir";
            this.rb_senin_tidakhadir.Size = new System.Drawing.Size(104, 24);
            this.rb_senin_tidakhadir.TabIndex = 1;
            this.rb_senin_tidakhadir.TabStop = true;
            this.rb_senin_tidakhadir.Text = "Tidak Hadir";
            this.rb_senin_tidakhadir.UseVisualStyleBackColor = true;
            this.rb_senin_tidakhadir.CheckedChanged += new System.EventHandler(this.radiobutton_change);
            // 
            // rb_senin_sesuaidivisi
            // 
            this.rb_senin_sesuaidivisi.AutoSize = true;
            this.rb_senin_sesuaidivisi.Location = new System.Drawing.Point(439, 4);
            this.rb_senin_sesuaidivisi.Name = "rb_senin_sesuaidivisi";
            this.rb_senin_sesuaidivisi.Size = new System.Drawing.Size(109, 24);
            this.rb_senin_sesuaidivisi.TabIndex = 1;
            this.rb_senin_sesuaidivisi.TabStop = true;
            this.rb_senin_sesuaidivisi.Text = "Sesuai Divisi";
            this.rb_senin_sesuaidivisi.UseVisualStyleBackColor = true;
            this.rb_senin_sesuaidivisi.CheckedChanged += new System.EventHandler(this.radiobutton_change);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI Symbol", 11F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(3, 4);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(50, 20);
            this.label9.TabIndex = 0;
            this.label9.Text = "Senin";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.rb_selasa_tidakhadir);
            this.panel2.Controls.Add(this.n_menitkeluar_selasa);
            this.panel2.Controls.Add(this.n_jamkeluar_selasa);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.n_menitmasuk_selasa);
            this.panel2.Controls.Add(this.n_jammasuk_selasa);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.rb_selasa_jadwalsendiri);
            this.panel2.Controls.Add(this.rb_selasa_sesuaidivisi);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Location = new System.Drawing.Point(3, 76);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(706, 67);
            this.panel2.TabIndex = 1;
            // 
            // rb_selasa_tidakhadir
            // 
            this.rb_selasa_tidakhadir.AutoSize = true;
            this.rb_selasa_tidakhadir.Location = new System.Drawing.Point(312, 4);
            this.rb_selasa_tidakhadir.Name = "rb_selasa_tidakhadir";
            this.rb_selasa_tidakhadir.Size = new System.Drawing.Size(104, 24);
            this.rb_selasa_tidakhadir.TabIndex = 4;
            this.rb_selasa_tidakhadir.TabStop = true;
            this.rb_selasa_tidakhadir.Text = "Tidak Hadir";
            this.rb_selasa_tidakhadir.UseVisualStyleBackColor = true;
            // 
            // n_menitkeluar_selasa
            // 
            this.n_menitkeluar_selasa.Font = new System.Drawing.Font("Segoe UI Symbol", 10F);
            this.n_menitkeluar_selasa.Location = new System.Drawing.Point(423, 32);
            this.n_menitkeluar_selasa.Name = "n_menitkeluar_selasa";
            this.n_menitkeluar_selasa.Size = new System.Drawing.Size(75, 25);
            this.n_menitkeluar_selasa.TabIndex = 3;
            this.n_menitkeluar_selasa.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // n_jamkeluar_selasa
            // 
            this.n_jamkeluar_selasa.Font = new System.Drawing.Font("Segoe UI Symbol", 10F);
            this.n_jamkeluar_selasa.Location = new System.Drawing.Point(342, 32);
            this.n_jamkeluar_selasa.Name = "n_jamkeluar_selasa";
            this.n_jamkeluar_selasa.Size = new System.Drawing.Size(75, 25);
            this.n_jamkeluar_selasa.TabIndex = 3;
            this.n_jamkeluar_selasa.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI Symbol", 10F);
            this.label12.Location = new System.Drawing.Point(256, 34);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(75, 19);
            this.label12.TabIndex = 2;
            this.label12.Text = "Jam Keluar";
            // 
            // n_menitmasuk_selasa
            // 
            this.n_menitmasuk_selasa.Font = new System.Drawing.Font("Segoe UI Symbol", 10F);
            this.n_menitmasuk_selasa.Location = new System.Drawing.Point(172, 32);
            this.n_menitmasuk_selasa.Name = "n_menitmasuk_selasa";
            this.n_menitmasuk_selasa.Size = new System.Drawing.Size(78, 25);
            this.n_menitmasuk_selasa.TabIndex = 3;
            this.n_menitmasuk_selasa.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // n_jammasuk_selasa
            // 
            this.n_jammasuk_selasa.Font = new System.Drawing.Font("Segoe UI Symbol", 10F);
            this.n_jammasuk_selasa.Location = new System.Drawing.Point(88, 32);
            this.n_jammasuk_selasa.Name = "n_jammasuk_selasa";
            this.n_jammasuk_selasa.Size = new System.Drawing.Size(78, 25);
            this.n_jammasuk_selasa.TabIndex = 3;
            this.n_jammasuk_selasa.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI Symbol", 10F);
            this.label13.Location = new System.Drawing.Point(3, 34);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(78, 19);
            this.label13.TabIndex = 2;
            this.label13.Text = "Jam Masuk";
            // 
            // rb_selasa_jadwalsendiri
            // 
            this.rb_selasa_jadwalsendiri.AutoSize = true;
            this.rb_selasa_jadwalsendiri.Location = new System.Drawing.Point(568, 4);
            this.rb_selasa_jadwalsendiri.Name = "rb_selasa_jadwalsendiri";
            this.rb_selasa_jadwalsendiri.Size = new System.Drawing.Size(122, 24);
            this.rb_selasa_jadwalsendiri.TabIndex = 1;
            this.rb_selasa_jadwalsendiri.TabStop = true;
            this.rb_selasa_jadwalsendiri.Text = "Jadwal Sendiri";
            this.rb_selasa_jadwalsendiri.UseVisualStyleBackColor = true;
            // 
            // rb_selasa_sesuaidivisi
            // 
            this.rb_selasa_sesuaidivisi.AutoSize = true;
            this.rb_selasa_sesuaidivisi.Location = new System.Drawing.Point(439, 4);
            this.rb_selasa_sesuaidivisi.Name = "rb_selasa_sesuaidivisi";
            this.rb_selasa_sesuaidivisi.Size = new System.Drawing.Size(109, 24);
            this.rb_selasa_sesuaidivisi.TabIndex = 1;
            this.rb_selasa_sesuaidivisi.TabStop = true;
            this.rb_selasa_sesuaidivisi.Text = "Sesuai Divisi";
            this.rb_selasa_sesuaidivisi.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI Symbol", 11F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(3, 4);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(57, 20);
            this.label14.TabIndex = 0;
            this.label14.Text = "Selasa";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.rb_rabu_tidakhadir);
            this.panel3.Controls.Add(this.n_menitkeluar_rabu);
            this.panel3.Controls.Add(this.n_jamkeluar_rabu);
            this.panel3.Controls.Add(this.label15);
            this.panel3.Controls.Add(this.n_menitmasuk_rabu);
            this.panel3.Controls.Add(this.n_jammasuk_rabu);
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.rb_rabu_jadwalsendiri);
            this.panel3.Controls.Add(this.rb_rabu_sesuaidivisi);
            this.panel3.Controls.Add(this.label17);
            this.panel3.Location = new System.Drawing.Point(3, 149);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(706, 67);
            this.panel3.TabIndex = 4;
            // 
            // rb_rabu_tidakhadir
            // 
            this.rb_rabu_tidakhadir.AutoSize = true;
            this.rb_rabu_tidakhadir.Location = new System.Drawing.Point(313, 4);
            this.rb_rabu_tidakhadir.Name = "rb_rabu_tidakhadir";
            this.rb_rabu_tidakhadir.Size = new System.Drawing.Size(104, 24);
            this.rb_rabu_tidakhadir.TabIndex = 4;
            this.rb_rabu_tidakhadir.TabStop = true;
            this.rb_rabu_tidakhadir.Text = "Tidak Hadir";
            this.rb_rabu_tidakhadir.UseVisualStyleBackColor = true;
            // 
            // n_menitkeluar_rabu
            // 
            this.n_menitkeluar_rabu.Font = new System.Drawing.Font("Segoe UI Symbol", 10F);
            this.n_menitkeluar_rabu.Location = new System.Drawing.Point(423, 32);
            this.n_menitkeluar_rabu.Name = "n_menitkeluar_rabu";
            this.n_menitkeluar_rabu.Size = new System.Drawing.Size(75, 25);
            this.n_menitkeluar_rabu.TabIndex = 3;
            this.n_menitkeluar_rabu.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // n_jamkeluar_rabu
            // 
            this.n_jamkeluar_rabu.Font = new System.Drawing.Font("Segoe UI Symbol", 10F);
            this.n_jamkeluar_rabu.Location = new System.Drawing.Point(342, 32);
            this.n_jamkeluar_rabu.Name = "n_jamkeluar_rabu";
            this.n_jamkeluar_rabu.Size = new System.Drawing.Size(75, 25);
            this.n_jamkeluar_rabu.TabIndex = 3;
            this.n_jamkeluar_rabu.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Segoe UI Symbol", 10F);
            this.label15.Location = new System.Drawing.Point(256, 34);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(75, 19);
            this.label15.TabIndex = 2;
            this.label15.Text = "Jam Keluar";
            // 
            // n_menitmasuk_rabu
            // 
            this.n_menitmasuk_rabu.Font = new System.Drawing.Font("Segoe UI Symbol", 10F);
            this.n_menitmasuk_rabu.Location = new System.Drawing.Point(172, 32);
            this.n_menitmasuk_rabu.Name = "n_menitmasuk_rabu";
            this.n_menitmasuk_rabu.Size = new System.Drawing.Size(78, 25);
            this.n_menitmasuk_rabu.TabIndex = 3;
            this.n_menitmasuk_rabu.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // n_jammasuk_rabu
            // 
            this.n_jammasuk_rabu.Font = new System.Drawing.Font("Segoe UI Symbol", 10F);
            this.n_jammasuk_rabu.Location = new System.Drawing.Point(88, 32);
            this.n_jammasuk_rabu.Name = "n_jammasuk_rabu";
            this.n_jammasuk_rabu.Size = new System.Drawing.Size(78, 25);
            this.n_jammasuk_rabu.TabIndex = 3;
            this.n_jammasuk_rabu.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Segoe UI Symbol", 10F);
            this.label16.Location = new System.Drawing.Point(3, 34);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(78, 19);
            this.label16.TabIndex = 2;
            this.label16.Text = "Jam Masuk";
            // 
            // rb_rabu_jadwalsendiri
            // 
            this.rb_rabu_jadwalsendiri.AutoSize = true;
            this.rb_rabu_jadwalsendiri.Location = new System.Drawing.Point(568, 4);
            this.rb_rabu_jadwalsendiri.Name = "rb_rabu_jadwalsendiri";
            this.rb_rabu_jadwalsendiri.Size = new System.Drawing.Size(122, 24);
            this.rb_rabu_jadwalsendiri.TabIndex = 1;
            this.rb_rabu_jadwalsendiri.TabStop = true;
            this.rb_rabu_jadwalsendiri.Text = "Jadwal Sendiri";
            this.rb_rabu_jadwalsendiri.UseVisualStyleBackColor = true;
            // 
            // rb_rabu_sesuaidivisi
            // 
            this.rb_rabu_sesuaidivisi.AutoSize = true;
            this.rb_rabu_sesuaidivisi.Location = new System.Drawing.Point(439, 4);
            this.rb_rabu_sesuaidivisi.Name = "rb_rabu_sesuaidivisi";
            this.rb_rabu_sesuaidivisi.Size = new System.Drawing.Size(109, 24);
            this.rb_rabu_sesuaidivisi.TabIndex = 1;
            this.rb_rabu_sesuaidivisi.TabStop = true;
            this.rb_rabu_sesuaidivisi.Text = "Sesuai Divisi";
            this.rb_rabu_sesuaidivisi.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Segoe UI Symbol", 11F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(3, 4);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(47, 20);
            this.label17.TabIndex = 0;
            this.label17.Text = "Rabu";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.rb_kamis_tidakhadir);
            this.panel4.Controls.Add(this.n_menitkeluar_kamis);
            this.panel4.Controls.Add(this.n_jamkeluar_kamis);
            this.panel4.Controls.Add(this.label18);
            this.panel4.Controls.Add(this.n_menitmasuk_kamis);
            this.panel4.Controls.Add(this.n_jammasuk_kamis);
            this.panel4.Controls.Add(this.label19);
            this.panel4.Controls.Add(this.rb_kamis_jadwalsendiri);
            this.panel4.Controls.Add(this.rb_kamis_sesuaidivisi);
            this.panel4.Controls.Add(this.label20);
            this.panel4.Location = new System.Drawing.Point(3, 222);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(706, 67);
            this.panel4.TabIndex = 5;
            // 
            // rb_kamis_tidakhadir
            // 
            this.rb_kamis_tidakhadir.AutoSize = true;
            this.rb_kamis_tidakhadir.Location = new System.Drawing.Point(313, 4);
            this.rb_kamis_tidakhadir.Name = "rb_kamis_tidakhadir";
            this.rb_kamis_tidakhadir.Size = new System.Drawing.Size(104, 24);
            this.rb_kamis_tidakhadir.TabIndex = 4;
            this.rb_kamis_tidakhadir.TabStop = true;
            this.rb_kamis_tidakhadir.Text = "Tidak Hadir";
            this.rb_kamis_tidakhadir.UseVisualStyleBackColor = true;
            // 
            // n_menitkeluar_kamis
            // 
            this.n_menitkeluar_kamis.Font = new System.Drawing.Font("Segoe UI Symbol", 10F);
            this.n_menitkeluar_kamis.Location = new System.Drawing.Point(423, 32);
            this.n_menitkeluar_kamis.Name = "n_menitkeluar_kamis";
            this.n_menitkeluar_kamis.Size = new System.Drawing.Size(75, 25);
            this.n_menitkeluar_kamis.TabIndex = 3;
            this.n_menitkeluar_kamis.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // n_jamkeluar_kamis
            // 
            this.n_jamkeluar_kamis.Font = new System.Drawing.Font("Segoe UI Symbol", 10F);
            this.n_jamkeluar_kamis.Location = new System.Drawing.Point(342, 32);
            this.n_jamkeluar_kamis.Name = "n_jamkeluar_kamis";
            this.n_jamkeluar_kamis.Size = new System.Drawing.Size(75, 25);
            this.n_jamkeluar_kamis.TabIndex = 3;
            this.n_jamkeluar_kamis.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Segoe UI Symbol", 10F);
            this.label18.Location = new System.Drawing.Point(256, 34);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(75, 19);
            this.label18.TabIndex = 2;
            this.label18.Text = "Jam Keluar";
            // 
            // n_menitmasuk_kamis
            // 
            this.n_menitmasuk_kamis.Font = new System.Drawing.Font("Segoe UI Symbol", 10F);
            this.n_menitmasuk_kamis.Location = new System.Drawing.Point(172, 32);
            this.n_menitmasuk_kamis.Name = "n_menitmasuk_kamis";
            this.n_menitmasuk_kamis.Size = new System.Drawing.Size(78, 25);
            this.n_menitmasuk_kamis.TabIndex = 3;
            this.n_menitmasuk_kamis.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.n_menitmasuk_kamis.ValueChanged += new System.EventHandler(this.n_jammasuk_kamis_ValueChanged);
            // 
            // n_jammasuk_kamis
            // 
            this.n_jammasuk_kamis.Font = new System.Drawing.Font("Segoe UI Symbol", 10F);
            this.n_jammasuk_kamis.Location = new System.Drawing.Point(88, 32);
            this.n_jammasuk_kamis.Name = "n_jammasuk_kamis";
            this.n_jammasuk_kamis.Size = new System.Drawing.Size(78, 25);
            this.n_jammasuk_kamis.TabIndex = 3;
            this.n_jammasuk_kamis.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.n_jammasuk_kamis.ValueChanged += new System.EventHandler(this.n_jammasuk_kamis_ValueChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Segoe UI Symbol", 10F);
            this.label19.Location = new System.Drawing.Point(3, 34);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(78, 19);
            this.label19.TabIndex = 2;
            this.label19.Text = "Jam Masuk";
            // 
            // rb_kamis_jadwalsendiri
            // 
            this.rb_kamis_jadwalsendiri.AutoSize = true;
            this.rb_kamis_jadwalsendiri.Location = new System.Drawing.Point(568, 4);
            this.rb_kamis_jadwalsendiri.Name = "rb_kamis_jadwalsendiri";
            this.rb_kamis_jadwalsendiri.Size = new System.Drawing.Size(122, 24);
            this.rb_kamis_jadwalsendiri.TabIndex = 1;
            this.rb_kamis_jadwalsendiri.TabStop = true;
            this.rb_kamis_jadwalsendiri.Text = "Jadwal Sendiri";
            this.rb_kamis_jadwalsendiri.UseVisualStyleBackColor = true;
            // 
            // rb_kamis_sesuaidivisi
            // 
            this.rb_kamis_sesuaidivisi.AutoSize = true;
            this.rb_kamis_sesuaidivisi.Location = new System.Drawing.Point(439, 4);
            this.rb_kamis_sesuaidivisi.Name = "rb_kamis_sesuaidivisi";
            this.rb_kamis_sesuaidivisi.Size = new System.Drawing.Size(109, 24);
            this.rb_kamis_sesuaidivisi.TabIndex = 1;
            this.rb_kamis_sesuaidivisi.TabStop = true;
            this.rb_kamis_sesuaidivisi.Text = "Sesuai Divisi";
            this.rb_kamis_sesuaidivisi.UseVisualStyleBackColor = true;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Segoe UI Symbol", 11F, System.Drawing.FontStyle.Bold);
            this.label20.Location = new System.Drawing.Point(3, 4);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(54, 20);
            this.label20.TabIndex = 0;
            this.label20.Text = "Kamis";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.rb_jumat_tidakhadir);
            this.panel5.Controls.Add(this.n_menitkeluar_jumat);
            this.panel5.Controls.Add(this.n_jamkeluar_jumat);
            this.panel5.Controls.Add(this.label21);
            this.panel5.Controls.Add(this.n_menitmasuk_jumat);
            this.panel5.Controls.Add(this.n_jammasuk_jumat);
            this.panel5.Controls.Add(this.label22);
            this.panel5.Controls.Add(this.rb_jumat_jadwalsendiri);
            this.panel5.Controls.Add(this.rb_jumat_sesuaidivisi);
            this.panel5.Controls.Add(this.label23);
            this.panel5.Location = new System.Drawing.Point(3, 295);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(706, 67);
            this.panel5.TabIndex = 6;
            // 
            // rb_jumat_tidakhadir
            // 
            this.rb_jumat_tidakhadir.AutoSize = true;
            this.rb_jumat_tidakhadir.Location = new System.Drawing.Point(313, 4);
            this.rb_jumat_tidakhadir.Name = "rb_jumat_tidakhadir";
            this.rb_jumat_tidakhadir.Size = new System.Drawing.Size(104, 24);
            this.rb_jumat_tidakhadir.TabIndex = 4;
            this.rb_jumat_tidakhadir.TabStop = true;
            this.rb_jumat_tidakhadir.Text = "Tidak Hadir";
            this.rb_jumat_tidakhadir.UseVisualStyleBackColor = true;
            // 
            // n_menitkeluar_jumat
            // 
            this.n_menitkeluar_jumat.Font = new System.Drawing.Font("Segoe UI Symbol", 10F);
            this.n_menitkeluar_jumat.Location = new System.Drawing.Point(423, 32);
            this.n_menitkeluar_jumat.Name = "n_menitkeluar_jumat";
            this.n_menitkeluar_jumat.Size = new System.Drawing.Size(75, 25);
            this.n_menitkeluar_jumat.TabIndex = 3;
            this.n_menitkeluar_jumat.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // n_jamkeluar_jumat
            // 
            this.n_jamkeluar_jumat.Font = new System.Drawing.Font("Segoe UI Symbol", 10F);
            this.n_jamkeluar_jumat.Location = new System.Drawing.Point(342, 32);
            this.n_jamkeluar_jumat.Name = "n_jamkeluar_jumat";
            this.n_jamkeluar_jumat.Size = new System.Drawing.Size(75, 25);
            this.n_jamkeluar_jumat.TabIndex = 3;
            this.n_jamkeluar_jumat.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Segoe UI Symbol", 10F);
            this.label21.Location = new System.Drawing.Point(256, 34);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(75, 19);
            this.label21.TabIndex = 2;
            this.label21.Text = "Jam Keluar";
            // 
            // n_menitmasuk_jumat
            // 
            this.n_menitmasuk_jumat.Font = new System.Drawing.Font("Segoe UI Symbol", 10F);
            this.n_menitmasuk_jumat.Location = new System.Drawing.Point(172, 32);
            this.n_menitmasuk_jumat.Name = "n_menitmasuk_jumat";
            this.n_menitmasuk_jumat.Size = new System.Drawing.Size(78, 25);
            this.n_menitmasuk_jumat.TabIndex = 3;
            this.n_menitmasuk_jumat.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // n_jammasuk_jumat
            // 
            this.n_jammasuk_jumat.Font = new System.Drawing.Font("Segoe UI Symbol", 10F);
            this.n_jammasuk_jumat.Location = new System.Drawing.Point(88, 32);
            this.n_jammasuk_jumat.Name = "n_jammasuk_jumat";
            this.n_jammasuk_jumat.Size = new System.Drawing.Size(78, 25);
            this.n_jammasuk_jumat.TabIndex = 3;
            this.n_jammasuk_jumat.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Segoe UI Symbol", 10F);
            this.label22.Location = new System.Drawing.Point(3, 34);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(78, 19);
            this.label22.TabIndex = 2;
            this.label22.Text = "Jam Masuk";
            // 
            // rb_jumat_jadwalsendiri
            // 
            this.rb_jumat_jadwalsendiri.AutoSize = true;
            this.rb_jumat_jadwalsendiri.Location = new System.Drawing.Point(568, 4);
            this.rb_jumat_jadwalsendiri.Name = "rb_jumat_jadwalsendiri";
            this.rb_jumat_jadwalsendiri.Size = new System.Drawing.Size(122, 24);
            this.rb_jumat_jadwalsendiri.TabIndex = 1;
            this.rb_jumat_jadwalsendiri.TabStop = true;
            this.rb_jumat_jadwalsendiri.Text = "Jadwal Sendiri";
            this.rb_jumat_jadwalsendiri.UseVisualStyleBackColor = true;
            // 
            // rb_jumat_sesuaidivisi
            // 
            this.rb_jumat_sesuaidivisi.AutoSize = true;
            this.rb_jumat_sesuaidivisi.Location = new System.Drawing.Point(439, 4);
            this.rb_jumat_sesuaidivisi.Name = "rb_jumat_sesuaidivisi";
            this.rb_jumat_sesuaidivisi.Size = new System.Drawing.Size(109, 24);
            this.rb_jumat_sesuaidivisi.TabIndex = 1;
            this.rb_jumat_sesuaidivisi.TabStop = true;
            this.rb_jumat_sesuaidivisi.Text = "Sesuai Divisi";
            this.rb_jumat_sesuaidivisi.UseVisualStyleBackColor = true;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Segoe UI Symbol", 11F, System.Drawing.FontStyle.Bold);
            this.label23.Location = new System.Drawing.Point(3, 4);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(53, 20);
            this.label23.TabIndex = 0;
            this.label23.Text = "Jumat";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.button1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button1.Image = global::Fingerprint_Reader.Properties.Resources.Save_65301;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(857, 470);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(119, 33);
            this.button1.TabIndex = 22;
            this.button1.Text = "Simpan";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Red;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.ForeColor = System.Drawing.SystemColors.Control;
            this.button2.Image = global::Fingerprint_Reader.Properties.Resources.DeleteColumn_5627;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(629, 470);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(222, 33);
            this.button2.TabIndex = 22;
            this.button2.Text = "Hapus Jadwal Pribadi";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI Symbol", 10F);
            this.label4.Location = new System.Drawing.Point(12, 274);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 19);
            this.label4.TabIndex = 19;
            this.label4.Text = "Entri jadwal";
            // 
            // l_entry_jadwal
            // 
            this.l_entry_jadwal.AutoSize = true;
            this.l_entry_jadwal.Font = new System.Drawing.Font("Segoe UI Symbol", 11F, System.Drawing.FontStyle.Bold);
            this.l_entry_jadwal.Location = new System.Drawing.Point(12, 296);
            this.l_entry_jadwal.Name = "l_entry_jadwal";
            this.l_entry_jadwal.Size = new System.Drawing.Size(104, 20);
            this.l_entry_jadwal.TabIndex = 20;
            this.l_entry_jadwal.Text = "Sesuai Divisi";
            // 
            // CustomSchedule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(995, 515);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.l_entry_jadwal);
            this.Controls.Add(this.l_employment);
            this.Controls.Add(this.l_division);
            this.Controls.Add(this.l_enrollment_name);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label2);
            this.Font = new System.Drawing.Font("Segoe UI Symbol", 11F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "CustomSchedule";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Jadwal Pribadi";
            this.Load += new System.EventHandler(this.CustomSchedule_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.n_menitkeluar_senin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_jamkeluar_senin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_menitmasuk_senin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_jammasuk_senin)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.n_menitkeluar_selasa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_jamkeluar_selasa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_menitmasuk_selasa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_jammasuk_selasa)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.n_menitkeluar_rabu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_jamkeluar_rabu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_menitmasuk_rabu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_jammasuk_rabu)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.n_menitkeluar_kamis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_jamkeluar_kamis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_menitmasuk_kamis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_jammasuk_kamis)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.n_menitkeluar_jumat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_jamkeluar_jumat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_menitmasuk_jumat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_jammasuk_jumat)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label l_enrollment_name;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label l_division;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label l_employment;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.NumericUpDown n_jamkeluar_senin;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown n_jammasuk_senin;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.RadioButton rb_senin_jadwalsendiri;
        private System.Windows.Forms.RadioButton rb_senin_sesuaidivisi;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.NumericUpDown n_jamkeluar_selasa;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown n_jammasuk_selasa;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.RadioButton rb_selasa_jadwalsendiri;
        private System.Windows.Forms.RadioButton rb_selasa_sesuaidivisi;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.NumericUpDown n_jamkeluar_rabu;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown n_jammasuk_rabu;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.RadioButton rb_rabu_jadwalsendiri;
        private System.Windows.Forms.RadioButton rb_rabu_sesuaidivisi;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.NumericUpDown n_jamkeluar_kamis;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.NumericUpDown n_jammasuk_kamis;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.RadioButton rb_kamis_jadwalsendiri;
        private System.Windows.Forms.RadioButton rb_kamis_sesuaidivisi;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.NumericUpDown n_jamkeluar_jumat;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.NumericUpDown n_jammasuk_jumat;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.RadioButton rb_jumat_jadwalsendiri;
        private System.Windows.Forms.RadioButton rb_jumat_sesuaidivisi;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RadioButton rb_senin_tidakhadir;
        private System.Windows.Forms.RadioButton rb_selasa_tidakhadir;
        private System.Windows.Forms.RadioButton rb_rabu_tidakhadir;
        private System.Windows.Forms.RadioButton rb_kamis_tidakhadir;
        private System.Windows.Forms.RadioButton rb_jumat_tidakhadir;
        private System.Windows.Forms.NumericUpDown n_menitmasuk_senin;
        private System.Windows.Forms.NumericUpDown n_menitmasuk_selasa;
        private System.Windows.Forms.NumericUpDown n_menitmasuk_rabu;
        private System.Windows.Forms.NumericUpDown n_menitkeluar_senin;
        private System.Windows.Forms.NumericUpDown n_menitkeluar_selasa;
        private System.Windows.Forms.NumericUpDown n_menitkeluar_rabu;
        private System.Windows.Forms.NumericUpDown n_menitkeluar_kamis;
        private System.Windows.Forms.NumericUpDown n_menitmasuk_kamis;
        private System.Windows.Forms.NumericUpDown n_menitkeluar_jumat;
        private System.Windows.Forms.NumericUpDown n_menitmasuk_jumat;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label l_entry_jadwal;
    }
}
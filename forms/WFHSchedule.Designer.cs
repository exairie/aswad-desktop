﻿namespace Fingerprint_Reader.forms
{
    partial class WFHSchedule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WFHSchedule));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dateWfh = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.firstnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.divisionidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.employmentstatusidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iswfh = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.enrollmentsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.wfh_date = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.t_search = new System.Windows.Forms.TextBox();
            this.waitLoading1 = new CSharp.Winform.UI.Loading.WaitLoading();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.enrollmentsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(59)))), ((int)(((byte)(68)))));
            this.pictureBox1.Image = global::Fingerprint_Reader.Properties.Resources.logo_app;
            this.pictureBox1.Location = new System.Drawing.Point(3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(79, 57);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(59)))), ((int)(((byte)(68)))));
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(-1, -1);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(1082, 60);
            this.label2.TabIndex = 16;
            this.label2.Text = "Jadwal WFH";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dateWfh
            // 
            this.dateWfh.Location = new System.Drawing.Point(12, 109);
            this.dateWfh.Name = "dateWfh";
            this.dateWfh.Size = new System.Drawing.Size(247, 20);
            this.dateWfh.TabIndex = 18;
            this.dateWfh.ValueChanged += new System.EventHandler(this.dateWfh_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(672, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 17);
            this.label1.TabIndex = 19;
            this.label1.Text = "Cari";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(12, 151);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(247, 188);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(235, 169);
            this.label3.TabIndex = 20;
            this.label3.Text = resources.GetString("label3.Text");
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.firstnameDataGridViewTextBoxColumn,
            this.divisionidDataGridViewTextBoxColumn,
            this.employmentstatusidDataGridViewTextBoxColumn,
            this.iswfh});
            this.dataGridView1.DataSource = this.enrollmentsBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(266, 109);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(804, 344);
            this.dataGridView1.TabIndex = 21;
            // 
            // firstnameDataGridViewTextBoxColumn
            // 
            this.firstnameDataGridViewTextBoxColumn.DataPropertyName = "firstname";
            this.firstnameDataGridViewTextBoxColumn.HeaderText = "Nama";
            this.firstnameDataGridViewTextBoxColumn.Name = "firstnameDataGridViewTextBoxColumn";
            // 
            // divisionidDataGridViewTextBoxColumn
            // 
            this.divisionidDataGridViewTextBoxColumn.DataPropertyName = "division_id";
            this.divisionidDataGridViewTextBoxColumn.HeaderText = "Divisi";
            this.divisionidDataGridViewTextBoxColumn.Name = "divisionidDataGridViewTextBoxColumn";
            // 
            // employmentstatusidDataGridViewTextBoxColumn
            // 
            this.employmentstatusidDataGridViewTextBoxColumn.DataPropertyName = "employment_status_id";
            this.employmentstatusidDataGridViewTextBoxColumn.HeaderText = "Sta. Kepegawaian";
            this.employmentstatusidDataGridViewTextBoxColumn.Name = "employmentstatusidDataGridViewTextBoxColumn";
            // 
            // iswfh
            // 
            this.iswfh.DataPropertyName = "isWfhToday";
            this.iswfh.HeaderText = "Status WFH";
            this.iswfh.Name = "iswfh";
            // 
            // enrollmentsBindingSource
            // 
            this.enrollmentsBindingSource.DataSource = typeof(Fingerprint_Reader.models.Enrollments);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(262, 85);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(227, 17);
            this.label4.TabIndex = 19;
            this.label4.Text = "Pegawai yang WFH pada tanggal -";
            // 
            // wfh_date
            // 
            this.wfh_date.AutoSize = true;
            this.wfh_date.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wfh_date.Location = new System.Drawing.Point(486, 85);
            this.wfh_date.Name = "wfh_date";
            this.wfh_date.Size = new System.Drawing.Size(14, 17);
            this.wfh_date.TabIndex = 19;
            this.wfh_date.Text = "-";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(912, 78);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(156, 27);
            this.button1.TabIndex = 22;
            this.button1.Text = "Simpan Perubahan";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // t_search
            // 
            this.t_search.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.t_search.Location = new System.Drawing.Point(711, 80);
            this.t_search.Name = "t_search";
            this.t_search.Size = new System.Drawing.Size(195, 23);
            this.t_search.TabIndex = 23;
            this.t_search.TextChanged += new System.EventHandler(this.t_search_TextChanged);
            this.t_search.KeyUp += new System.Windows.Forms.KeyEventHandler(this.t_search_KeyUp);
            // 
            // waitLoading1
            // 
            this.waitLoading1.Alpha = 125;
            this.waitLoading1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.waitLoading1.BindControl = null;
            this.waitLoading1.BkColor = System.Drawing.Color.WhiteSmoke;
            this.waitLoading1.IsTransparent = true;
            this.waitLoading1.Location = new System.Drawing.Point(266, 62);
            this.waitLoading1.Name = "waitLoading1";
            this.waitLoading1.Size = new System.Drawing.Size(816, 405);
            this.waitLoading1.TabIndex = 24;
            this.waitLoading1.Text = "waitLoading1";
            this.waitLoading1.Visible = false;
            // 
            // WFHSchedule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1081, 465);
            this.Controls.Add(this.waitLoading1);
            this.Controls.Add(this.t_search);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.wfh_date);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dateWfh);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label2);
            this.Name = "WFHSchedule";
            this.Text = "WFHSchedule";
            this.Load += new System.EventHandler(this.WFHSchedule_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.enrollmentsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateWfh;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label wfh_date;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.BindingSource enrollmentsBindingSource;
        private System.Windows.Forms.TextBox t_search;
        private System.Windows.Forms.DataGridViewTextBoxColumn firstnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn divisionidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn employmentstatusidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn iswfh;
        private CSharp.Winform.UI.Loading.WaitLoading waitLoading1;
    }
}
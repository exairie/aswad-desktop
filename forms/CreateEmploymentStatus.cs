﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Flurl;
using Flurl.Http;
using Fingerprint_Reader.models;

namespace Fingerprint_Reader.forms
{    
    public partial class CreateEmploymentStatus : Form
    {
        models.EmploymentStatus employmentStatus = new models.EmploymentStatus();
        bool edit = false;
        public CreateEmploymentStatus()
        {
            InitializeComponent();
        }
        public CreateEmploymentStatus(EmploymentStatus data)
        {
            InitializeComponent();
            this.employmentStatus = data;
            edit = true;
            t_employment.Text = employmentStatus.employment_status;            

        }
        bool validate()
        {
            if(t_employment.Text.Length < 1)
            {
                return false;
            }            
            return true;
        }
        async private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            if (!validate())
            {
                MessageBox.Show("Cek kembali input anda!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }

            if (edit)
            {
                update();
            }else
            {
                create();
            }
        }

        async void update()
        {
            employmentStatus.employment_status = t_employment.Text;            

            try
            {
                var q = await Properties.Settings.Default.serverUrl
                    .WithHeader("token", Program.token)
                    .AppendPathSegment("employment")
                    .AppendPathSegment(employmentStatus.id.ToString())
                    .PutJsonAsync(employmentStatus);
                if (q.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    MessageBox.Show("Data berhasil disimpan!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Hide();
                    Dispose();
                    DataStore.LoadEmploymentStatus(() => { });
                }
            }
            catch (FlurlHttpException ex)
            {
                MessageBox.Show("Tidak dapat terhubung ke server!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            finally
            {
                button1.Enabled = true;
            }
        }

        async void create()
        {
            employmentStatus.employment_status = t_employment.Text;

            try
            {
                var q = await Properties.Settings.Default.serverUrl
                    .WithHeader("token", Program.token)
                    .AppendPathSegment("employment")
                    .PostJsonAsync(employmentStatus);
                if (q.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    MessageBox.Show("Data berhasil disimpan!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Hide();
                    Dispose();
                    DataStore.LoadEmploymentStatus(() => { });
                }
            }
            catch (FlurlHttpException ex)
            {
                MessageBox.Show("Tidak dapat terhubung ke server!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            finally
            {
                button1.Enabled = true;
            }
        }

        private void CreateDivision_Load(object sender, EventArgs e)
        {

        }
    }
}

﻿namespace Fingerprint_Reader.forms
{
    partial class ExtraTask
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.l_nama = new System.Windows.Forms.Label();
            this.l_divisi = new System.Windows.Forms.Label();
            this.l_staken = new System.Windows.Forms.Label();
            this.l_nip = new System.Windows.Forms.Label();
            this.l_nuptk = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.waitLoading1 = new CSharp.Winform.UI.Loading.WaitLoading();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.l_jabatan = new System.Windows.Forms.Label();
            this.t_jabatan = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.permitsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.extraTasksBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tasktypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.taskpointspecDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.taskdescriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._deletetask = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.permitsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.extraTasksBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(59)))), ((int)(((byte)(68)))));
            this.pictureBox1.Image = global::Fingerprint_Reader.Properties.Resources.logo_app;
            this.pictureBox1.Location = new System.Drawing.Point(3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(87, 72);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(59)))), ((int)(((byte)(68)))));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(-1, -3);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(966, 82);
            this.label2.TabIndex = 18;
            this.label2.Text = "Daftar Tugas Tambahan";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 101);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 19);
            this.label1.TabIndex = 19;
            this.label1.Text = "Nama";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 122);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 19);
            this.label3.TabIndex = 20;
            this.label3.Text = "Divisi";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 162);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 19);
            this.label4.TabIndex = 21;
            this.label4.Text = "NIP";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 182);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 19);
            this.label5.TabIndex = 21;
            this.label5.Text = "NUPTK";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 142);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(136, 19);
            this.label6.TabIndex = 20;
            this.label6.Text = "Sta. Ketenagakerjaan";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // l_nama
            // 
            this.l_nama.AutoSize = true;
            this.l_nama.Font = new System.Drawing.Font("Segoe UI Symbol", 10F, System.Drawing.FontStyle.Bold);
            this.l_nama.Location = new System.Drawing.Point(179, 101);
            this.l_nama.Name = "l_nama";
            this.l_nama.Size = new System.Drawing.Size(166, 19);
            this.l_nama.TabIndex = 22;
            this.l_nama.Text = "Ridwan Achadi N, S.Pd";
            // 
            // l_divisi
            // 
            this.l_divisi.AutoSize = true;
            this.l_divisi.Font = new System.Drawing.Font("Segoe UI Symbol", 10F, System.Drawing.FontStyle.Bold);
            this.l_divisi.Location = new System.Drawing.Point(179, 122);
            this.l_divisi.Name = "l_divisi";
            this.l_divisi.Size = new System.Drawing.Size(166, 19);
            this.l_divisi.TabIndex = 22;
            this.l_divisi.Text = "Ridwan Achadi N, S.Pd";
            // 
            // l_staken
            // 
            this.l_staken.AutoSize = true;
            this.l_staken.Font = new System.Drawing.Font("Segoe UI Symbol", 10F, System.Drawing.FontStyle.Bold);
            this.l_staken.Location = new System.Drawing.Point(179, 142);
            this.l_staken.Name = "l_staken";
            this.l_staken.Size = new System.Drawing.Size(166, 19);
            this.l_staken.TabIndex = 22;
            this.l_staken.Text = "Ridwan Achadi N, S.Pd";
            this.l_staken.Click += new System.EventHandler(this.label9_Click);
            // 
            // l_nip
            // 
            this.l_nip.AutoSize = true;
            this.l_nip.Font = new System.Drawing.Font("Segoe UI Symbol", 10F, System.Drawing.FontStyle.Bold);
            this.l_nip.Location = new System.Drawing.Point(179, 162);
            this.l_nip.Name = "l_nip";
            this.l_nip.Size = new System.Drawing.Size(166, 19);
            this.l_nip.TabIndex = 22;
            this.l_nip.Text = "Ridwan Achadi N, S.Pd";
            // 
            // l_nuptk
            // 
            this.l_nuptk.AutoSize = true;
            this.l_nuptk.Font = new System.Drawing.Font("Segoe UI Symbol", 10F, System.Drawing.FontStyle.Bold);
            this.l_nuptk.Location = new System.Drawing.Point(179, 182);
            this.l_nuptk.Name = "l_nuptk";
            this.l_nuptk.Size = new System.Drawing.Size(166, 19);
            this.l_nuptk.TabIndex = 22;
            this.l_nuptk.Text = "Ridwan Achadi N, S.Pd";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI Symbol", 11F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(12, 222);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(189, 20);
            this.label12.TabIndex = 23;
            this.label12.Text = "Daftar Tugas Tambahan";
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tasktypeDataGridViewTextBoxColumn,
            this.taskpointspecDataGridViewTextBoxColumn,
            this.taskdescriptionDataGridViewTextBoxColumn,
            this._deletetask});
            this.dataGridView1.DataSource = this.extraTasksBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(16, 253);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(937, 235);
            this.dataGridView1.TabIndex = 24;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(356, 92);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 32);
            this.button1.TabIndex = 25;
            this.button1.Text = "Tambah";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // waitLoading1
            // 
            this.waitLoading1.Alpha = 125;
            this.waitLoading1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.waitLoading1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.waitLoading1.BindControl = null;
            this.waitLoading1.BkColor = System.Drawing.Color.WhiteSmoke;
            this.waitLoading1.IsTransparent = true;
            this.waitLoading1.Location = new System.Drawing.Point(16, 279);
            this.waitLoading1.Name = "waitLoading1";
            this.waitLoading1.Size = new System.Drawing.Size(937, 209);
            this.waitLoading1.TabIndex = 26;
            this.waitLoading1.Text = "waitLoading1";
            this.waitLoading1.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.Controls.Add(this.t_jabatan);
            this.groupBox1.Controls.Add(this.textBox3);
            this.groupBox1.Controls.Add(this.l_jabatan);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Location = new System.Drawing.Point(512, 80);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(441, 167);
            this.groupBox1.TabIndex = 27;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tambah/Edit Tugas";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 21);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(114, 19);
            this.label13.TabIndex = 26;
            this.label13.Text = "Posisi Penugasan";
            // 
            // l_jabatan
            // 
            this.l_jabatan.AutoSize = true;
            this.l_jabatan.Location = new System.Drawing.Point(6, 72);
            this.l_jabatan.Name = "l_jabatan";
            this.l_jabatan.Size = new System.Drawing.Size(92, 19);
            this.l_jabatan.TabIndex = 26;
            this.l_jabatan.Text = "Jabatan/Kelas";
            // 
            // t_jabatan
            // 
            this.t_jabatan.Location = new System.Drawing.Point(10, 94);
            this.t_jabatan.Name = "t_jabatan";
            this.t_jabatan.Size = new System.Drawing.Size(216, 25);
            this.t_jabatan.TabIndex = 27;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(228, 21);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(104, 19);
            this.label15.TabIndex = 26;
            this.label15.Text = "Deskripsi Tugas";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(232, 43);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(199, 25);
            this.textBox3.TabIndex = 27;
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(10, 43);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(216, 25);
            this.comboBox1.TabIndex = 28;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label14.Location = new System.Drawing.Point(7, 125);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(424, 32);
            this.label14.TabIndex = 29;
            this.label14.Text = "Mohon isi jabatan tugas dengan benar tanpa typo agar dapat terdeteksi di sistem";
            // 
            // permitsBindingSource
            // 
            this.permitsBindingSource.DataSource = typeof(Fingerprint_Reader.models.Summary.Permits);
            // 
            // extraTasksBindingSource
            // 
            this.extraTasksBindingSource.DataSource = typeof(Fingerprint_Reader.models.ExtraTasks);
            // 
            // tasktypeDataGridViewTextBoxColumn
            // 
            this.tasktypeDataGridViewTextBoxColumn.DataPropertyName = "task_type_printed";
            this.tasktypeDataGridViewTextBoxColumn.HeaderText = "Jenis Penugasan";
            this.tasktypeDataGridViewTextBoxColumn.Name = "tasktypeDataGridViewTextBoxColumn";
            this.tasktypeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // taskpointspecDataGridViewTextBoxColumn
            // 
            this.taskpointspecDataGridViewTextBoxColumn.DataPropertyName = "task_point_spec";
            this.taskpointspecDataGridViewTextBoxColumn.HeaderText = "Jabatan/Kelas/Posisi";
            this.taskpointspecDataGridViewTextBoxColumn.Name = "taskpointspecDataGridViewTextBoxColumn";
            // 
            // taskdescriptionDataGridViewTextBoxColumn
            // 
            this.taskdescriptionDataGridViewTextBoxColumn.DataPropertyName = "task_description";
            this.taskdescriptionDataGridViewTextBoxColumn.HeaderText = "Deskripsi";
            this.taskdescriptionDataGridViewTextBoxColumn.Name = "taskdescriptionDataGridViewTextBoxColumn";
            // 
            // _deletetask
            // 
            this._deletetask.HeaderText = "Hapus";
            this._deletetask.Name = "_deletetask";
            this._deletetask.Text = "Hapus";
            this._deletetask.UseColumnTextForButtonValue = true;
            // 
            // ExtraTask
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(965, 500);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.waitLoading1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.l_nuptk);
            this.Controls.Add(this.l_nip);
            this.Controls.Add(this.l_staken);
            this.Controls.Add(this.l_divisi);
            this.Controls.Add(this.l_nama);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label2);
            this.Font = new System.Drawing.Font("Segoe UI Symbol", 10F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "ExtraTask";
            this.Text = "ExtraTask";
            this.Load += new System.EventHandler(this.ExtraTask_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.permitsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.extraTasksBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label l_nama;
        private System.Windows.Forms.Label l_divisi;
        private System.Windows.Forms.Label l_staken;
        private System.Windows.Forms.Label l_nip;
        private System.Windows.Forms.Label l_nuptk;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DataGridView dataGridView1;
        protected System.Windows.Forms.Button button1;
        private CSharp.Winform.UI.Loading.WaitLoading waitLoading1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox t_jabatan;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label l_jabatan;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.BindingSource extraTasksBindingSource;
        private System.Windows.Forms.BindingSource permitsBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn tasktypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn taskpointspecDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn taskdescriptionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewButtonColumn _deletetask;
    }
}
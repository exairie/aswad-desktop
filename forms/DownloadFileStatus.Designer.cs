﻿namespace Fingerprint_Reader.forms
{
    partial class DownloadFileStatus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DownloadFileStatus));
            this.waitLoading1 = new CSharp.Winform.UI.Loading.WaitLoading();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // waitLoading1
            // 
            this.waitLoading1.Alpha = 125;
            this.waitLoading1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.waitLoading1.BindControl = null;
            this.waitLoading1.BkColor = System.Drawing.Color.WhiteSmoke;
            this.waitLoading1.IsTransparent = true;
            this.waitLoading1.Location = new System.Drawing.Point(9, 5);
            this.waitLoading1.Margin = new System.Windows.Forms.Padding(4);
            this.waitLoading1.Name = "waitLoading1";
            this.waitLoading1.Size = new System.Drawing.Size(76, 70);
            this.waitLoading1.TabIndex = 0;
            this.waitLoading1.Text = "waitLoading1";
            this.waitLoading1.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(96, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Mohon tunggu";
            // 
            // DownloadFileStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(361, 79);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.waitLoading1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(808, 153);
            this.MinimizeBox = false;
            this.Name = "DownloadFileStatus";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Downloading...";
            this.Load += new System.EventHandler(this.DownloadFileStatus_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CSharp.Winform.UI.Loading.WaitLoading waitLoading1;
        private System.Windows.Forms.Label label1;
    }
}
﻿using Fingerprint_Reader.models;
using Flurl;
using Flurl.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fingerprint_Reader.forms
{
    public partial class ReportEntry : Form
    {
        class GridData
        {
            public string name { get; set; }
            public string entry { get; set; }
            public string exit { get; set; }
        }
        List<GridData> gridData = new List<GridData>();
        public ReportEntry()
        {
            InitializeComponent();
        }

        private void ReportEntry_Load(object sender, EventArgs e)
        {
            label2.Text = DateTime.Now.ToString("dd MMM yyyy");
            loadReport();
        }
        string getEntryDateValue(Attendances a)
        {
            if (a == null) return "-";
            else if (a.entry_date == null) return "-";
            else return HomePage.normalizeDate(a.entry_date.Value);
        }
        string getExitDateValue(Attendances a)
        {
            if (a == null) return "-";
            else if (a.exit_date == null) return "-";
            else return HomePage.normalizeDate(a.exit_date.Value);
        }
        
        async void loadReport()
        {
            var data = await Properties.Settings.Default.serverUrl
                    .AppendPathSegments(new string[] { "attendance_log", "logtoday" })
                    .GetJsonAsync<Attendances[]>();

            var attendances = from enroll in DataStore.enrollments
                       join att in data on enroll.id equals att.enrollment_id into ps
                       from p in ps.DefaultIfEmpty()
                       select new GridData(){
                           name = enroll.firstname,
                           entry = getEntryDateValue(ps.FirstOrDefault()),
                           exit = getExitDateValue(ps.FirstOrDefault())
                       };
            gridData = attendances.ToList();
            dataGridView1.DataSource = gridData;
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if(textBox1.Text == "")
            {
                dataGridView1.DataSource = gridData;
            }else
            {
                dataGridView1.DataSource = (from d in gridData
                                            where d.name.ToLower().Contains(textBox1.Text)
                                            select d).ToList();
            }
            
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Flurl;
using Flurl.Http;
using Fingerprint_Reader.models;
using Newtonsoft.Json;
using System.Net;
using System.Diagnostics;

namespace Fingerprint_Reader.forms
{
    public partial class ReportLate : Form
    {
        Dictionary<string, string> mapDivisi = new Dictionary<string, string>();
        public ReportLate()
        {
            InitializeComponent();

            mapDivisi.Add("guru", "Guru");
            mapDivisi.Add("tata_usaha", "Tata Usaha");
        }

        private void ReportLate_Load(object sender, EventArgs e)
        {

            //this.reportViewer1.RefreshReport();
            comboBox1.DisplayMember = "Value";
            comboBox1.ValueMember = "Key";

            comboBox1.DataSource = mapDivisi.ToList();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //loadReport();
        }

        async void loadReport()
        {
            try
            {
                var req = await Properties.Settings.Default.serverUrl
                    .WithHeader("token", Program.token)
                    .AppendPathSegments(new string[] { "attendance", "latereport" })
                    .PostJsonAsync(new
                    {
                        from = dateTimePicker1.Value.ToString("yyyy-MM-dd"),
                        to = dateTimePicker2.Value.ToString("yyyy-MM-dd")
                    });

                var resp = await req.Content.ReadAsStringAsync();

                var data = JsonConvert.DeserializeObject<Enrollments[]>(resp);
                
                reportViewer1.LocalReport.ReportPath = Environment.CurrentDirectory + @"/reports/ReportLateAccumulation.rdlc";
                reportViewer1.LocalReport.DataSources.Clear();                
                reportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WinForms.ReportDataSource("ds", data));
                reportViewer1.LocalReport.Refresh();
                reportViewer1.RefreshReport();
                reportViewer1.Refresh();

            }catch(FlurlHttpException e)
            {
                Console.Write(e);
            }catch(JsonException j)
            {
                Console.Write(j);
            }
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {

        }
        async void download(string filename)
        {
            var waiter = new DownloadFileStatus();
            waiter.TopMost = true;
            string division = "";
            Invoke(new Action(() => { division = comboBox1.SelectedValue.ToString(); }));
            try
            {                
                waiter.Show();
                var wClient = new WebClient();
                await Task.Run(() =>
                {
                    try
                    {
                        wClient.DownloadFile(Properties.Settings.Default.serverUrl +
                            String.Format("export/fulldetail?from={0}&to={1}&division_id={2}",
                            dateTimePicker1.Value.ToString("yyyy-MM-dd"),
                            dateTimePicker2.Value.ToString("yyyy-MM-dd"),
                            division
                            ), filename);
                        Invoke(new Action(() =>
                        {
                            waiter.finish();
                        }));
                        var dialog = MessageBox.Show("File telah disimpan! Buka file?", "Berhasil", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                        if (dialog == DialogResult.Yes)
                        {
                            Process.Start(filename);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.StackTrace);
                        Invoke(new Action(() =>
                        {
                            MessageBox.Show("File gagal disimpan!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            waiter.finish();
                        }));
                    }
                    finally
                    {
                        
                    }
                });
                
            }catch(Exception e)
            {
                MessageBox.Show("File gagal disimpan!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }            
        }
        private void button2_Click(object sender, EventArgs e)
        {
            var dialog = new SaveFileDialog();
            dialog.Filter = "Excel File (*.xlsx)|*.xlsx";
            dialog.FileName = String.Format("Report {0}, {1} - {2}", comboBox1.SelectedValue.ToString(), dateTimePicker1.Value.ToString("yyyy-MM-dd"), dateTimePicker2.Value.ToString("yyyy-MM-dd"));

            var result = dialog.ShowDialog(); //shows save file dialog
            if (result == DialogResult.OK)
            {
                Console.WriteLine("writing to: " + dialog.FileName); //prints the file to save

                try
                {
                    download(dialog.FileName);                    
                }
                catch (Exception)
                {
                    MessageBox.Show("File gagal disimpan!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}

﻿using Fingerprint_Reader.models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fingerprint_Reader.forms
{
    public partial class EmploymentStatusList : Form
    {
        public EmploymentStatusList()
        {
            InitializeComponent();
        }

        private void DivisionList_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = DataStore.employmentStatuses;
            progressBar1.Value = 80;
            DataStore.LoadEmploymentStatus(() =>
            {
                progressBar1.Value = 100;
                progressBar1.Hide();
            });
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void divisiBaruToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new CreateEmploymentStatus().ShowDialog();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {
                var data = (BindingList<EmploymentStatus>)senderGrid.DataSource;
                new CreateEmploymentStatus(data[e.RowIndex]).ShowDialog();
            }
        }
    }
}

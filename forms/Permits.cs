﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Flurl;
using Flurl.Http;
using Fingerprint_Reader.models;

namespace Fingerprint_Reader.forms
{
    public partial class Permits : Form
    {
        public Permits()
        {
            InitializeComponent();
        }

        private void Permits_Load(object sender, EventArgs e)
        {
            dataGridView1.AutoGenerateColumns = false;
        }
        async void loadPermits()
        {
            try
            {
                DataStore.LoadPermits(() =>
                {
                    var list = DataStore.permits;

                    dataGridView1.DataSource = DataStore.permits;

                    for (int i = 0; i < list.Count; i++)
                    {
                        var d = list[i];
                        // Tanggal berlaku
                        dataGridView1[3, i].Value = String.Format("{0} - {1}",
                            d.permit_from.Value.ToString("dd MMM yyyy"),
                            d.permit_from.Value.ToString("dd MMM yyyy"));
                        // Jam masuk
                        dataGridView1[4, i].Value = String.Format("{0}:{1}",
                            d.entry_hour.Value.ToString("#0"),
                            d.entry_minute.Value.ToString("#0"));
                        // Jam keluar
                        dataGridView1[4, i].Value = String.Format("{0}:{1}",
                            d.exit_hour.Value.ToString("#0"),
                            d.exit_minute.Value.ToString("#0"));
                    }
                });
                

            }catch(FlurlHttpException e)
            {

            }
        }

        private void buatDispensasiBaruToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new PermitInput().ShowDialog();
        }

        private void Permits_Activated(object sender, EventArgs e)
        {
            loadPermits();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
    }
}

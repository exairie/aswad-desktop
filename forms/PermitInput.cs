﻿using Fingerprint_Reader.libs;
using Fingerprint_Reader.models;
using Flurl;
using Flurl.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fingerprint_Reader.forms
{    
    public partial class PermitInput : Form
    {
        public event VoidCallback ReopenForm;
        bool disposable = false;
        Enrollments selectedUser = null;
        models.Permits lastPermit = null;
        Enrollments SelectedUser
        {
            get
            {
                return selectedUser;
            }set
            {
                selectedUser = value;
                useData(selectedUser);
            }
        }
        public PermitInput()
        {
            InitializeComponent();
        }
        public PermitInput(bool disposable)
        {
            InitializeComponent();            
            this.disposable = true;            
        }

        private void PermitInput_Load(object sender, EventArgs e)
        {
            
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = DataStore.enrollments;         
            this.SelectedUser = null;

            label2.Text = "Buat Izin Dispensasi";

            if(Program.AdminSession == null)
            {
                new Login().ShowDialog();
                if(Program.AdminSession == null)
                {
                    MessageBox.Show("Anda harus login terlebih dahulu!", "Akses tidak diperbolehkan", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    Hide();
                    Dispose();
                    return;
                }

                if (!PermissionCheck.checkPermission(Program.AdminSession, PermissionCheck.Permission.AddPermit))
                {
                    MessageBox.Show("Anda tidak memiliki hak akses untuk membuka halaman ini!", "Forbidden", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Hide();
                    Dispose();
                    return;
                }

                label2.Text = "Buat Izin Dispensasi\nAnda Login Sebagai " + Program.AdminSession.name;
                return;
            }            
        }
        async void loadEnrollments()
        {
            waitLoading1.Visible = true;

            DataStore.LoadEnrollments(() =>
            {
                waitLoading1.Visible = false;
            });
        }

        private void PermitInput_Activated(object sender, EventArgs e)
        {
            loadEnrollments();
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            string search = ((TextBox)sender).Text;
            if(search != "")
            {
                var q = from d in DataStore.enrollments
                        where d.firstname.ToLower().Contains(search) ||
                        d.lastname.ToLower().Contains(search)
                        select d;

                dataGridView1.DataSource = q.ToList();
            }else
            {
                dataGridView1.DataSource = DataStore.enrollments;
            }

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0) return;
            if(dataGridView1.Columns[e.ColumnIndex] is DataGridViewButtonColumn)
            {
                var grid = ((DataGridView)sender);
                if(grid.DataSource is BindingList<Enrollments>)
                    this.SelectedUser = ((BindingList<Enrollments>)grid.DataSource)[e.RowIndex];
                else if(grid.DataSource is List<Enrollments>)
                    this.SelectedUser = ((List<Enrollments>)grid.DataSource)[e.RowIndex];
            }
            
        }

        private void useData(Enrollments p)
        {
            if(p == null)
            {
                l_nama.Text = "-";
                l_nip.Text = "-";
                l_divisi.Text = "-";
                l_status_pn.Text = "-";
                l_izin.Text = "-";
                l_izin_bulan.Text = "-";
                group_input.Enabled = false;
                return;
            }
            l_permitby.Text = Program.AdminSession.name;
            l_nama.Text = String.Format("{0} {1}", p.firstname, p.lastname);
            l_nip.Text = p.nip;
            l_divisi.Text = p.division.division;
            l_status_pn.Text = p.employment_status.employment_status;

            group_input.Enabled = true;

            getPermitCounts(p);
        }
        async void getPermitCounts(Enrollments enrollment)
        {
            var q = from p in DataStore.permits
                    where p.enrollment_id == enrollment.id
                    select p;

            l_izin.Text = q.Count() + " Kali";

            var qmonth = from p in q
                         where p.created_at.Value.ToString("MMyyyy") == DateTime.Now.ToString("MMyyyy")
                         select p;

            l_izin_bulan.Text = qmonth.Count() + " Kali";

            // Check permit from server
            try
            {
                var pcheck = await Properties.Settings.Default.serverUrl
                    .WithHeader("token",Program.token)
                    .AppendPathSegments(new String[] { "permits", "getpermit", enrollment.id.ToString() })
                    .GetJsonAsync<models.Permits>();
                if(pcheck != null)
                {
                    l_telah_ada_izin.Visible = true;
                    group_input.Enabled = false;
                    save_btn.Enabled = false;
                    this.lastPermit = pcheck;
                    r_tipe_sakit.Checked = lastPermit.permit_type == "sakit";
                    r_tipe_izin.Checked = lastPermit.permit_type == "izin";
                    r_tipe_izin_lain.Checked = lastPermit.permit_type == "izin_lain";
                    r_tipe_cuti.Checked = lastPermit.permit_type == "cuti";
                    r_tipe_cuti_lain.Checked = lastPermit.permit_type == "cuti_lain";

                    l_reason.Text = lastPermit.reason;
                    l_permitby.Text = lastPermit.permit_by;
                    l_from.Value = lastPermit.permit_from.Value;
                    l_to.Value = lastPermit.permit_to.Value;

                    bool isTheSameAdmin = pcheck.permit_by == Program.AdminSession.name;      
                             
                    l_nopermit.Visible = !isTheSameAdmin;
                    button1.Visible = isTheSameAdmin;
                    button2.Visible = isTheSameAdmin;                    

                }
                else
                {
                    save_btn.Enabled = true;
                    flowLayoutPanel2.Enabled = true;

                    l_telah_ada_izin.Visible = false;
                    
                }
            }
            catch (Exception cex)
            {
                Console.WriteLine(cex);
                MessageBox.Show(cex.Message);
            }

        }

        private void group_bio_Enter(object sender, EventArgs e)
        {

        }

        private void label20_Click(object sender, EventArgs e)
        {

        }
        bool validateForm()
        {
            l_from.Value = new DateTime(l_from.Value.Year, l_from.Value.Month, l_from.Value.Day, 0, 0, 0);
            l_to.Value = new DateTime(l_to.Value.Year, l_to.Value.Month, l_to.Value.Day, 23, 59, 59);
            if (SelectedUser == null) return false;
            if (l_reason.Text == "") return false;
            if (l_permitby.Text == "") return false;            
            if (l_to.Value < l_from.Value) return false;
            if (!r_tipe_izin.Checked && !r_tipe_izin_lain.Checked && !r_tipe_sakit.Checked && !r_tipe_cuti.Checked && !r_tipe_cuti_lain.Checked) return false;
            return true;
        }
        private void save_btn_Click(object sender, EventArgs e)
        {
            if (!validateForm())
            {
                MessageBox.Show("Cek kembali input anda! Pastikan: \n" +
                    "1. Pemohon izin tidak boleh kosong\n" +
                    "2. Alasan dan Pemberi Izin tidak kosong\n" +
                    "3. Tanggal awal berlaku izin tidak boleh kosong\n" +
                    "4. Tanggal akhir izin tidak kurang dari tanggal awal berlaku\n" +
                    "5. Tipe izin harus dipilih", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if(this.lastPermit == null)
            {
                savePermit();
            }
            else
            {
                updatePermit();
            }
        }
        models.Permits populateData(models.Permits permit)
        {
            permit.bypass_attendance = 0;
            permit.entry_hour = l_from.Value.Hour;
            permit.entry_minute = l_from.Value.Minute;

            permit.exit_hour = l_to.Value.Hour;
            permit.exit_minute = l_to.Value.Minute;

            permit.permit_from = l_from.Value;
            permit.permit_to = l_to.Value;

            permit.enrollment_id = SelectedUser.id;

            permit.reason = l_reason.Text;
            permit.permit_by = l_permitby.Text;
            permit.status = "active";
            if (r_tipe_izin.Checked)
            {
                permit.permit_type = "izin";
            }
            else if (r_tipe_izin_lain.Checked)
            {
                permit.permit_type = "izin_lain";
            }
            else if(r_tipe_sakit.Checked)
            {
                permit.permit_type = "sakit";
            }else if (r_tipe_cuti.Checked)
            {
                permit.permit_type = "cuti";
            }
            else if (r_tipe_cuti_lain.Checked)
            {
                permit.permit_type = "cuti_lain";
            }
            return permit;
        }
        async void updatePermit()
        {
            populateData(this.lastPermit);
            try
            {
                save_btn.Enabled = false;
                var resp = await Properties.Settings.Default.serverUrl
                       .WithHeader("token",Program.token)
                       .AppendPathSegments(new string[] { "permits", this.lastPermit.id.ToString() })
                       .PutJsonAsync(this.lastPermit);
                if (resp.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    MessageBox.Show("Informasi izin telah diupdate!", "Berhasil", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Hide();
                    Dispose();
                }
                else
                {
                    MessageBox.Show("Informasi izin gagal diupdate!", "Gagal", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Informasi izin gagal diupdate!", "Gagal", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally
            {
                save_btn.Enabled = true;
            }
        }
        async void savePermit()
        {
            models.Permits permit = new models.Permits();
            populateData(permit);      

            var check = await Properties.Settings.Default.serverUrl
                .AppendPathSegments(new string[] { "permits", "checkuserpermit", permit.enrollment_id.ToString() })
                .WithHeader("token", Program.token)
                .PostJsonAsync(new
                {
                    from = permit.permit_from,
                    to = permit.permit_to
                });
            if(check.StatusCode == System.Net.HttpStatusCode.InternalServerError)
            {
                MessageBox.Show("Internal Server Error!","Error",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
                return;
            }
            else if(check.StatusCode != System.Net.HttpStatusCode.OK)
            {
                MessageBox.Show("Pegawai telah mengajukan izin pada tanggal tersebut. Silahkan pilih tanggal lain!"
                    ,"Tidak Diizinkan", MessageBoxButtons.OK,MessageBoxIcon.Information);
                return;
            }            

            save_btn.Text = "Menyimpan...";
            save_btn.Enabled = false;
            try
            {
                var resp = await Properties.Settings.Default.serverUrl
                    .WithHeader("token",Program.token)
                    .AppendPathSegments(new string[] { "permits" })
                    .PostJsonAsync(permit);

                if(resp.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    MessageBox.Show("Simpan berhasil!", "Sukses", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Hide();
                    Dispose();
                }else
                {
                    MessageBox.Show("Simpan gagal!", "Sukses", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch(FlurlHttpException e)
            {
                await e.GetResponseJsonAsync();
                MessageBox.Show("Simpan gagal! Terjadi kesalahan", "Sukses", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Console.Write(e);
            }
            finally
            {
                save_btn.Enabled = true;
                save_btn.Text = "Simpan";

                DataStore.LoadPermits(() => { });
            }

        }

        private void waitLoading1_Click(object sender, EventArgs e)
        {

        }

        private void PermitInput_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (disposable == true)
            {   
                var dialog = MessageBox.Show("Ulangi input izin?", "Informasi", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (dialog == DialogResult.Yes)
                {
                    Hide();
                    if(ReopenForm != null) this.ReopenForm();
                }
                else
                {
                    Program.token = null;
                    Program.AdminSession = null;
                }

            }
        }

        private void r_tipe_izin_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            save_btn.Enabled = true;
            group_input.Enabled = true;            
        }
        async void cancelPermit()
        {
            if (this.lastPermit == null) return;
            try
            {
                save_btn.Enabled = false;
                var resp = await Properties.Settings.Default.serverUrl
                    .WithHeader("token",Program.token)
                    .AppendPathSegments(new String[] { "permits", lastPermit.id.ToString() })
                    .DeleteAsync();
                if(resp.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    MessageBox.Show("Izin telah dibatalkan!", "Informasi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Hide();
                    Dispose();
                }
                else
                {
                    MessageBox.Show("Gagal menghapus izin!", "Informasi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Koneksi ke server bermasalah!", "Informasi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally
            {
                save_btn.Enabled = true;
            }
        }
        void reinput()
        {
            if (disposable == true)
            {
                var dialog = MessageBox.Show("Ulangi input izin?", "Informasi", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (dialog == DialogResult.Yes)
                {
                    Hide();
                    if (ReopenForm != null) this.ReopenForm();
                }
                else
                {
                    Program.token = null;
                    Program.AdminSession = null;
                }

            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            // Batalkan izin
            var dialog = MessageBox.Show("Batalkan izin?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if(dialog == DialogResult.Yes)
            {
                cancelPermit();
            }
        }
    }
}

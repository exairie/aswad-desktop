﻿namespace Fingerprint_Reader.forms
{
    partial class AddPhoto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddPhoto));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureone = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.picturetwo = new System.Windows.Forms.PictureBox();
            this.pickphoto2 = new System.Windows.Forms.Button();
            this.pickphoto1 = new System.Windows.Forms.Button();
            this.btnsave = new System.Windows.Forms.Button();
            this.l_enrollment_name = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picturetwo)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(59)))), ((int)(((byte)(68)))));
            this.pictureBox1.Image = global::Fingerprint_Reader.Properties.Resources.logo_app;
            this.pictureBox1.Location = new System.Drawing.Point(3, 1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(79, 57);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 15;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(59)))), ((int)(((byte)(68)))));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(-1, -2);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(393, 60);
            this.label2.TabIndex = 14;
            this.label2.Text = "Tambahkan Foto";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("Segoe UI Symbol", 10F, System.Drawing.FontStyle.Italic);
            this.label1.Location = new System.Drawing.Point(12, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(366, 63);
            this.label1.TabIndex = 16;
            this.label1.Text = "Silahkan input foto pegawai sesuai dengan nama pegawai yang tertera di bawah beri" +
    "kut";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(148, 170);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 20);
            this.label3.TabIndex = 17;
            this.label3.Text = "Foto Pegawai";
            // 
            // pictureone
            // 
            this.pictureone.Location = new System.Drawing.Point(83, 202);
            this.pictureone.Name = "pictureone";
            this.pictureone.Size = new System.Drawing.Size(226, 223);
            this.pictureone.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureone.TabIndex = 18;
            this.pictureone.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoEllipsis = true;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(599, 213);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 20);
            this.label4.TabIndex = 17;
            this.label4.Text = "Foto Kedua";
            this.label4.Visible = false;
            // 
            // picturetwo
            // 
            this.picturetwo.Location = new System.Drawing.Point(534, 229);
            this.picturetwo.Name = "picturetwo";
            this.picturetwo.Size = new System.Drawing.Size(226, 222);
            this.picturetwo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picturetwo.TabIndex = 18;
            this.picturetwo.TabStop = false;
            this.picturetwo.Visible = false;
            // 
            // pickphoto2
            // 
            this.pickphoto2.Font = new System.Drawing.Font("Segoe UI Symbol", 9F);
            this.pickphoto2.Location = new System.Drawing.Point(534, 458);
            this.pickphoto2.Name = "pickphoto2";
            this.pickphoto2.Size = new System.Drawing.Size(226, 29);
            this.pickphoto2.TabIndex = 19;
            this.pickphoto2.Text = "Browse";
            this.pickphoto2.UseVisualStyleBackColor = true;
            this.pickphoto2.Visible = false;
            this.pickphoto2.Click += new System.EventHandler(this.pickphoto2_Click);
            // 
            // pickphoto1
            // 
            this.pickphoto1.Font = new System.Drawing.Font("Segoe UI Symbol", 9F);
            this.pickphoto1.Location = new System.Drawing.Point(83, 431);
            this.pickphoto1.Name = "pickphoto1";
            this.pickphoto1.Size = new System.Drawing.Size(226, 30);
            this.pickphoto1.TabIndex = 19;
            this.pickphoto1.Text = "Browse";
            this.pickphoto1.UseVisualStyleBackColor = true;
            this.pickphoto1.Click += new System.EventHandler(this.pickphoto1_Click);
            // 
            // btnsave
            // 
            this.btnsave.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnsave.Location = new System.Drawing.Point(16, 474);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(362, 40);
            this.btnsave.TabIndex = 20;
            this.btnsave.Text = "Simpan";
            this.btnsave.UseVisualStyleBackColor = true;
            this.btnsave.Click += new System.EventHandler(this.button3_Click);
            // 
            // l_enrollment_name
            // 
            this.l_enrollment_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.l_enrollment_name.Font = new System.Drawing.Font("Segoe UI Symbol", 11F, System.Drawing.FontStyle.Bold);
            this.l_enrollment_name.Location = new System.Drawing.Point(12, 127);
            this.l_enrollment_name.Name = "l_enrollment_name";
            this.l_enrollment_name.Size = new System.Drawing.Size(366, 26);
            this.l_enrollment_name.TabIndex = 21;
            this.l_enrollment_name.Text = "label5";
            this.l_enrollment_name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // AddPhoto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(390, 523);
            this.Controls.Add(this.l_enrollment_name);
            this.Controls.Add(this.btnsave);
            this.Controls.Add(this.pickphoto1);
            this.Controls.Add(this.pickphoto2);
            this.Controls.Add(this.picturetwo);
            this.Controls.Add(this.pictureone);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label2);
            this.Font = new System.Drawing.Font("Segoe UI Symbol", 11F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "AddPhoto";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AddPhoto";
            this.Load += new System.EventHandler(this.AddPhoto_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picturetwo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureone;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox picturetwo;
        private System.Windows.Forms.Button pickphoto2;
        private System.Windows.Forms.Button pickphoto1;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.Label l_enrollment_name;
    }
}
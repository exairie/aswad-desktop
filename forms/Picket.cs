﻿using Fingerprint_Reader.models;
using Flurl;
using Flurl.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fingerprint_Reader.forms
{
    public partial class Picket : Form
    {
        class GridData
        {
            public string hari { get; set; }
            public string guru { get; set; }
            public models.Picket picket { get; set; }
        }
        Enrollments e;
        Enrollments Enrollment
        {
            get { return e; }
            set
            {
                e = value;
                l_firstname.Text = value != null?value.firstname:"-";
            }
        }
        public Picket()
        {
            InitializeComponent();
        }
        void setupComboBox()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("senin", "Senin");
            dict.Add("selasa", "Selasa");
            dict.Add("rabu", "Rabu");
            dict.Add("kamis", "Kamis");
            dict.Add("jumat", "Jumat");

            comboBox1.DisplayMember = "Value";
            comboBox1.ValueMember = "Key";
            comboBox1.DataSource = dict.ToList();
        }
        private void Picket_Load(object sender, EventArgs e)
        {
            dataGridView1.AutoGenerateColumns = false;
            setupComboBox();
            this.Enrollment = null;
            loadData();
        }
        string capitalize(string s)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            // Return char and concat substring.
            return char.ToUpper(s[0]) + s.Substring(1);
        }
        async void loadData()
        {
            waitLoading1.Visible = true;
            try
            {
                var resp = await Properties.Settings.Default.serverUrl
                       .WithHeader("token",Program.token)
                       .AppendPathSegments(new string[] { "picket" })
                       .GetJsonAsync<models.Picket[]>();

                dataGridView1.DataSource = (from r in resp
                                            select new GridData()
                                            {
                                                picket = r,
                                                hari = capitalize(r.day),
                                                guru = r.enrollment.firstname,
                                            }).ToList();
            }
            catch (Exception e)
            {                
                Console.WriteLine(e.StackTrace);
            }
            finally
            {
                waitLoading1.Visible = false;
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            new SelectEnrollment("Pilih Guru Piket", (enrollment) =>
            {
                this.Enrollment = enrollment;
            }).Show();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            save();
        }
        async void save()
        {
            if(this.Enrollment == null)
            {
                MessageBox.Show("Silahkan pilih guru piket!", "Guru piket belum dipilih", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            try
            {
                var resp = await Properties.Settings.Default.serverUrl
                    .WithHeader("token",Program.token)
                    .AppendPathSegment("picket")
                    .PostJsonAsync(new models.Picket()
                    {
                        day = comboBox1.SelectedValue.ToString(),
                        enrollment_id = this.Enrollment.id
                    });
                if(resp.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    MessageBox.Show("Data telah disimpan!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Enrollment = null;
                    loadData();
                }
            }
            catch (FlurlHttpException ex)
            {
                if (ex.Call.HttpStatus == (System.Net.HttpStatusCode)422)
                {

                    MessageBox.Show("Data sudah ada!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);return;
                }
                MessageBox.Show("Data gagal disimpan! " + ex.Message, "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0) return;
            if (e.ColumnIndex < 0) return;
            if(dataGridView1[e.ColumnIndex,e.RowIndex] is DataGridViewButtonCell)
            {
                var data = (List<GridData>)dataGridView1.DataSource;
                var guru = data[e.RowIndex];

                var conf = MessageBox.Show("Hapus Data?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if(conf == DialogResult.Yes)
                {
                    delete(guru.picket);
                }
            }
        }
        async void delete(models.Picket p)
        {
            try
            {
                var resp = await Properties.Settings.Default.serverUrl
                    .WithHeader("token", Program.token)
                    .AppendPathSegments(new string[] { "picket" , p.id.ToString()})
                    .DeleteAsync();
                if (resp.StatusCode == System.Net.HttpStatusCode.Accepted)
                {
                    MessageBox.Show("Data telah dihapus!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Enrollment = null;
                    loadData();
                }else if (resp.StatusCode == System.Net.HttpStatusCode.Accepted)
                {
                    MessageBox.Show("Data tidak ditemukan!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    loadData();
                }
            }
            catch (FlurlHttpException ex)
            {                
                MessageBox.Show("Data gagal disimpan! " + ex.Message, "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}

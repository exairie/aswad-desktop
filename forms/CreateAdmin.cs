﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Flurl;
using Flurl.Http;
using Fingerprint_Reader.models;

namespace Fingerprint_Reader.forms
{    
    public partial class CreateAdmin : Form
    {
        models.Admins admin = new models.Admins();
        bool edit = false;
        DivisionList parent;
        public CreateAdmin()
        {
            InitializeComponent();
        }
        public CreateAdmin withParent(DivisionList parent)
        {
            this.parent = parent;
            return this;
        }
        public CreateAdmin(models.Admins data)
        {
            InitializeComponent();
            this.admin = data;
            edit = true;

            t_division.Text = admin.name;
            textBox1.Text = admin.username;
            textBox2.Text = admin.password;
                  
        }
        bool validate()
        {
            if(t_division.Text.Length < 1)
            {
                return false;
            }            
            return true;
        }
        async private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            if (!validate())
            {
                MessageBox.Show("Cek kembali input anda!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }

            if (edit)
            {
                update();
            }else
            {
                create();
            }
        }

        async void update()
        {
            admin.username = textBox1.Text;
            admin.password = textBox2.Text;
            admin.name = t_division.Text;
            admin.phone = "";
            admin.email = "";
            admin.level = (int)comboBox1.SelectedValue;
            try
            {
                var q = await Properties.Settings.Default.serverUrl
                    .WithHeader("token", Program.token)
                    .AppendPathSegment("admin")
                    .AppendPathSegment(admin.id.ToString())
                    .PutJsonAsync(admin);
                if (q.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    MessageBox.Show("Data berhasil disimpan!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Hide();
                    Dispose();
                    DataStore.LoadDivisions(() => {
                        if (parent != null)
                        {
                            Invoke(new Action(() =>
                            {
                                parent.formatDate();
                            }));
                        }
                    });
                    
                }
            }
            catch (FlurlHttpException ex)
            {
                MessageBox.Show("Tidak dapat terhubung ke server!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            finally
            {
                button1.Enabled = true;
            }
        }

        async void create()
        {
            admin = new models.Admins();
            admin.username = textBox1.Text;
            admin.password = textBox2.Text;
            admin.name = t_division.Text;
            admin.phone = "";
            admin.email = "";
            admin.level = (int)comboBox1.SelectedValue;
            try
            {
                var q = await Properties.Settings.Default.serverUrl
                    .WithHeader("token", Program.token)
                    .AppendPathSegment("admin")
                    .PostJsonAsync(admin);
                if (q.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    MessageBox.Show("Data berhasil disimpan!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Hide();
                    Dispose();
                    DataStore.LoadDivisions(() => {
                        if (parent != null)
                        {
                            Invoke(new Action(() =>
                            {
                                parent.formatDate();
                            }));
                        }
                    });
                }
            }
            catch (FlurlHttpException ex)
            {
                MessageBox.Show("Tidak dapat terhubung ke server!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            finally
            {
                button1.Enabled = true;
            }
        }

        private void CreateDivision_Load(object sender, EventArgs e)
        {
            if (Program.AdminSession == null) return;
            var dict = new Dictionary<int, string>();
            dict.Add(99999999, "Super Admin");
            dict.Add(3, "Power User (WMM/Kur)");
            dict.Add(2, "User (Operator)");

            List<KeyValuePair<int, string>> levelList = new List<KeyValuePair<int, string>>();

            foreach(var d in dict.ToArray())
            {
                if(Program.AdminSession.level >= d.Key) {
                    levelList.Add(d);
                }
            }
            comboBox1.ValueMember = "Key";
            comboBox1.DisplayMember = "Value";
            comboBox1.DataSource = levelList;

            if (edit)
            {
                comboBox1.SelectedValue = admin.level;
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}

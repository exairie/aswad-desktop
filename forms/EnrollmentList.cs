﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Flurl;
using Flurl.Http;
using Fingerprint_Reader.models;

namespace Fingerprint_Reader.forms
{
    public partial class EnrollmentList : Form
    {
        public EnrollmentList()
        {
            InitializeComponent();
        }

        private void EnrollmentList_Deactivate(object sender, EventArgs e)
        {
            //Environment.Exit(0);
        }

        private void toolStripStatusLabel1_Click(object sender, EventArgs e)
        {
        }

        private void tambahDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new EnrollUser().ShowDialog();
        }

        private void statusStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void EnrollmentList_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

        private void EnrollmentList_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = DataStore.enrollments;
            loadData();
        }
        async void loadData()
        {
            dataGridView1.DataSource = null;
            panel1.Visible = true;
            DataStore.LoadEnrollments(() =>
            {
                panel1.Visible = false;
                dataGridView1.DataSource = DataStore.enrollments.OrderBy((e) => e.firstname).ToList();
            });
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var grid = (DataGridView)sender;
            if (e.RowIndex < 0) return;
            Enrollments selectedUser = null;
            if (grid.DataSource is BindingList<Enrollments>)
            {
                var griddata = (BindingList<Enrollments>)grid.DataSource;
                selectedUser = griddata[e.RowIndex];
            }
            else if (grid.DataSource is List<Enrollments>)
            {
                var griddata = (List<Enrollments>)grid.DataSource;
                selectedUser = griddata[e.RowIndex];
            }
            if (selectedUser == null)
            {
                MessageBox.Show("Invalid enrollment data");
                return;
            }
            if (grid.Columns[e.ColumnIndex] is DataGridViewButtonColumn)
            {
                var col = grid.Columns[e.ColumnIndex];
                if (col.Name == "_editbtn")
                {

                    new EnrollUser(selectedUser).ShowDialog();
                }

                if (col.Name == "_picbtn")
                {
                    new AddPhoto(selectedUser).ShowDialog();
                }

                if (col.Name == "_personalschedule")
                {
                    new CustomSchedule(selectedUser).ShowDialog();
                }
                if (col.Name == "_extratask")
                {
                    new ExtraTask(selectedUser).ShowDialog();
                }
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            new EnrollUser().ShowDialog();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (textBox1.Text == "")
            {
                dataGridView1.DataSource = DataStore.enrollments;
            }
            else
            {
                var squery = textBox1.Text.ToLower();
                var q = from d in DataStore.enrollments
                        where d.firstname.ToLower().Contains(squery) ||
                            d.lastname.ToLower().Contains(squery) ||
                            d.nip.ToLower().Contains(squery) ||
                            d.nik.ToLower().Contains(squery)
                        select d;

                dataGridView1.DataSource = q.ToList();
            }
        }
    }
}

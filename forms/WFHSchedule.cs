﻿using Fingerprint_Reader.models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Flurl;
using Flurl.Http;

namespace Fingerprint_Reader.forms
{
    public partial class WFHSchedule : Form
    {
        public class EnrollmentWFH : Enrollments
        {
            public bool isWfhToday { get; set; }
            public EnrollmentWFH(Enrollments data, bool isWfh)
            {
                this.firstname = data.firstname;
                this.lastname = data.lastname;
                this.gender = data.gender;
                this.division = data.division;
                this.id = data.id;

                this.isWfhToday = isWfh;
            }
        }
        BindingList<EnrollmentWFH> wfhdata = new BindingList<EnrollmentWFH>();
        public WFHSchedule()
        {
            InitializeComponent();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void WFHSchedule_Load(object sender, EventArgs e)
        {
            loadWFHScheduleBySelectedDate();
            wfh_date.Text = dateWfh.Value.ToLongDateString();
        }

        void loadWFHScheduleBySelectedDate()
        {
            Invoke(new Action(() => { waitLoading1.Visible = true; }));
            DataStore.LoadWFHSchedules(dateWfh.Value, () =>
            {
                wfhdata = new BindingList<EnrollmentWFH>();
                foreach (var enrollment in DataStore.enrollments)
                {
                    var isUserWfhToday = DataStore.wfhSchedule.Count(x => x.enrollment_id == enrollment.id) > 0;
                    wfhdata.Add(new EnrollmentWFH(enrollment, isUserWfhToday));
                }
                dataGridView1.DataSource = wfhdata;
                Invoke(new Action(() => { waitLoading1.Visible = false; }));
            });
        }

        private void dateWfh_ValueChanged(object sender, EventArgs e)
        {
            loadWFHScheduleBySelectedDate();
            wfh_date.Text = dateWfh.Value.ToLongDateString();
        }

        private void t_search_KeyUp(object sender, KeyEventArgs e)
        {
            if (t_search.Text.Length > 0)
            {
                var list = wfhdata.Where(x => x.firstname.ToLower().Contains(t_search.Text.ToLower())).ToList();
                dataGridView1.DataSource = list;
            }
            else
            {
                dataGridView1.DataSource = wfhdata;
            }
        }

        private void t_search_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<int> enrollmentWfh = new List<int>();
            foreach (var enrollment in wfhdata)
            {
                if (enrollment.isWfhToday)
                {
                    enrollmentWfh.Add(enrollment.id.Value);
                }
            }
            SaveWFHData(enrollmentWfh);

        }
        async void SaveWFHData(List<int> enrollmentIds)
        {
            try
            {
                var resp = await Properties.Settings.Default.serverUrl
                    .AppendPathSegments(new string[] { "wfh", "update" })
                    .PostJsonAsync(new
                    {
                        date = dateWfh.Value.ToString("yyyy-MM-dd"),
                        enrollment_ids = enrollmentIds
                    });

                if (resp.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    MessageBox.Show("Jadwal WFH Berhasil disimpan!");
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
                Console.Write(e.StackTrace);
            }
        }
    }
}

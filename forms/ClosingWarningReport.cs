﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Flurl;
using Flurl.Http;
using Fingerprint_Reader.models;

namespace Fingerprint_Reader.forms
{
    public partial class ClosingWarningReport : Form
    {
        public ClosingWarningReport()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LoadData();
        }
        string entryStatus(Attendances a)
        {
            if (a.entry_auto == -2)
            {
                return "Tidak masuk";
            }
            else if (a.entry_auto == 1)
            {
                return "Tugas Keluar";
            }
            else if (a.closing_auto == -2)
            {
                return "Tidak masuk";
            }
            else if (a.closing_auto == -1)
            {
                return "Tidak absen keluar";
            }
            else if (a.closing_auto == null && a.exit_date == null)
            {
                return "Sudah masuk";
            }
            else if (a.closing_auto == null && a.exit_date != null)
            {
                return "Sudah keluar";
            }
            else
            {
                return "N/A";
            }
        }
        async void LoadData()
        {
            try
            {
                var data = await Properties.Settings.Default.serverUrl
                    .WithHeader("token",Program.token)
                    .AppendPathSegments(new String[] { "attendance", "warningreport", dateTimePicker1.Value.ToString("yyyy-MM-dd") })                    
                    .GetJsonAsync<Attendances[]>();

                foreach(var d in data)
                {
                    d.entry_date = d.entry_date + TimeZoneInfo.Local.GetUtcOffset(d.entry_date.Value);
                    d.exit_date = d.exit_date + TimeZoneInfo.Local.GetUtcOffset(d.exit_date.Value);
                }

                dataGridView1.AutoGenerateColumns = false;
                dataGridView1.DataSource = data;

                for (int i = 0; i < data.Count(); i++)
                {
                    var d = data[i];
                    dataGridView1[3, i].Value = entryStatus(d);                    
                }
            }
            catch (Exception)
            {
                
            }
        }

        private void ClosingWarningReport_Load(object sender, EventArgs e)
        {
            dataGridView1.AutoGenerateColumns = false;
        }

        private void ClosingWarningReport_Load_1(object sender, EventArgs e)
        {
            dataGridView1.AutoGenerateColumns = false;
            dateTimePicker1.Value = DateTime.Now;
            LoadData();
        }
    }
}

﻿namespace Fingerprint_Reader.forms
{
    partial class Absensi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.l_status = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.label11 = new System.Windows.Forms.Label();
            this.l_total = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.l_absen = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.l_belum_absen = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.l_permits = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.l_fingerprintcount = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.waitLoading1 = new CSharp.Winform.UI.Loading.WaitLoading();
            this.reading = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(13, 435);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(614, 13);
            this.progressBar1.TabIndex = 4;
            // 
            // l_status
            // 
            this.l_status.Font = new System.Drawing.Font("Segoe UI Symbol", 11F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_status.Location = new System.Drawing.Point(60, 396);
            this.l_status.Name = "l_status";
            this.l_status.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.l_status.Size = new System.Drawing.Size(462, 36);
            this.l_status.TabIndex = 5;
            this.l_status.Text = "Letakkan jari anda pada scanner yang tersedia";
            this.l_status.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Fingerprint_Reader.Properties.Resources.info;
            this.pictureBox1.Location = new System.Drawing.Point(16, 396);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(38, 36);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(573, 411);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(53, 20);
            this.linkLabel1.TabIndex = 7;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Admin";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(16, 101);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(260, 25);
            this.label11.TabIndex = 8;
            this.label11.Text = "Total Data Terdaftar";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // l_total
            // 
            this.l_total.Font = new System.Drawing.Font("Segoe UI Symbol", 34F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_total.Location = new System.Drawing.Point(16, 126);
            this.l_total.Name = "l_total";
            this.l_total.Size = new System.Drawing.Size(260, 90);
            this.l_total.TabIndex = 9;
            this.l_total.Text = "190";
            this.l_total.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(366, 101);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(260, 25);
            this.label13.TabIndex = 8;
            this.label13.Text = "Absen Hari Ini";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label13.Click += new System.EventHandler(this.label11_Click);
            // 
            // l_absen
            // 
            this.l_absen.Font = new System.Drawing.Font("Segoe UI Symbol", 34F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_absen.Location = new System.Drawing.Point(370, 126);
            this.l_absen.Name = "l_absen";
            this.l_absen.Size = new System.Drawing.Size(264, 90);
            this.l_absen.TabIndex = 9;
            this.l_absen.Text = "80";
            this.l_absen.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(16, 236);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(260, 25);
            this.label15.TabIndex = 8;
            this.label15.Text = "Belum Absen";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label15.Click += new System.EventHandler(this.label11_Click);
            // 
            // l_belum_absen
            // 
            this.l_belum_absen.Font = new System.Drawing.Font("Segoe UI Symbol", 34F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_belum_absen.Location = new System.Drawing.Point(16, 261);
            this.l_belum_absen.Name = "l_belum_absen";
            this.l_belum_absen.Size = new System.Drawing.Size(260, 90);
            this.l_belum_absen.TabIndex = 9;
            this.l_belum_absen.Text = "190";
            this.l_belum_absen.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label17
            // 
            this.label17.Location = new System.Drawing.Point(359, 236);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(260, 25);
            this.label17.TabIndex = 8;
            this.label17.Text = "Dispensasi";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label17.Click += new System.EventHandler(this.label11_Click);
            // 
            // l_permits
            // 
            this.l_permits.Font = new System.Drawing.Font("Segoe UI Symbol", 34F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_permits.Location = new System.Drawing.Point(370, 261);
            this.l_permits.Name = "l_permits";
            this.l_permits.Size = new System.Drawing.Size(264, 90);
            this.l_permits.TabIndex = 9;
            this.l_permits.Text = "190";
            this.l_permits.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l_permits.Click += new System.EventHandler(this.label18_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.l_fingerprintcount);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(-1, -1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(650, 88);
            this.panel1.TabIndex = 11;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.pictureBox2.Image = global::Fingerprint_Reader.Properties.Resources.smk;
            this.pictureBox2.Location = new System.Drawing.Point(3, 4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(86, 82);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 13;
            this.pictureBox2.TabStop = false;
            // 
            // l_fingerprintcount
            // 
            this.l_fingerprintcount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.l_fingerprintcount.Font = new System.Drawing.Font("Segoe UI Symbol", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_fingerprintcount.ForeColor = System.Drawing.Color.White;
            this.l_fingerprintcount.Location = new System.Drawing.Point(16, 44);
            this.l_fingerprintcount.Name = "l_fingerprintcount";
            this.l_fingerprintcount.Size = new System.Drawing.Size(619, 22);
            this.l_fingerprintcount.TabIndex = 12;
            this.l_fingerprintcount.Text = "20 Oktober 2018";
            this.l_fingerprintcount.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label1.Font = new System.Drawing.Font("Segoe UI Symbol", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(15, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(619, 35);
            this.label1.TabIndex = 11;
            this.label1.Text = "Absensi Guru SMKN 1 Cibinong";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // waitLoading1
            // 
            this.waitLoading1.Alpha = 125;
            this.waitLoading1.BackColor = System.Drawing.Color.DarkGray;
            this.waitLoading1.BindControl = null;
            this.waitLoading1.BkColor = System.Drawing.Color.DarkGray;
            this.waitLoading1.IsTransparent = true;
            this.waitLoading1.Location = new System.Drawing.Point(2, 85);
            this.waitLoading1.Name = "waitLoading1";
            this.waitLoading1.Size = new System.Drawing.Size(644, 372);
            this.waitLoading1.TabIndex = 12;
            this.waitLoading1.Visible = false;
            this.waitLoading1.Click += new System.EventHandler(this.waitLoading1_Click);
            // 
            // reading
            // 
            this.reading.BackColor = System.Drawing.Color.DarkGray;
            this.reading.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reading.Location = new System.Drawing.Point(2, 389);
            this.reading.Name = "reading";
            this.reading.Padding = new System.Windows.Forms.Padding(10);
            this.reading.Size = new System.Drawing.Size(644, 43);
            this.reading.TabIndex = 13;
            this.reading.Text = "Membaca Fingerprint";
            this.reading.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Absensi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(646, 454);
            this.Controls.Add(this.reading);
            this.Controls.Add(this.waitLoading1);
            this.Controls.Add(this.l_absen);
            this.Controls.Add(this.l_permits);
            this.Controls.Add(this.l_belum_absen);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.l_total);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.l_status);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Segoe UI Symbol", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Absensi";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Absensi";
            this.Activated += new System.EventHandler(this.Absensi_Activated);
            this.Deactivate += new System.EventHandler(this.Absensi_Deactivate);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Absensi_FormClosed);
            this.Load += new System.EventHandler(this.Absensi_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label l_status;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.Label l_total;
        private System.Windows.Forms.Label label13;
        public System.Windows.Forms.Label l_absen;
        private System.Windows.Forms.Label label15;
        public System.Windows.Forms.Label l_belum_absen;
        private System.Windows.Forms.Label label17;
        public System.Windows.Forms.Label l_permits;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label l_fingerprintcount;
        private System.Windows.Forms.Label label1;
        private CSharp.Winform.UI.Loading.WaitLoading waitLoading1;
        private System.Windows.Forms.Label reading;
    }
}
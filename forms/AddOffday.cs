﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Flurl;
using Flurl.Http;
using Fingerprint_Reader.models;

namespace Fingerprint_Reader.forms
{    
    public partial class AddOffday : Form
    {
        models.Offday offday = null;
        bool edit = false;
        public AddOffday()
        {
            InitializeComponent();
        }        
        bool validate()
        {
            var date = dateTimePicker1.Value;

            if((date - DateTime.Now).TotalDays < -7)
            {
                return false;
            }
            if(t_reason.Text.Length < 5)
            {
                return false;
            }

            offday = new Offday();
            offday.input_by = Program.AdminSession.name;
            offday.off_date = date;
            offday.reason = t_reason.Text;           

            return true;
        }
        async private void button1_Click(object sender, EventArgs e)
        {
            save();
        }

        async void save()
        {
            if (!validate())
            {
                MessageBox.Show("Cek kembali input anda!\n" 
                    + "1. Tanggal libur tidak boleh lebih dari 7 hari sebelum hari ini\n"
                    + "2. Alasan libur tidak boleh kurang dari 5 huruf", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            try
            {
                var date = dateTimePicker1.Value.ToString("yyyy-MM-dd");
                var resp = await Properties.Settings.Default.serverUrl
                    .AppendPathSegment("offdays")
                    .WithHeader("token", Program.token)
                    .PostJsonAsync(offday);
                if(resp.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    MessageBox.Show("Simpan data berhasil!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Hide();Dispose();
                }else if(resp.StatusCode == System.Net.HttpStatusCode.PartialContent)
                {
                    MessageBox.Show("Tanggal libur tersebut sudah ditetapkan!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    var dialog = MessageBox.Show("Simpan data gagal! Coba lagi?", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialog == DialogResult.Yes)
                    {
                        save();
                    }
                }

            }
            catch (Exception)
            {
                var dialog = MessageBox.Show("Simpan data gagal! Coba lagi?", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if(dialog == DialogResult.Yes)
                {
                    save();
                }                
            }
        }
        
        private void CreateDivision_Load(object sender, EventArgs e)
        {

        }
    }
}

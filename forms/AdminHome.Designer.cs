﻿namespace Fingerprint_Reader.forms
{
    partial class AdminHome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdminHome));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.kepegawaianToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pendaftaranFingerprintToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logAbsensiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pengaturanDivisiKerjaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tanggalLiburToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.administrasiJamPelajaranToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.guruPiketToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.laporanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.laporanAbsensiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.akunToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.daftarAdminToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logInToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.l_totalentry = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.l_totalexit = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.l_autoclose_count = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.l_permits = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.l_late_avg = new System.Windows.Forms.Label();
            this.waitLoading1 = new CSharp.Winform.UI.Loading.WaitLoading();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.button1 = new System.Windows.Forms.Button();
            this.jadwalWFHToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.kepegawaianToolStripMenuItem,
            this.laporanToolStripMenuItem,
            this.akunToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(8, 3, 0, 3);
            this.menuStrip1.Size = new System.Drawing.Size(1112, 25);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(12, 19);
            // 
            // kepegawaianToolStripMenuItem
            // 
            this.kepegawaianToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pendaftaranFingerprintToolStripMenuItem,
            this.logAbsensiToolStripMenuItem,
            this.pengaturanDivisiKerjaToolStripMenuItem,
            this.tanggalLiburToolStripMenuItem,
            this.administrasiJamPelajaranToolStripMenuItem,
            this.guruPiketToolStripMenuItem,
            this.jadwalWFHToolStripMenuItem});
            this.kepegawaianToolStripMenuItem.Enabled = false;
            this.kepegawaianToolStripMenuItem.Name = "kepegawaianToolStripMenuItem";
            this.kepegawaianToolStripMenuItem.Size = new System.Drawing.Size(89, 19);
            this.kepegawaianToolStripMenuItem.Text = "Kepegawaian";
            // 
            // pendaftaranFingerprintToolStripMenuItem
            // 
            this.pendaftaranFingerprintToolStripMenuItem.Name = "pendaftaranFingerprintToolStripMenuItem";
            this.pendaftaranFingerprintToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.pendaftaranFingerprintToolStripMenuItem.Text = "Pendaftaran Fingerprint";
            this.pendaftaranFingerprintToolStripMenuItem.Click += new System.EventHandler(this.pendaftaranFingerprintToolStripMenuItem_Click);
            // 
            // logAbsensiToolStripMenuItem
            // 
            this.logAbsensiToolStripMenuItem.Name = "logAbsensiToolStripMenuItem";
            this.logAbsensiToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.logAbsensiToolStripMenuItem.Text = "Log Absensi";
            this.logAbsensiToolStripMenuItem.Click += new System.EventHandler(this.logAbsensiToolStripMenuItem_Click);
            // 
            // pengaturanDivisiKerjaToolStripMenuItem
            // 
            this.pengaturanDivisiKerjaToolStripMenuItem.Name = "pengaturanDivisiKerjaToolStripMenuItem";
            this.pengaturanDivisiKerjaToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.pengaturanDivisiKerjaToolStripMenuItem.Text = "Pengaturan Divisi Kerja";
            this.pengaturanDivisiKerjaToolStripMenuItem.Click += new System.EventHandler(this.pengaturanDivisiKerjaToolStripMenuItem_Click);
            // 
            // tanggalLiburToolStripMenuItem
            // 
            this.tanggalLiburToolStripMenuItem.Name = "tanggalLiburToolStripMenuItem";
            this.tanggalLiburToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.tanggalLiburToolStripMenuItem.Text = "Tanggal Libur";
            this.tanggalLiburToolStripMenuItem.Click += new System.EventHandler(this.tanggalLiburToolStripMenuItem_Click);
            // 
            // administrasiJamPelajaranToolStripMenuItem
            // 
            this.administrasiJamPelajaranToolStripMenuItem.Name = "administrasiJamPelajaranToolStripMenuItem";
            this.administrasiJamPelajaranToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.administrasiJamPelajaranToolStripMenuItem.Text = "Administrasi Jam Pelajaran";
            this.administrasiJamPelajaranToolStripMenuItem.Click += new System.EventHandler(this.administrasiJamPelajaranToolStripMenuItem_Click);
            // 
            // guruPiketToolStripMenuItem
            // 
            this.guruPiketToolStripMenuItem.Name = "guruPiketToolStripMenuItem";
            this.guruPiketToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.guruPiketToolStripMenuItem.Text = "Guru Piket";
            this.guruPiketToolStripMenuItem.Click += new System.EventHandler(this.guruPiketToolStripMenuItem_Click);
            // 
            // laporanToolStripMenuItem
            // 
            this.laporanToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.laporanAbsensiToolStripMenuItem});
            this.laporanToolStripMenuItem.Name = "laporanToolStripMenuItem";
            this.laporanToolStripMenuItem.Size = new System.Drawing.Size(62, 19);
            this.laporanToolStripMenuItem.Text = "Laporan";
            // 
            // laporanAbsensiToolStripMenuItem
            // 
            this.laporanAbsensiToolStripMenuItem.Name = "laporanAbsensiToolStripMenuItem";
            this.laporanAbsensiToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.laporanAbsensiToolStripMenuItem.Text = "Laporan Absensi";
            this.laporanAbsensiToolStripMenuItem.Click += new System.EventHandler(this.laporanAbsensiToolStripMenuItem_Click);
            // 
            // akunToolStripMenuItem
            // 
            this.akunToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.daftarAdminToolStripMenuItem,
            this.logOutToolStripMenuItem,
            this.logInToolStripMenuItem});
            this.akunToolStripMenuItem.Name = "akunToolStripMenuItem";
            this.akunToolStripMenuItem.Size = new System.Drawing.Size(47, 19);
            this.akunToolStripMenuItem.Text = "Akun";
            // 
            // daftarAdminToolStripMenuItem
            // 
            this.daftarAdminToolStripMenuItem.Name = "daftarAdminToolStripMenuItem";
            this.daftarAdminToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.daftarAdminToolStripMenuItem.Text = "Daftar Admin";
            this.daftarAdminToolStripMenuItem.Click += new System.EventHandler(this.daftarAdminToolStripMenuItem_Click);
            // 
            // logOutToolStripMenuItem
            // 
            this.logOutToolStripMenuItem.Name = "logOutToolStripMenuItem";
            this.logOutToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.logOutToolStripMenuItem.Text = "Log out";
            this.logOutToolStripMenuItem.Click += new System.EventHandler(this.logOutToolStripMenuItem_Click);
            // 
            // logInToolStripMenuItem
            // 
            this.logInToolStripMenuItem.Name = "logInToolStripMenuItem";
            this.logInToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.logInToolStripMenuItem.Text = "Log In";
            this.logInToolStripMenuItem.Click += new System.EventHandler(this.logInToolStripMenuItem_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Fingerprint_Reader.Properties.Resources.kampak;
            this.pictureBox1.Location = new System.Drawing.Point(15, 98);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1084, 469);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Segoe UI Symbol", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Image = global::Fingerprint_Reader.Properties.Resources.DialogID_6220_32x;
            this.button2.Location = new System.Drawing.Point(582, 575);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(254, 40);
            this.button2.TabIndex = 2;
            this.button2.Text = "Laporan Absensi";
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "Jumlah Masuk";
            // 
            // l_totalentry
            // 
            this.l_totalentry.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_totalentry.Location = new System.Drawing.Point(116, 23);
            this.l_totalentry.Name = "l_totalentry";
            this.l_totalentry.Size = new System.Drawing.Size(69, 20);
            this.l_totalentry.TabIndex = 5;
            this.l_totalentry.Text = "23";
            this.l_totalentry.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(191, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 20);
            this.label4.TabIndex = 4;
            this.label4.Text = "Jumlah Keluar";
            // 
            // l_totalexit
            // 
            this.l_totalexit.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_totalexit.Location = new System.Drawing.Point(298, 23);
            this.l_totalexit.Name = "l_totalexit";
            this.l_totalexit.Size = new System.Drawing.Size(69, 20);
            this.l_totalexit.TabIndex = 5;
            this.l_totalexit.Text = "23";
            this.l_totalexit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.linkLabel1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.l_autoclose_count);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.l_totalexit);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.l_totalentry);
            this.groupBox1.Location = new System.Drawing.Point(15, 22);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(385, 72);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Hari Ini";
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(191, 47);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(119, 20);
            this.linkLabel1.TabIndex = 8;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Tinjau Autoclose";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 20);
            this.label1.TabIndex = 6;
            this.label1.Text = "Autoclose";
            // 
            // l_autoclose_count
            // 
            this.l_autoclose_count.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_autoclose_count.Location = new System.Drawing.Point(116, 47);
            this.l_autoclose_count.Name = "l_autoclose_count";
            this.l_autoclose_count.Size = new System.Drawing.Size(69, 20);
            this.l_autoclose_count.TabIndex = 7;
            this.l_autoclose_count.Text = "23";
            this.l_autoclose_count.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.l_permits);
            this.groupBox2.Location = new System.Drawing.Point(406, 28);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(294, 55);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Dispensasi";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 23);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(181, 20);
            this.label7.TabIndex = 4;
            this.label7.Text = "Jumlah Dispensasi Hari Ini";
            // 
            // l_permits
            // 
            this.l_permits.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_permits.Location = new System.Drawing.Point(211, 22);
            this.l_permits.Name = "l_permits";
            this.l_permits.Size = new System.Drawing.Size(69, 20);
            this.l_permits.TabIndex = 5;
            this.l_permits.Text = "23";
            this.l_permits.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button4.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Segoe UI Symbol", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Image = global::Fingerprint_Reader.Properties.Resources.LayerDiagramfile_layerdiagram_13450_32x;
            this.button4.Location = new System.Drawing.Point(13, 575);
            this.button4.Margin = new System.Windows.Forms.Padding(4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(561, 40);
            this.button4.TabIndex = 2;
            this.button4.Text = "Dispensasi";
            this.button4.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.l_late_avg);
            this.groupBox3.Location = new System.Drawing.Point(706, 28);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(393, 55);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Rata Rata Keterlambatan";
            // 
            // l_late_avg
            // 
            this.l_late_avg.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_late_avg.Location = new System.Drawing.Point(6, 22);
            this.l_late_avg.Name = "l_late_avg";
            this.l_late_avg.Size = new System.Drawing.Size(381, 20);
            this.l_late_avg.TabIndex = 5;
            this.l_late_avg.Text = "23 Menit 19 Detik";
            this.l_late_avg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l_late_avg.Click += new System.EventHandler(this.l_late_avg_Click);
            // 
            // waitLoading1
            // 
            this.waitLoading1.Alpha = 125;
            this.waitLoading1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.waitLoading1.BindControl = null;
            this.waitLoading1.BkColor = System.Drawing.Color.WhiteSmoke;
            this.waitLoading1.IsTransparent = true;
            this.waitLoading1.Location = new System.Drawing.Point(15, 98);
            this.waitLoading1.Name = "waitLoading1";
            this.waitLoading1.Size = new System.Drawing.Size(1084, 470);
            this.waitLoading1.TabIndex = 7;
            this.waitLoading1.Text = "waitLoading1";
            this.waitLoading1.Visible = false;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(868, 89);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(225, 23);
            this.progressBar1.TabIndex = 8;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Segoe UI Symbol", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = global::Fingerprint_Reader.Properties.Resources.DialogID_6220_32x;
            this.button1.Location = new System.Drawing.Point(839, 575);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(254, 40);
            this.button1.TabIndex = 2;
            this.button1.Text = "Laporan Bulanan";
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // jadwalWFHToolStripMenuItem
            // 
            this.jadwalWFHToolStripMenuItem.Name = "jadwalWFHToolStripMenuItem";
            this.jadwalWFHToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.jadwalWFHToolStripMenuItem.Text = "Jadwal WFH";
            this.jadwalWFHToolStripMenuItem.Click += new System.EventHandler(this.jadwalWFHToolStripMenuItem_Click);
            // 
            // AdminHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1112, 636);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.waitLoading1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Segoe UI Symbol", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AdminHome";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AdminHome";
            this.Activated += new System.EventHandler(this.Absensi_Activated);
            this.Deactivate += new System.EventHandler(this.Absensi_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AdminHome_FormClosing);
            this.Load += new System.EventHandler(this.AdminHome_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem kepegawaianToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pendaftaranFingerprintToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logAbsensiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem laporanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem laporanAbsensiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem akunToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem daftarAdminToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logOutToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label l_totalentry;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label l_totalexit;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label l_permits;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label l_late_avg;
        private CSharp.Winform.UI.Loading.WaitLoading waitLoading1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.ToolStripMenuItem logInToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pengaturanDivisiKerjaToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label l_autoclose_count;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolStripMenuItem tanggalLiburToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem administrasiJamPelajaranToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem guruPiketToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem jadwalWFHToolStripMenuItem;
    }
}
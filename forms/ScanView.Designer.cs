﻿namespace Fingerprint_Reader.forms
{
    partial class ScanView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.waitLoading1 = new CSharp.Winform.UI.Loading.WaitLoading();
            this.l_message = new System.Windows.Forms.Label();
            this.l_ontime_status = new System.Windows.Forms.Label();
            this.l_jammasuk = new System.Windows.Forms.Label();
            this.l_divisi = new System.Windows.Forms.Label();
            this.l_nip = new System.Windows.Forms.Label();
            this.l_nama = new System.Windows.Forms.Label();
            this.l_jammasukkeluar = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.l_status = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.waitLoading1);
            this.groupBox7.Controls.Add(this.l_message);
            this.groupBox7.Controls.Add(this.l_ontime_status);
            this.groupBox7.Controls.Add(this.l_jammasuk);
            this.groupBox7.Controls.Add(this.l_divisi);
            this.groupBox7.Controls.Add(this.l_nip);
            this.groupBox7.Controls.Add(this.l_nama);
            this.groupBox7.Controls.Add(this.l_jammasukkeluar);
            this.groupBox7.Controls.Add(this.label6);
            this.groupBox7.Controls.Add(this.label5);
            this.groupBox7.Controls.Add(this.label4);
            this.groupBox7.Controls.Add(this.l_status);
            this.groupBox7.Controls.Add(this.pictureBox2);
            this.groupBox7.Location = new System.Drawing.Point(12, 12);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(614, 304);
            this.groupBox7.TabIndex = 4;
            this.groupBox7.TabStop = false;
            this.groupBox7.Enter += new System.EventHandler(this.groupBox7_Enter);
            // 
            // waitLoading1
            // 
            this.waitLoading1.Alpha = 125;
            this.waitLoading1.BackColor = System.Drawing.Color.Transparent;
            this.waitLoading1.BindControl = null;
            this.waitLoading1.BkColor = System.Drawing.Color.Transparent;
            this.waitLoading1.IsTransparent = true;
            this.waitLoading1.Location = new System.Drawing.Point(96, 118);
            this.waitLoading1.Name = "waitLoading1";
            this.waitLoading1.Size = new System.Drawing.Size(81, 75);
            this.waitLoading1.TabIndex = 5;
            this.waitLoading1.Text = "waitLoading1";
            this.waitLoading1.Visible = false;
            this.waitLoading1.Click += new System.EventHandler(this.waitLoading1_Click);
            // 
            // l_message
            // 
            this.l_message.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_message.Location = new System.Drawing.Point(35, 268);
            this.l_message.Name = "l_message";
            this.l_message.Size = new System.Drawing.Size(545, 33);
            this.l_message.TabIndex = 8;
            this.l_message.Text = "Mencari data anda";
            this.l_message.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // l_ontime_status
            // 
            this.l_ontime_status.BackColor = System.Drawing.Color.SpringGreen;
            this.l_ontime_status.Font = new System.Drawing.Font("Segoe UI Symbol", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_ontime_status.Location = new System.Drawing.Point(377, 212);
            this.l_ontime_status.Name = "l_ontime_status";
            this.l_ontime_status.Size = new System.Drawing.Size(203, 32);
            this.l_ontime_status.TabIndex = 7;
            this.l_ontime_status.Text = "-";
            this.l_ontime_status.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // l_jammasuk
            // 
            this.l_jammasuk.Font = new System.Drawing.Font("Segoe UI Symbol", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_jammasuk.Location = new System.Drawing.Point(66, 224);
            this.l_jammasuk.Name = "l_jammasuk";
            this.l_jammasuk.Size = new System.Drawing.Size(290, 20);
            this.l_jammasuk.TabIndex = 6;
            this.l_jammasuk.Text = "-";
            this.l_jammasuk.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.l_jammasuk.Click += new System.EventHandler(this.l_jammasuk_Click);
            // 
            // l_divisi
            // 
            this.l_divisi.Font = new System.Drawing.Font("Segoe UI Symbol", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_divisi.Location = new System.Drawing.Point(66, 173);
            this.l_divisi.Name = "l_divisi";
            this.l_divisi.Size = new System.Drawing.Size(290, 20);
            this.l_divisi.TabIndex = 6;
            this.l_divisi.Text = "-";
            this.l_divisi.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // l_nip
            // 
            this.l_nip.Font = new System.Drawing.Font("Segoe UI Symbol", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_nip.Location = new System.Drawing.Point(66, 123);
            this.l_nip.Name = "l_nip";
            this.l_nip.Size = new System.Drawing.Size(290, 20);
            this.l_nip.TabIndex = 6;
            this.l_nip.Text = "-";
            this.l_nip.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // l_nama
            // 
            this.l_nama.Font = new System.Drawing.Font("Segoe UI Symbol", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_nama.Location = new System.Drawing.Point(62, 78);
            this.l_nama.Name = "l_nama";
            this.l_nama.Size = new System.Drawing.Size(294, 20);
            this.l_nama.TabIndex = 6;
            this.l_nama.Text = "-";
            this.l_nama.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // l_jammasukkeluar
            // 
            this.l_jammasukkeluar.Location = new System.Drawing.Point(275, 204);
            this.l_jammasukkeluar.Name = "l_jammasukkeluar";
            this.l_jammasukkeluar.Size = new System.Drawing.Size(81, 20);
            this.l_jammasukkeluar.TabIndex = 5;
            this.l_jammasukkeluar.Text = "Jam Masuk";
            this.l_jammasukkeluar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(273, 153);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 20);
            this.label6.TabIndex = 4;
            this.label6.Text = "Divisi Kerja";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(323, 103);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 20);
            this.label5.TabIndex = 3;
            this.label5.Text = "Nip";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(247, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 20);
            this.label4.TabIndex = 2;
            this.label4.Text = "Nama Pegawai";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // l_status
            // 
            this.l_status.AutoSize = true;
            this.l_status.Font = new System.Drawing.Font("Segoe UI Symbol", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_status.Location = new System.Drawing.Point(235, 23);
            this.l_status.Name = "l_status";
            this.l_status.Size = new System.Drawing.Size(109, 20);
            this.l_status.TabIndex = 1;
            this.l_status.Text = "Scan Berhasil";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(377, 34);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(203, 175);
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(12, 317);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(614, 11);
            this.progressBar1.TabIndex = 5;
            // 
            // ScanComplete
            // 
            //this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            //this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(636, 340);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.groupBox7);
            this.Font = new System.Drawing.Font("Segoe UI Symbol", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "ScanComplete";
            //this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ScanComplete";
            //this.Load += new System.EventHandler(this.ScanComplete_Load);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label l_ontime_status;
        private System.Windows.Forms.Label l_jammasuk;
        private System.Windows.Forms.Label l_divisi;
        private System.Windows.Forms.Label l_nip;
        private System.Windows.Forms.Label l_nama;
        private System.Windows.Forms.Label l_jammasukkeluar;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label l_status;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label l_message;
        private CSharp.Winform.UI.Loading.WaitLoading waitLoading1;
        private System.Windows.Forms.ProgressBar progressBar1;
    }
}
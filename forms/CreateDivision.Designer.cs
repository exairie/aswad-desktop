﻿namespace Fingerprint_Reader.forms
{
    partial class CreateDivision
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreateDivision));
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.t_division = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.n_hour_start = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.n_hour_end = new System.Windows.Forms.NumericUpDown();
            this.button1 = new System.Windows.Forms.Button();
            this.n_minute_start = new System.Windows.Forms.NumericUpDown();
            this.n_minute_end = new System.Windows.Forms.NumericUpDown();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.n_closing_hour = new System.Windows.Forms.NumericUpDown();
            this.n_closing_minute = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.n_break_hour = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.n_break_minute = new System.Windows.Forms.NumericUpDown();
            this.n_break_hour_end = new System.Windows.Forms.NumericUpDown();
            this.n_break_minute_end = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.inter_end_minute = new System.Windows.Forms.NumericUpDown();
            this.inter_end_hour = new System.Windows.Forms.NumericUpDown();
            this.inter_start_minute = new System.Windows.Forms.NumericUpDown();
            this.inter_start_hour = new System.Windows.Forms.NumericUpDown();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.n_hour_start)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_hour_end)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_minute_start)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_minute_end)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_closing_hour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_closing_minute)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_break_hour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_break_minute)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_break_hour_end)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_break_minute_end)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inter_end_minute)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inter_end_hour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inter_start_minute)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inter_start_hour)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(59)))), ((int)(((byte)(68)))));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(0, -3);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(508, 69);
            this.label2.TabIndex = 3;
            this.label2.Text = "Buat Divisi Baru";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 81);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "Nama Divisi";
            // 
            // t_division
            // 
            this.t_division.Location = new System.Drawing.Point(12, 104);
            this.t_division.Name = "t_division";
            this.t_division.Size = new System.Drawing.Size(481, 27);
            this.t_division.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 144);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Jam Masuk";
            // 
            // n_hour_start
            // 
            this.n_hour_start.Location = new System.Drawing.Point(12, 193);
            this.n_hour_start.Maximum = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.n_hour_start.Name = "n_hour_start";
            this.n_hour_start.Size = new System.Drawing.Size(120, 27);
            this.n_hour_start.TabIndex = 6;
            this.n_hour_start.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.n_hour_start.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(253, 144);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 20);
            this.label4.TabIndex = 4;
            this.label4.Text = "Jam Pulang";
            // 
            // n_hour_end
            // 
            this.n_hour_end.Location = new System.Drawing.Point(257, 193);
            this.n_hour_end.Maximum = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.n_hour_end.Name = "n_hour_end";
            this.n_hour_end.Size = new System.Drawing.Size(118, 27);
            this.n_hour_end.TabIndex = 6;
            this.n_hour_end.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.n_hour_end.Value = new decimal(new int[] {
            17,
            0,
            0,
            0});
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(367, 483);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(126, 34);
            this.button1.TabIndex = 7;
            this.button1.Text = "Simpan";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // n_minute_start
            // 
            this.n_minute_start.Location = new System.Drawing.Point(138, 193);
            this.n_minute_start.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.n_minute_start.Name = "n_minute_start";
            this.n_minute_start.Size = new System.Drawing.Size(113, 27);
            this.n_minute_start.TabIndex = 6;
            this.n_minute_start.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // n_minute_end
            // 
            this.n_minute_end.Location = new System.Drawing.Point(381, 193);
            this.n_minute_end.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.n_minute_end.Name = "n_minute_end";
            this.n_minute_end.Size = new System.Drawing.Size(112, 27);
            this.n_minute_end.TabIndex = 6;
            this.n_minute_end.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(59)))), ((int)(((byte)(68)))));
            this.pictureBox1.Image = global::Fingerprint_Reader.Properties.Resources.smk;
            this.pictureBox1.Location = new System.Drawing.Point(1, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(86, 60);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 324);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(250, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = "Batas Akhir Scan Fingerprint (Keluar)";
            // 
            // n_closing_hour
            // 
            this.n_closing_hour.Location = new System.Drawing.Point(12, 380);
            this.n_closing_hour.Maximum = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.n_closing_hour.Name = "n_closing_hour";
            this.n_closing_hour.Size = new System.Drawing.Size(239, 27);
            this.n_closing_hour.TabIndex = 6;
            this.n_closing_hour.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.n_closing_hour.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // n_closing_minute
            // 
            this.n_closing_minute.Location = new System.Drawing.Point(257, 380);
            this.n_closing_minute.Maximum = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.n_closing_minute.Name = "n_closing_minute";
            this.n_closing_minute.Size = new System.Drawing.Size(236, 27);
            this.n_closing_minute.TabIndex = 6;
            this.n_closing_minute.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.n_closing_minute.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(12, 170);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(120, 20);
            this.label6.TabIndex = 13;
            this.label6.Text = "Jam";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(138, 170);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(113, 20);
            this.label7.TabIndex = 13;
            this.label7.Text = "Menit";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(257, 170);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(118, 20);
            this.label8.TabIndex = 13;
            this.label8.Text = "Jam";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(381, 170);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(112, 20);
            this.label9.TabIndex = 13;
            this.label9.Text = "Menit";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(12, 357);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(239, 20);
            this.label10.TabIndex = 13;
            this.label10.Text = "Jam";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(257, 357);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(236, 20);
            this.label11.TabIndex = 13;
            this.label11.Text = "Menit";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // n_break_hour
            // 
            this.n_break_hour.Location = new System.Drawing.Point(12, 444);
            this.n_break_hour.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.n_break_hour.Name = "n_break_hour";
            this.n_break_hour.Size = new System.Drawing.Size(95, 27);
            this.n_break_hour.TabIndex = 6;
            this.n_break_hour.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.n_break_hour.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(138, 418);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(239, 20);
            this.label12.TabIndex = 13;
            this.label12.Text = "Jam Istirahat";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(219, 446);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(70, 20);
            this.label13.TabIndex = 13;
            this.label13.Text = "Sampai";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // n_break_minute
            // 
            this.n_break_minute.Location = new System.Drawing.Point(113, 444);
            this.n_break_minute.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.n_break_minute.Name = "n_break_minute";
            this.n_break_minute.Size = new System.Drawing.Size(88, 27);
            this.n_break_minute.TabIndex = 6;
            this.n_break_minute.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.n_break_minute.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // n_break_hour_end
            // 
            this.n_break_hour_end.Location = new System.Drawing.Point(304, 444);
            this.n_break_hour_end.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.n_break_hour_end.Name = "n_break_hour_end";
            this.n_break_hour_end.Size = new System.Drawing.Size(95, 27);
            this.n_break_hour_end.TabIndex = 6;
            this.n_break_hour_end.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.n_break_hour_end.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // n_break_minute_end
            // 
            this.n_break_minute_end.Location = new System.Drawing.Point(405, 444);
            this.n_break_minute_end.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.n_break_minute_end.Name = "n_break_minute_end";
            this.n_break_minute_end.Size = new System.Drawing.Size(88, 27);
            this.n_break_minute_end.TabIndex = 6;
            this.n_break_minute_end.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.n_break_minute_end.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(381, 256);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(112, 20);
            this.label14.TabIndex = 20;
            this.label14.Text = "Menit";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(257, 256);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(118, 20);
            this.label15.TabIndex = 21;
            this.label15.Text = "Jam";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(138, 256);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(113, 20);
            this.label16.TabIndex = 22;
            this.label16.Text = "Menit";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label17
            // 
            this.label17.Location = new System.Drawing.Point(12, 256);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(120, 20);
            this.label17.TabIndex = 23;
            this.label17.Text = "Jam";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // inter_end_minute
            // 
            this.inter_end_minute.Location = new System.Drawing.Point(381, 279);
            this.inter_end_minute.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.inter_end_minute.Name = "inter_end_minute";
            this.inter_end_minute.Size = new System.Drawing.Size(112, 27);
            this.inter_end_minute.TabIndex = 16;
            this.inter_end_minute.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // inter_end_hour
            // 
            this.inter_end_hour.Location = new System.Drawing.Point(257, 279);
            this.inter_end_hour.Maximum = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.inter_end_hour.Name = "inter_end_hour";
            this.inter_end_hour.Size = new System.Drawing.Size(118, 27);
            this.inter_end_hour.TabIndex = 17;
            this.inter_end_hour.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.inter_end_hour.Value = new decimal(new int[] {
            17,
            0,
            0,
            0});
            // 
            // inter_start_minute
            // 
            this.inter_start_minute.Location = new System.Drawing.Point(138, 279);
            this.inter_start_minute.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.inter_start_minute.Name = "inter_start_minute";
            this.inter_start_minute.Size = new System.Drawing.Size(113, 27);
            this.inter_start_minute.TabIndex = 18;
            this.inter_start_minute.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // inter_start_hour
            // 
            this.inter_start_hour.Location = new System.Drawing.Point(12, 279);
            this.inter_start_hour.Maximum = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.inter_start_hour.Name = "inter_start_hour";
            this.inter_start_hour.Size = new System.Drawing.Size(120, 27);
            this.inter_start_hour.TabIndex = 19;
            this.inter_start_hour.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.inter_start_hour.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(253, 230);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(143, 20);
            this.label18.TabIndex = 14;
            this.label18.Text = "Selesai Absen Ke - 2";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(12, 230);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(134, 20);
            this.label19.TabIndex = 15;
            this.label19.Text = "Mulai Absen Ke - 2";
            // 
            // CreateDivision
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(505, 529);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.inter_end_minute);
            this.Controls.Add(this.inter_end_hour);
            this.Controls.Add(this.inter_start_minute);
            this.Controls.Add(this.inter_start_hour);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.n_minute_end);
            this.Controls.Add(this.n_hour_end);
            this.Controls.Add(this.n_minute_start);
            this.Controls.Add(this.n_closing_minute);
            this.Controls.Add(this.n_break_minute_end);
            this.Controls.Add(this.n_break_hour_end);
            this.Controls.Add(this.n_break_minute);
            this.Controls.Add(this.n_break_hour);
            this.Controls.Add(this.n_closing_hour);
            this.Controls.Add(this.n_hour_start);
            this.Controls.Add(this.t_division);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Font = new System.Drawing.Font("Segoe UI Symbol", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CreateDivision";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CreateAdmin";
            this.Load += new System.EventHandler(this.CreateDivision_Load);
            ((System.ComponentModel.ISupportInitialize)(this.n_hour_start)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_hour_end)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_minute_start)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_minute_end)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_closing_hour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_closing_minute)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_break_hour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_break_minute)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_break_hour_end)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n_break_minute_end)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inter_end_minute)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inter_end_hour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inter_start_minute)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inter_start_hour)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox t_division;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown n_hour_start;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown n_hour_end;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.NumericUpDown n_minute_start;
        private System.Windows.Forms.NumericUpDown n_minute_end;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown n_closing_hour;
        private System.Windows.Forms.NumericUpDown n_closing_minute;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown n_break_hour;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown n_break_minute;
        private System.Windows.Forms.NumericUpDown n_break_hour_end;
        private System.Windows.Forms.NumericUpDown n_break_minute_end;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.NumericUpDown inter_end_minute;
        private System.Windows.Forms.NumericUpDown inter_end_hour;
        private System.Windows.Forms.NumericUpDown inter_start_minute;
        private System.Windows.Forms.NumericUpDown inter_start_hour;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
    }
}
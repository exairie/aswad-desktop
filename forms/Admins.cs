﻿using Fingerprint_Reader.models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fingerprint_Reader.forms
{
    public partial class Admins : Form
    {
        public Admins()
        {
            InitializeComponent();
        }

        private void DivisionList_Load(object sender, EventArgs e)
        {
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = DataStore.admins;
            progressBar1.Value = 80;
            DataStore.LoadAdmins(() =>
            {
                progressBar1.Value = 100;
                progressBar1.Hide();

                formatDate();
            });
        }

        void formatDate()
        {
            for (int i = 0; i < DataStore.divisions.Count; i++)
            {
                var d = DataStore.divisions[i];
                dataGridView1[1, i].Value = String.Format("{0}:{1}",
                    d.shift_start_hour.Value.ToString("0#"),
                    d.shift_start_minute.Value.ToString("0#"));
                dataGridView1[2, i].Value = String.Format("{0}:{1}",
                    d.shift_end_hour.Value.ToString("0#"),
                    d.shift_end_minute.Value.ToString("0#"));
            }
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void divisiBaruToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new CreateAdmin().ShowDialog();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {
                var data = (BindingList<models.Admins>)senderGrid.DataSource;
                new CreateAdmin(data[e.RowIndex]).ShowDialog();
            }
        }
    }
}

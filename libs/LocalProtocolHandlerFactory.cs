﻿using CefSharp;

namespace Fingerprint_Reader.libs
{
    public class LocalProtocolSchemeHandlerFactory : ISchemeHandlerFactory
    {
        public const string SchemeName = "customscheme";

        public IResourceHandler Create(IBrowser browser, IFrame frame, string schemeName, IRequest request)
        {
            return new LocalProtocol();
        }
    }
}
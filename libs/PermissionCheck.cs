﻿using Fingerprint_Reader.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingerprint_Reader.libs
{    
    public class PermissionCheck
    {
        public enum Permission
        {
            ChangeSchedule = 1,
            AddPermit = 2,
            ChangeDivisionSettings = 3,
            SuperAdminAccess = 4
        }
        public static bool checkPermission(Admins e, Permission p)
        {
            if (e.level >= 9999 && p == Permission.SuperAdminAccess) return true;
            else if (e.level < 9999 && p == Permission.SuperAdminAccess) return false;
            // Level 3 = Kurikulum & WMM
            if (e.level == 3 && p == Permission.ChangeDivisionSettings) return false;
            if (e.level == 3 && p == Permission.ChangeSchedule) return false;
            // Level 4 = Kepsek
            if (e.level == 4 && p == Permission.ChangeDivisionSettings) return false;   
                     
            return true;
        }
    }
}

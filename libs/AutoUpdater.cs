﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flurl.Http;
using System.Net;
using Flurl;
using System.Windows.Forms;

namespace Fingerprint_Reader.libs
{
    public class AutoUpdater
    {
        public static async void CheckForUpdate()
        {
            var currentHashString = "";
            try
            {
                currentHashString = System.IO.File.ReadAllText(Environment.CurrentDirectory + "/update_hash.txt");
            }
            catch (Exception e)
            {
                Console.Write(e.StackTrace);
            }
            try
            {

                var resp = await Properties.Settings.Default.serverUrl.AppendPathSegments(new string[] { "bin", "hash" })
                .GetAsync();
                var currentUpdateHash = await resp.Content.ReadAsStringAsync();
                MessageBox.Show("Update Hash : " + currentUpdateHash);
            }
            catch (Exception e)
            {

            }
        }
        public static void UpdateApp()
        {
            using (WebClient wc = new WebClient())
            {
                wc.DownloadProgressChanged += wc_DownloadProgressChanged;
                wc.DownloadFileAsync(
                    // Param1 = Link of file
                    new System.Uri("http://www.sayka.com/downloads/front_view.jpg"),
                    // Param2 = Path to save
                    "D:\\Images\\front_view.jpg"
                );
            }
        }

        private static void wc_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            throw new NotImplementedException();
        }
    }
}

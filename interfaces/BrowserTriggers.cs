﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingerprint_Reader.interfaces
{
    public delegate void StartCommand();
    public class BrowserTriggers
    {
        public event StartCommand StartDispensasi;
        public event StartCommand StartAdmin;
        public event StartCommand PageLoadCallback;
        public void pageLoadCallback()
        {
            PageLoadCallback?.Invoke();
        }
        public void startDispensasi()
        {
            StartDispensasi?.Invoke();
        }

        public void startAdmin()
        {
            StartAdmin?.Invoke();
        }
    }
}

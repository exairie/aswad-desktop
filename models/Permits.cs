﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingerprint_Reader.models
{
    public class Permits
    {
        public string reason { get; set; }
        public string permit_by { get; set; }
        public DateTime? permit_from { get; set; }
        public DateTime? permit_to { get; set; }
        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }
        public int? id { get; set; }
        public int? enrollment_id { get; set; }
        public int? entry_hour { get; set; }
        public int? entry_minute { get; set; }
        public int? exit_hour { get; set; }
        public int? exit_minute { get; set; }
        public int bypass_attendance { get; set; }
        public string status { get; set; }
        public string permit_type { get; set; }
        public Enrollments enrollment { get; set; }
    }
}

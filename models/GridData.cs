﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingerprint_Reader.models
{
    public class GridData
    {
        public string name { get; set; }
        public string status { get; set; }
        public string detail { get; set; }
        public string enter { get; set; }
        public string exit { get; set; }
        public DateTime date { get; set; }
    }
}

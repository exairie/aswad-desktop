﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingerprint_Reader.models
{
    public class Divisions
    {
        public string division { get; set; }
        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }
        public string id { get; set; }
        public int? shift_start_hour { get; set; }
        public int? shift_end_hour { get; set; }
        public int? shift_start_minute { get; set; }
        public int? shift_end_minute { get; set; }

        public int? inter_shift_start_hour { get; set; }
        public int? inter_shift_end_hour { get; set; }
        public int? inter_shift_start_minute { get; set; }
        public int? inter_shift_end_minute { get; set; }

        public int? closing_hour { get; set; }
        public int? closing_minute { get; set; }
        public int? break_hour { get; set; }
        public int? break_minute { get; set; }

        public int? break_hour_end { get; set; }
        public int? break_minute_end { get; set; }

        public override string ToString()
        {
            return division;
        }
    }
}

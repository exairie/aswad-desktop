﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingerprint_Reader.models
{
    public class EmploymentStatus
    {        
        public string employment_status { get; set; }
        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }
        public string id { get; set; }

        public override string ToString()
        {
            return employment_status;
        }
    }
}

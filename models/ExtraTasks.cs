﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingerprint_Reader.models
{
    public class ExtraTasks
    {
        public string task_type { get; set; }
        public string task_type_printed
        {
            get
            {
                try
                {
                    var s = DataStore.ListPenugasan.Where(x => x.Key == task_type).First();
                    return s.Value;
                }
                catch (Exception)
                {
                    return "n/a";
                }
            }
        }
        public string task_description { get; set; }
        public string task_point_spec { get; set; }
        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }
        public int? id { get; set; }
        public int? enrollment_id { get; set; }
    }
}

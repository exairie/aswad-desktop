﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingerprint_Reader.models
{
    public class Picket
    {
        public string day { get; set; }
        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }
        public int? id { get; set; }
        public int? enrollment_id { get; set; }

        public Enrollments enrollment { get; set; }
    }
}

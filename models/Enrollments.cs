﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingerprint_Reader.models
{
    public class Enrollments
    {
        private string _firstname;
        public string firstname
        {
            get
            {
                return _firstname == null ? "" : _firstname;
            }
            set
            {
                _firstname = value;
            }
        }
        private string _lastname;
        public string lastname
        {
            get
            {
                return _lastname == null ? "" : _lastname;
            }
            set
            {
                _lastname = value;
            }
        }
        private string _nip;
        public string nip
        {
            get
            {
                return _nip == null ? "" : _nip;
            }
            set
            {
                _nip = value;
            }
        }
        private string _nik;
        public string nik
        {
            get
            {
                return _nik == null ? "" : _nik;
            }
            set
            {
                _nik = value;
            }
        }
        private string _gender;
        public string gender
        {
            get
            {
                return _gender == null ? "" : _gender;
            }
            set
            {
                _gender = value;
            }
        }
        private string _phone;
        public string phone
        {
            get
            {
                return _phone == null ? "" : _phone;
            }
            set
            {
                _phone = value;
            }
        }
        private string _email;
        public string email
        {
            get
            {
                return _email == null ? "" : _email;
            }
            set
            {
                _email = value;
            }
        }
        private string _grade;
        public string grade
        {
            get
            {
                return _grade == null ? "" : _grade;
            }
            set
            {
                _grade = value;
            }
        }
        private string _fmd1;
        public string fmd1
        {
            get
            {
                return _fmd1 == null ? "" : _fmd1;
            }
            set
            {
                _fmd1 = value;
            }
        }
        private string _fmd2;
        public string fmd2
        {
            get
            {
                return _fmd2 == null ? "" : _fmd2;
            }
            set
            {
                _fmd2 = value;
            }
        }
        private string _fmd3;
        public string fmd3
        {
            get
            {
                return _fmd3 == null ? "" : _fmd3;
            }
            set
            {
                _fmd3 = value;
            }
        }
        private string _username;
        public string username
        {
            get
            {
                return _username == null ? "" : _username;
            }
            set
            {
                _username = value;
            }
        }
        private string _password;
        public string password
        {
            get
            {
                return _password == null ? "" : _password;
            }
            set
            {
                _password = value;
            }
        }
        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }
        public string address { get; set; }
        public int? id { get; set; }
        public int rfid_allowed { get; set; }
        public string rfid_code { get; set; }
        public string employment_status_id { get; set; }
        public string division_id { get; set; }
        public Divisions division { get; set; }
        public EmploymentStatus employment_status { get; set; }
        public double? late_accumulation_entry { get; set; }
        public double? late_accumulation_exit { get; set; }

        public string _kota;
        public string kota
        {
            get
            {
                return _kota;
            }
            set
            {
                _kota = value;
            }
        }
        public string _kecamatan;
        public string kecamatan
        {
            get
            {
                return _kecamatan;
            }
            set
            {
                _kecamatan = value;
            }
        }
        public string _kelurahan;
        public string kelurahan
        {
            get
            {
                return _kelurahan;
            }
            set
            {
                _kelurahan = value;
            }
        }

        private string _nuptk;
        public string nuptk
        {
            get
            {
                return _nuptk;
            }
            set
            {
                _nuptk = value;
            }
        }

        public string photo1 { get; set; }
        public string photo2 { get; set; }
        public int duk { get; set; }
        public PersonalSchedule personal_schedule { get; set; }
        public int? active { get; set; }
        public override string ToString()
        {
            return firstname + " " + lastname;
        }

        public List<WFHSchedule> wfh_schedules { get; set; }
    }
}

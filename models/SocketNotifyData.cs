﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingerprint_Reader.models
{
    public class SocketNotifyData
    {
        public string action { get; set; }
        public int attendant { get; set; }
        public string origin { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingerprint_Reader.models
{
    public class Offday
    {
        public int id { get; set; }
        public DateTime off_date { get; set; }
        public string reason { get; set; }
        public string input_by { get; set; }
        public DateTime? updated_at { get; set; }
        public DateTime? created_at { get; set; }
    }
}

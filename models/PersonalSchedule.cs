﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingerprint_Reader.models
{
    public class PersonalSchedule
    {
        public Enrollments enrollment { get; set; }
        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }
        public int? id { get; set; }
        public int? enrollment_id { get; set; }
        public int? senin_hadir { get; set; }
        public int? senin_custom { get; set; }
        public int? senin_masuk_jam { get; set; }
        public int? senin_masuk_menit { get; set; }
        public int? senin_keluar_jam { get; set; }
        public int? senin_keluar_menit { get; set; }
        public int? selasa_hadir { get; set; }
        public int? selasa_custom { get; set; }
        public int? selasa_masuk_jam { get; set; }
        public int? selasa_masuk_menit { get; set; }
        public int? selasa_keluar_jam { get; set; }
        public int? selasa_keluar_menit { get; set; }
        public int? rabu_hadir { get; set; }
        public int? rabu_custom { get; set; }
        public int? rabu_masuk_jam { get; set; }
        public int? rabu_masuk_menit { get; set; }
        public int? rabu_keluar_jam { get; set; }
        public int? rabu_keluar_menit { get; set; }
        public int? kamis_hadir { get; set; }
        public int? kamis_custom { get; set; }
        public int? kamis_masuk_jam { get; set; }
        public int? kamis_masuk_menit { get; set; }
        public int? kamis_keluar_jam { get; set; }
        public int? kamis_keluar_menit { get; set; }
        public int? jumat_hadir { get; set; }
        public int? jumat_custom { get; set; }
        public int? jumat_masuk_jam { get; set; }
        public int? jumat_masuk_menit { get; set; }
        public int? jumat_keluar_jam { get; set; }
        public int? jumat_keluar_menit { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingerprint_Reader.models
{
    public class Attendances
    {
        public string fmd { get; set; }
        private DateTime entry = DateTime.Now;
        public DateTime? entry_date
        {
            get { return entry; }
            set
            {
                entry = value.Value;
            }
        }
        public DateTime? exit_date { get; set; }
        public DateTime? auto_disposal_date { get; set; }
        public string exit_fmd { get; set; }
        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }
        public int? id { get; set; }
        public long entry_delta { get; set; }
        public long exit_delta { get; set; }
        public int? enrollment_id { get; set; }
        public int? shift_start_hour { get; set; }
        public int? shift_end_hour { get; set; }
        public int? shift_start_minute { get; set; }
        public int? shift_end_minute { get; set; }
        public int? entry_status { get; set; }
        public int? exit_work_hour { get; set; }

        public int? inter_attendance_start_hour { get; set; }
        public int? inter_attendance_start_minute { get; set; }
        public int? inter_attendance_end_hour { get; set; }
        public int? inter_attendance_end_minute { get; set; }

        public string inter_attendance_fmd { get; set; }
        public DateTime? inter_attendance_date { get; set; }

        public int? total_work_sec { get; set; }

        public string closing_reason { get; set; }
        public string entry_auto_reason { get; set; }
        public int? closing_auto { get; set; }
        public int? entry_auto { get; set; }
        public int? offday { get; set; }
        public string position { get; set; }

        public Enrollments enrollment { get; set; }
    }
}

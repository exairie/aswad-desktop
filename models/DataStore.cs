﻿using Flurl;
using Flurl.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingerprint_Reader.models
{
    public delegate void VoidCallback();
    public delegate void BoolCallback(Boolean status);
    public delegate void ScanNotify(Enrollments e);
    public delegate void UpdateNotify(String socketid);
    public class DataStore
    {
        public static event VoidCallback divisionUpdated;

        public static BindingList<Enrollments> enrollments = new BindingList<models.Enrollments>();
        public static BindingList<Divisions> divisions = new BindingList<Divisions>();
        public static BindingList<EmploymentStatus> employmentStatuses = new BindingList<EmploymentStatus>();
        public static BindingList<Admins> admins = new BindingList<Admins>();
        public static BindingList<Permits> permits = new BindingList<Permits>();
        public static BindingList<WFHSchedule> wfhSchedule = new BindingList<WFHSchedule>();
        public static BindingList<Enrollments> enrollmentWithWfhData = new BindingList<Enrollments>();
        public static Dictionary<string, string> _divisiKerja = null;
        public static Dictionary<string, string> DivisiKerja
        {
            get
            {
                if (_divisiKerja == null)
                {
                    _divisiKerja = new Dictionary<string, string>();
                    _divisiKerja.Add("tata_usaha", "Tata Usaha");
                    _divisiKerja.Add("guru", "Guru");
                    _divisiKerja.Add("tester", "Tester");
                }



                return _divisiKerja;
            }
        }
        public static List<KeyValuePair<string, string>> _listPenugasan = null;
        public static List<KeyValuePair<string, string>> ListPenugasan
        {
            get
            {
                if (_listPenugasan == null)
                {
                    _listPenugasan = new List<KeyValuePair<string, string>>();
                    _listPenugasan.Add(new KeyValuePair<string, string>("wakasek", "Wakil Kepala Sekolah"));
                    _listPenugasan.Add(new KeyValuePair<string, string>("kakom", "Kepala Kompetensi Keahlian"));
                    _listPenugasan.Add(new KeyValuePair<string, string>("kabeng", "Kepala Bengkel"));
                    _listPenugasan.Add(new KeyValuePair<string, string>("walas", "Wali Kelas"));
                    _listPenugasan.Add(new KeyValuePair<string, string>("stafftu", "Staff Tata Usaha"));
                }
                return _listPenugasan;
            }
        }
        public static Dictionary<string, string> _statusKetenagakerjaan = null;
        public static Dictionary<string, string> StatusKetenagakerjaan
        {
            get
            {
                if (_statusKetenagakerjaan == null)
                {
                    _statusKetenagakerjaan = new Dictionary<string, string>();
                    _statusKetenagakerjaan.Add("pegawai_negeri", "Pegawai Negeri");
                    _statusKetenagakerjaan.Add("honorer", "Honorer");
                }


                return _statusKetenagakerjaan;
            }
        }
        public static async void LoadEnrollments(VoidCallback callback, VoidCallback onFailure = null)
        {
            try
            {
                var query = await Properties.Settings.Default.serverUrl
                .AppendPathSegments(new string[] { "administrator", "enrollment", Program.socket_id })
                .GetJsonAsync<models.Enrollments[]>();

                enrollments.Clear();
                foreach (var e in query)
                {
                    enrollments.Add(e);
                }

                callback();
            }
            catch (FlurlHttpException e)
            {
                Console.Write(e);
                if (onFailure != null)
                {
                    onFailure();
                }
            }
        }
        public static async void LoadDivisions(VoidCallback callback)
        {
            try
            {
                var query = await Properties.Settings.Default.serverUrl
                .WithHeader("token", Program.token)
                .AppendPathSegments(new string[] { "division" })
                .GetJsonAsync<models.Divisions[]>();

                divisions.Clear();
                foreach (var e in query)
                {
                    divisions.Add(e);
                }
                divisionUpdated();
            }
            catch (FlurlHttpException e)
            {
                Console.WriteLine(e);
            }

            callback();
        }
        public static async void LoadEmploymentStatus(VoidCallback callback)
        {
            try
            {
                var query = await Properties.Settings.Default.serverUrl
                    .WithHeader("token", Program.token)
                    .AppendPathSegments(new string[] { "employment" })
                    .GetJsonAsync<models.EmploymentStatus[]>();

                employmentStatuses.Clear();
                foreach (var e in query)
                {
                    employmentStatuses.Add(e);
                }
            }
            catch (FlurlHttpException e)
            {
                Console.WriteLine(e);
            }

            callback();
        }
        public static async void LoadPermits(VoidCallback callback)
        {
            try
            {
                var query = await Properties.Settings.Default.serverUrl
                    .AppendPathSegments(new string[] { "administrator", "permits" })
                    .GetJsonAsync<models.Permits[]>();

                permits.Clear();
                foreach (var e in query)
                {
                    permits.Add(e);
                }
            }
            catch (FlurlHttpException e)
            {
                Console.WriteLine(e);
            }

            callback();
        }
        public static async void LoadAdmins(VoidCallback callback)
        {
            try
            {
                var query = await Properties.Settings.Default.serverUrl
                    .AppendPathSegments(new string[] { "admin" })
                    .WithHeader("token", Program.token)
                    .GetJsonAsync<models.Admins[]>();

                admins.Clear();
                foreach (var e in query)
                {
                    admins.Add(e);
                }
            }
            catch (FlurlHttpException e)
            {
                Console.WriteLine(e);
            }

            callback();
        }
        public static async void LoadWFHSchedules(DateTime date, VoidCallback callback)
        {
            try
            {
                var query = await Properties.Settings.Default.serverUrl
                    .AppendPathSegments(new string[] { "wfh", "all_data" })
                    .SetQueryParams(new
                    {
                        date = date.ToString("yyyy-MM-dd")
                    })
                    .WithHeader("token", Program.token)
                    .GetJsonAsync<models.WFHSchedule[]>();

                wfhSchedule.Clear();
                foreach (var schedule in query)
                {
                    wfhSchedule.Add(schedule);
                }
            }
            catch (FlurlHttpException e)
            {
                Console.WriteLine(e);
            }

            callback();
        }
    }
}

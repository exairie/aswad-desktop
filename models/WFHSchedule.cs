﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingerprint_Reader.models
{
    public class WFHSchedule
    {
        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }
        public int? id { get; set; }
        public int? enrollment_id { get; set; }
        public DateTime? date { get; set; }

        public Enrollments enrollment { get; set; }

        public bool isTodayWfh { get; set; }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}

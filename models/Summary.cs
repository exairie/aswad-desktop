﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingerprint_Reader.models
{
    public class Summary
    {
        public class EntranceExit
        {
            int total_entrance { get; set; }
            int total_exit { get; set; }
        }

        public class Permits
        {
            int total_permits { get; set; }
        }

        public class AverageTime
        {
            long average_time { get; set; }
        }
        public class FingerprintSum
        {
            public Attendances[] attendance { get; set; }
            public models.Permits[] permit { get; set; }
        }
    }
}

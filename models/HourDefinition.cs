﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fingerprint_Reader.models
{
    public class HourDefinition
    {
        public int id { get; set; }
        public int hour { get; set; }
        public string senin { get; set; }
        public string selasa { get; set; }
        public string rabu { get; set; }
        public string kamis { get; set; }
        public string jumat { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
    }
}

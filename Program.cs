﻿using Fingerprint_Reader.forms;
using Fingerprint_Reader.models;
using Newtonsoft.Json.Linq;
using Quobject.SocketIoClientDotNet.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Fingerprint_Reader.models.Events;

namespace Fingerprint_Reader
{
    static class Program
    {
        public static event ScanNotify scanNotified;
        public static event UpdateNotify updateNotified;
        public static event UpdateNotify systemReloadNotified;
        public static models.Admins AdminSession = null;
        public static event AttendanceDataUpdated DataUpdate;

        public static Socket socketClient;
        public static string socket_id = "";
        public static void notifyAttendanceDataUpdated()
        {
            if (DataUpdate != null)
            {
                DataUpdate();
            }
        }
        public static string token = null;
        static void setupSocket()
        {
            socketClient = IO.Socket(Properties.Settings.Default.serverUrl);
            socketClient.On("registered", id =>
            {
                socket_id = id.ToString();
            });
            socketClient.On("notifier", data =>
            {
                Console.WriteLine("Notifier Triggered");
                var socketNotifyData = JObject.Parse(data.ToString());
                if (socketNotifyData.Value<string>("origin") != socket_id)
                {
                    Enrollments[] enrollments = (from d in DataStore.enrollments
                                                 where d.id == socketNotifyData.Value<int>("attendant")
                                                 select d).ToArray();
                    if (enrollments.Count() > 0)
                    {
                        scanNotified(enrollments.First());
                    }
                }
            });
            socketClient.On("update_enrollment", data =>
            {
                String socketid = data.ToString();
                if (socket_id == socketid) return;
                updateNotified?.Invoke(socketid);
            });
            socketClient.On("frontend_reload", data =>
            {
                systemReloadNotified?.Invoke("");
            });
        }
        [DllImportAttribute("user32.dll")]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        [DllImportAttribute("user32.dll")]
        public static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
        [DllImportAttribute("user32.dll")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        public static void ShowFront(string windowName)
        {
            IntPtr firstInstance = FindWindow(null, windowName);
            ShowWindow(firstInstance, 1);
            SetForegroundWindow(firstInstance);
        }
        static Mutex mutex;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        ///
        [STAThread]
        static void Main()
        {
            bool created;
            mutex = new Mutex(false, "FINGERPRINTABSENSIRPL", out created);
            if (!created)
            {
                ShowFront("FINGERPRINTABSENSIRPL");
                return;
            }
            setupSocket();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new HomePage());
            GC.KeepAlive(mutex);


        }
    }
}

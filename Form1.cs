﻿using DPUruNet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fingerprint_Reader
{
    public partial class Form1 : Form
    {
        public bool register = false;
        public class FingerData
        {
            public string name { get; set; }
            public Fmd fingerData { get; set; }
        }
        Reader freader = null;
        Fmd lastResult = null;
        BindingList<FingerData> fingerprintRegistration = new BindingList<FingerData>();
        Timer timer = new Timer();
        public Form1()
        {
            InitializeComponent();
            timer.Tick += Timer_Tick;

            dataGridView1.DataSource = fingerprintRegistration;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            var captured = freader.Capture(Constants.Formats.Fid.ISO, Constants.CaptureProcessing.DP_IMG_PROC_DEFAULT, 3000, 500);
            if(captured.ResultCode == Constants.ResultCode.DP_SUCCESS)
            {
                var fmd = FeatureExtraction.CreateFmdFromFid(captured.Data, Constants.Formats.Fmd.ISO).Data;

                foreach(var f in fingerprintRegistration)
                {
                    var compScore = Comparison.Compare(f.fingerData, 0, fmd, 0).Score;
                    if(compScore <= int.MaxValue / 2)
                    {
                        label3.Text = f.name;
                    }
                }
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {            
            var reader = ReaderCollection.GetReaders();
            freader = reader[0];
            freader.Open(Constants.CapturePriority.DP_PRIORITY_EXCLUSIVE);
            freader.Calibrate();
            label1.Text = freader.GetStatus().ToString();
            label2.Text = freader.Status.ToString();
            freader.On_Captured += Form1_On_Captured;

        }
        

        private void Form1_On_Captured(CaptureResult result)
        {
            if (!register) return;
            var capture = result;
            label1.Text = capture.ResultCode.ToString();
            if (capture.ResultCode == Constants.ResultCode.DP_SUCCESS)
            {
                Invoke(new Action(() =>
                {
                    fingerprintRegistration.Add(new FingerData()
                    {
                        name = textBox1.Text,
                        fingerData = FeatureExtraction.CreateFmdFromFid(capture.Data, Constants.Formats.Fmd.ANSI).Data
                    });
                    MessageBox.Show("Capture Success!");
                    dataGridView1.Refresh();
                }));
            }
        }



        private void button1_Click(object sender, EventArgs e)
        {
            register = !register;
            if (register)
            {
                MessageBox.Show("Capture Start");
                freader.UpdateLed(Constants.LedId.MAIN, Constants.LedCommand.OFF);
                var capture = freader.CaptureAsync(Constants.Formats.Fid.ISO, Constants.CaptureProcessing.DP_IMG_PROC_DEFAULT, 500);
            }else
            {
                MessageBox.Show("Capture end");
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {            
            timer.Interval = 500;
            timer.Start();
        }
    }
}
